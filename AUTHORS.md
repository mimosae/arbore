# Authors

Yuwei WANG
<yuwei.wang@edf.fr>
<yuwei.wang@telecom-sudparis.eu>
- Design and implementation of the GDE use case with synchronous calls
- Design and implementation of the DSU algorithm

Julio GUZMÁN BARRAZA, MSc CSN internship, March 2022 — September 2022
<julio.guzman_barraza@telecom-sudparis.eu>
- Design and implementation of the GDE use case with asynchronous calls (AMQP)
- First elements of implementation of the DSU algorithm of ARBORE

Denis CONAN
<denis.conan@telecom-sudparis.eu>
- Architecting of ARBORE
- Review of code and documentation

Sophie CHABRIDON
<sophie.chabridon@telecom-sudparis.eu>
- Architecting of ARBORE

Nesrine JOUINI, ASR 2023, November 2022 - February 2023
<nesrine.jouini@telecom-sudparis.eu>
<nesrine.jouini.nj@gmail.com>
- Refactoring of the original implementation (GDE use case + DSU algorithm)
  with asynchronous calls

Timothé GRISOT, ASR 2023, November 2022 - February 2023
<timothe.grisot@telecom-sudparis.eu>
<timothe.grisot@gmail.com>
- Refactoring of the original implementation (GDE use case + DSU algorithm)
  with asynchronous calls

Ayoub BNINA, MS internship, June 2023 — November 2023
<ayoub.bnina@telecom-sudparis.eu>
- Refactoring and implementation of the DSU algorithm with quiescence
