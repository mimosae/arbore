- [ ] Implement the DSU algorithm for quiescence with data structures
      (multigraphs).

- [ ] Reorganize services with the following packages/directories:

    - `config`, with a `config.go` file, containing general service
      configuration

    - `core`, with files `handler_application.go` and
      `handler_control.go`, for each function declaration

    - `db`, for all database-related files

    - `dsu`, with a file `dsu.go` containing the variables for the algorithms

    - `entities`, with a file for each entity, for the structures used
      by the service

    - `service`, for application-related functions

    - `http`, in case there is an HTTP handler

- [ ] Services should receive an argument when being started stating
    whether they should immediately connect to the `gde_exchange` or
    not. This is usefull for separating microservices and links
    creation when executing reconfiguration plans. If a service starts
    without connecting to the `gde_exchange`, how do we tell it to
    create links? One idea could be to have a separate exchange
    dedicated to control messages, exchange to which services always
    immediately connect so they can receive control messages. Then,
    the Autonomic Manager can decide to tell a service to link to the
    `gde_exchange` by sending it an appropriate control message. This
    change would also introduce a `switch` statement in the
    `ConsumeMessages` to switch between the different queues.

- [ ] Have the Autonomic Manager run continuously so that it can keep
    information in memory, and use a small separate service to send
    commands to it.

- [ ] The "accountability" request does nothing from the point of view
    of the user, we (Nesrine & Timothé) think it is something that has
    been started but not finished, but we are not sure.

- [ ] The Events Logger service is not implemented in the same way as
    other services from the point of view of the DSU algorithm, thus
    it is ignored for now when taking a snapshot of the system (see
    details in the Autonomic Manager consumer and commands). It should
    be reimplemented to have a correct behavior— i.e., it should
    receive direct messages from other services that want to log data,
    instead of "capturing" all messages circulating in the exchange,
    and then reintegrated for snapshots.

- [ ] Implement responses in some failure cases, notably in the
    Permission Service, in case the permission can't be checked, and
    in the User Service, when the user does not exists.

- [ ] In the `arbore/dsu/algorithms` module, avoid returning routing
    keys and, messages leave the message sending to algorithms methods
    instead. Needs a small reimplementation of the `ConsumeMessages`
    methods in `gde/common/amqp-handler`.
