# ARBORE

ARBORE = ARchitecting Based on microservice versiOns with REconfiguration



## Content of this directory

- `Code`: Golang code & scripts of the GDE use case with the DSU algorithm
- `Documentation`: technical reports, etc.
- `README.md`: this file
- `AUTHORS.md`: all the people who participated in the development of ARBORE
- `LICENSE.md`: the license notification for ARBORE
- `TODO.md`: suggested tasks to continue the development of ARBORE

For more details about the Golang implementation of ARBORE, please read the `README.md` file located
in the `Code` directory.
