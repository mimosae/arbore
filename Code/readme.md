# Implementation of Publish and Subscribe Communication GDE for Microservices

This directory contains the implementation of asynchronous
communication for GDE microservices using the publish-subscribe
pattern, as well as the dynamic software updating (DSU) algorithms.

The research work considers the implementation of two interaction
modes between the microservices: client-server (synchronous) and
publish-subscribe (asynchronous). However, the first implementation of
GDE was mainly client-server; thus, to prototype the research
proposal, we need a version that brings publish-subscribe into
play. This is the purpose of the new GDE version in the ARBORE
project.

## License

LGPL and refer to file `LICENSE.md` at the project's root for
potential exceptions.

## Software prerequisites

Golang 1.19
```
$ which go
/usr/bin/go
$ go version
go version go1.18.1 linux/amd64
```

Docker v 20.10
```
$ which docker
/usr/bin/docker
$ docker -v
Docker version 20.10.22, build 3a2c30b
```

## Demonstrations

You can run demonstrations of ARBORE using the scripts located in the
`scripts/demos` directory.  Note that you can run these scripts from
any directory.

For more details about the provided Bash scripts, please read the
`readme.md` file located in the `scripts` directory.

For explanations about the demonstration scripts specifically, please
read the `readme.md` file located in the `scripts/demos` directory.

## Considerations about the publish-subscribe interaction mode

In our implementation, the routing key format for application messages
is made up of four words <src_ms>.<dst_ms>.<event_type>.<event_name>:

- `src_ms` is the microservice that originates the message.
- `dst_ms` is the microservice to which the message is destined.
- `event_type` is the type of the operation, either `Application` or `Control`.
- `event_name` is the operation that is being requested, for example,
  create a user, create a file, modify a permission, etc.


## GDE Architecture

### GDE Microservices

The following list contains the event types consumed and produced by
each microservice in GDE:

- Authentication Service

    - Consumes: Authenticate, TokenToCheck, LoginToRequest, PasswordChecked.

    - Produces: PasswordToCheck, TokenChecked, PasswordToCheck,
      AuthenticationTokenReceived.

- User Service

    - Consumes: TokenChecked, PasswordToCheck,
      UserToCreateByNameAndPassword, GroupsToListByLogin,
      UserPermissionChecked.

    - Produces: PasswordChecked, TokenToCheck, UserPermissionToCheck,
      GroupsListedByLogin, UserCreated.

- Permission Service
    - Consumes: UserPermissionToCheck, PermissionToCheckByLogin,
      GroupsListedByLogin.

    - Produces: UserPermissionChecked, GroupsToListByLogin,
      PermissionCheckedByLogin.

- Project Service

    - Consumes: ProjectToCreate, TokenChecked, FileToAttachInProject,
      FileFoundById, PermissionCheckedByLogin.

    - Produces: TokenToCheck, PermissionToCheckByLogin,
      FileAttachedInProject, ProjectCreated, FileToFindById.

- File Service

    - Consumes: FileToCreate, TokenChecked, FileToFindById,
      PermissionCheckedByLogin.

    - Produces: TokenToCheck, PermissionToCheckByLogin, FileCreated,
      FileFoundById.

- Frontend

    - Consumes: AuthenticationTokenReceived, UserCreated,
      ProjectCreated, FileAttachedInProject, FileCreated.

    - Produces: Authenticate, UserToCreateByNameAndPassword,
      ProjectToCreate, FileToCreate, FileToAttachInProject.

### Generic architecture of a service

All GDE microservices share a common structure. They also use the
common modules `amqp-handler` and `message-handler` to make them as
light as possible. Details about these modules can be found below.

Each microservice contains 4 sub-modules:
- `main`: the main program–i.e., the microservice entry-point—that
  configures the communication with the AMQP broker, initialises
  connections to databases, and starts consuming messages.

- `core`: implements two functions (`HandleApplicationMessage` and
  `HandleControlMessage`) that handle both type of messages. These two
  functions are passed as arguments to the AMQP Handler when starting
  to consume messages.

- `service`: the application logic of GDE.

- `dsu`: the variables of the microservices related to the DSU algorithm.

- `config`: the configuration of the microservice.

Please note that every microservice is consuming and publishing
messages to the AMQP broker at the same time.

### The `amqp-handler`, `message-handler` and `service-state` modules

In order to factorise as much code as possible to keep microservices
clean and lightweight, we create two common modules used by every
microservice: `amqp-handler` and `message-handler`.

#### `gde/common/amqp-handler` module

The AMQP Handler abstracts all the work done to initialise the
connection to the AMQP broker and the corresponding exchanges. More
precisely, the AMQP Handler structure allows microservices to:

- Connect to the AMQP broker,

- Create a channel linked to the AMQP broker,

- Configure the exchange and message consuming queue,

- Publish messages,

- Consume messages,

- Close the channel and connection.

```golang
// The AMQPHandler structure
type AMQPHandler struct {
	AMQPConnection	*amqp.Connection
	AMQPChannel		*amqp.Channel
	StopConsumption	bool
	exchangeName	string
	exchangeType	string
}

// Initializes an AMQPHandler structure, by creating a connection and a channel to the AMQP broker.
func InitAMQPHandler(
    ip string,
    port string,
    exchangeName string, 
    exchangeType string) *AMQPHandler

// Creates a queue for receiving messages from the AMQP broker.
func (amqpHandler *AMQPHandler) CreateQueue(
    queueName string)

// Binds a existing queue to the provided binding key.
func (amqpHandler *AMQPHandler) BindQueue(
    queueName string,
    bindingKey string)

// Publishes a message to the AMQP broker with the associated routing key.
func (amqpHandler *AMQPHandler) PublishMessage(
    routingKey msghandler.RoutingKey,
    msg msghandler.Message)

// Tells the AMQPHandler to start consuming messages using the specified handlers.
// The consumption will not stop unless AMQPHandler.StopConsumption is set to true.
func (amqpHandler *AMQPHandler) ConsumeMessages(
	queueName string,
	consumerName string, 
	handlerApplication func(
        msghandler.RoutingKey,
        map[string]interface{},
        msghandler.Metadata) 
            (bool, 
            msghandler.RoutingKey, 
            map[string]interface{}, 
            error),
	handlerControl func(
        msghandler.RoutingKey, 
        map[string]interface{}, 
        msghandler.Metadata) 
            (bool, 
            msghandler.RoutingKey, 
            map[string]interface{}, 
            error), 
	DSU *Vars.DSUVars)
```

For a complete documentation about everything contained in the module,
we encourage you to execute the following command, which will print
the Go documentation included in the source code:

```bash
$ (cd GDE/common/amqp-handler; go doc -all)
```

#### Module `gde/common/message-handler` in directory `GDE/common/message-handler/`

The Message Handler module is primarily intended to provide a fixed and
structured format to messages exchanged between microservices. The module
also provides functions and type methods to help building routing keys
and messages more easily. It also handles conversion between the
Message structure and a JSON string in both ways.

```golang
// The routing key structure
type RoutingKey struct {
	Source      	string
	Destination 	string
	MessageType 	string
	EventType   	string
}

// The message structure
type Message struct {
	Metadata   		Metadata
	Content    		map[string]interface{}
}

// The metadata structure
type Metadata struct {
	Algorithms		map[string]map[string]interface{}
	CollaborationID string
}
```

For a complete documentation about everything contained in the module,
we encourage you to execute the following command, which will print
the Go documentation included in the source code:

```bash
$ (cd GDE/common/message-handler/; go doc -all)
```

#### `gde/common/service-state` module

The `service-state` enables microservices to keep a local state for
collaborations. It allows them to keep data about a collaboration in
order to retrieve it later.

The module offers the `ServiceState` structure that enables, for each
collaboration, to keep data related to every event occurring for a
specific service. Each collaboration, which is identified by its
`CollaborationID`, indexes a list of events represented by the
`CollaborationState` structure.

The `CollaborationState` structure is a stack of events (structure of
type `Event`). The stack is implemented using a list and provides
methods in order to manipulate data according to the behaviour of a
stack.

Finally, the `Event` structure represents an event occurring in the
context of a collaboration and allows to keep the name of the event as
well as any arbitrary data that is linked to it in a
`map[string]interface` field.

```golang
type Event struct {
	Name string
	Data map[string]interface{}
}

type CollaborationState struct {
	Events []Event
}

type ServiceState struct {
	Collaborations map[string]CollaborationState
}
```

### The particular case of the frontend

The frontend is different from any other microservice because it
needs to concurrently deal with synchronous HTTP requests from the
users and the asynchronous messages of GDE microservices.

Then, it is necessary to make the synchronous call from the end-user
(outside of GDE) wait for the asynchronous response from GDE
microservices. Practically, we use a channel as described in the code
sample that follows.

```golang
// The structure holding information about a user's request
type ClientRequestsDataStructure struct {
    MessageCounter  int
    EventName       string
    ReceiverName    string
    GoChannel       chan string
    Parameters      []byte
}

func FrontEndHTTPHandler([...]) {
    [...]

    // Upon receiving a user request, we create a new unique collaboration ID.
    collaborationID := config.ConsumerName + eventType + strconv.Itoa(CollaborationSequence)

    // Then, we create a ClientRequestsDataStructure instance identified by the collaboration ID,
    // and we create a channel for the response.
    goChannel := make(chan string)
    ClientRequests[collaborationID] = ClientRequestsDataStructure {
        MessageCounter: 0,
        EventName:      eventType,
        ReceiverName:   destinationService,
        GoChannel:      goChannel,
        Parameters:     body,
    }
    response := <- goChannel

    [...]
}

// Upon receiving the response to the user's request, the frontend "unlocks" the HTTP request by
// delivering a response into the channel.
func FrontEndConsumeHandler([...]) {
    [...]

    responseChannel := ClientRequests[metadata.CollaborationID].GoChannel
    responseChannel <- string(response)

    [...]
}
```
