#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../config.sh



# DELETING PREVIOUS BINARIES

for microservice in ${MICROSERVICES[*]}; do
    if [ -f "$ARBORE_PROJECT_FOLDER/Code/GDE/services/$microservice/$microservice" ]; then
        go clean
    fi
done



# BUILDING BINARIES FOR EACH MICROSERVICE

echo -e "\r${BLUE}[RUNNING]${RESET} Compiling microservices"

COUNT=1
for microservice in ${MICROSERVICES[*]}; do
    echo -e "  >>  Compiling microservice \"$microservice\""
    COUNT=$((COUNT+1))

    cd $ARBORE_PROJECT_FOLDER/Code/GDE/services/$microservice
    go build
done

echo -e "  >>  Compiling Autonomic Manager"
cd $ARBORE_PROJECT_FOLDER/$AUTONOMIC_MANAGER_PATH
go build
COUNT=$((COUNT+1))

up $COUNT
clear_line
echo -ne "\r${GREEN}[SUCCESS]${RESET} Compiling microservices\r"
down $COUNT
