#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../config.sh



# STARTING MICROSERVICES

echo -e "\r${BLUE}[RUNNING]${RESET} Stopping microservices"

COUNT=1
cd $ARBORE_PROJECT_FOLDER/Code/ARBORE/DSU/autonomic-manager
for microservice in ${MICROSERVICES_NAMES[*]}; do
    COUNT=$((COUNT+1))
    echo -e "  >>  Stopping microservice \"$microservice\""
    timeout 1 ./autonomic-manager delete-microservice $microservice &> /dev/null
done

up $COUNT
clear_line
echo -ne "\r${GREEN}[SUCCESS]${RESET} Stopping microservices\r"
down $COUNT

# Killing services in case some are still running
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/kill-services.sh