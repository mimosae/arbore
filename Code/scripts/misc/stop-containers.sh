#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../config.sh



# STOPPING & REMOVING DOCKER CONTAINERS IF THEY ARE RUNNING

echo -e "\r${BLUE}[RUNNING]${RESET} Stopping & removing containers"

COUNT=1

if netstat -nlp 2> /dev/null | grep -q "5672"; then
    COUNT=$((COUNT+1))
    echo -e "  >>  Stopping & removing RabbitMQ broker container"
    docker stop $RABBITMQ_CONTAINER_NAME #>> /dev/null
    docker rm $RABBITMQ_CONTAINER_NAME #>> /dev/null
fi
if docker ps -a | grep $RABBITMQ_CONTAINER_NAME; then
    COUNT=$((COUNT+1))
    echo -e "  >>  Removing RabbitMQ broker container"
    docker stop $RABBITMQ_CONTAINER_NAME #>> /dev/null
    docker rm $RABBITMQ_CONTAINER_NAME #>> /dev/null
fi

if netstat -nlp 2> /dev/null | grep -q "5432"; then
    COUNT=$((COUNT+1))
    echo -e "  >>  Stopping & removing PostgreSQL container"
    echo docker stop $POSTGRESQL_CONTAINER_NAME
    docker stop $POSTGRESQL_CONTAINER_NAME #>> /dev/null
    docker rm $POSTGRESQL_CONTAINER_NAME #>> /dev/null
fi
if docker ps -a | grep $POSTGRESQL_CONTAINER_NAME; then
    COUNT=$((COUNT+1))
    echo -e "  >>  Removing PostgreSQL container"
    docker stop $POSTGRESQL_CONTAINER_NAME #>> /dev/null
    docker rm $POSTGRESQL_CONTAINER_NAME #>> /dev/null
fi

up $COUNT
clear_line
echo -ne "\r${GREEN}[SUCCESS]${RESET} Stopping & removing containers\r"
down $COUNT