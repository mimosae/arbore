#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../config.sh



# DELETING BINARIES

echo -e "\r${BLUE}[RUNNING]${RESET} Cleaning microservices binaries"

COUNT=1
for microservice in ${MICROSERVICES[*]}; do
    echo -e "  >>  Cleaning binary for microservice \"$microservice\""
    COUNT=$((COUNT+1))
    cd $ARBORE_PROJECT_FOLDER/Code/GDE/services/$microservice
    go clean
done

echo -e "  >>  Cleaning binary for Autonomic Manager"
cd $ARBORE_PROJECT_FOLDER/$AUTONOMIC_MANAGER_PATH
go clean
COUNT=$((COUNT+1))

up $COUNT
clear_line
echo -ne "\r${GREEN}[SUCCESS]${RESET} Cleaning microservices binaries\r"
down $COUNT
