#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../config.sh



# Here we kill the services if they are still running
# Because the name of the services is truncated to 16 characters, we truncate their name
for microservice in ${MICROSERVICES[*]}; do
    PROCNAME="${microservice:0:15}"

    if ps | grep -q "$PROCNAME"; then
        pkill $PROCNAME
    fi
done
