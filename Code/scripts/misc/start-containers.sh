#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../config.sh



# STARTING CONTAINERS

echo -e "\r${BLUE}[RUNNING]${RESET} Starting containers"

# Start RabbitMQ broker
echo -e "  >>  Starting RabbitMQ broker container"
docker run -itd --name $RABBITMQ_CONTAINER_NAME -p 5672:5672 -p 15672:15672 rabbitmq:3.10-management #>> /dev/null
sleep 5 
# Start & initialize PostgreSQL
echo -e "  >>  Starting PostgreSQL container"
docker run --name $POSTGRESQL_CONTAINER_NAME -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=arboredb -d postgres:latest #>> /dev/null
sleep 5
docker exec -it $POSTGRESQL_CONTAINER_NAME psql -d arboredb -U postgres -c "CREATE DATABASE gde_user_service" #>> /dev/null
docker exec -it $POSTGRESQL_CONTAINER_NAME psql -d arboredb -U postgres -c "CREATE DATABASE gde_file_service" #>> /dev/null
docker exec -it $POSTGRESQL_CONTAINER_NAME psql -d arboredb -U postgres -c "CREATE DATABASE gde_permission_service" #>> /dev/null
docker exec -it $POSTGRESQL_CONTAINER_NAME psql -d arboredb -U postgres -c "CREATE DATABASE gde_project_service" #>> /dev/null
docker exec -it $POSTGRESQL_CONTAINER_NAME psql -d arboredb -U postgres -c "CREATE DATABASE gde_events" #>> /dev/null

echo -e "  >>  Waiting for containers to spin up"
sleep 5

up 4
clear_line
echo -ne "\r${GREEN}[SUCCESS]${RESET} Starting containers\r"
down 4
