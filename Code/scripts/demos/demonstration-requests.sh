#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../config.sh



# MAIN SCRIPT

echo -e "${BOLD}==============================  DEMONSTRATION - VARIOUS REQUESTS  ==============================\n${RESET}"

$ARBORE_PROJECT_FOLDER/Code/scripts/misc/compile.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-containers.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/start-containers.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/start-services.sh

echo -e "\r${BLUE}[RUNNING]${RESET} Running various application requests"

echo -ne "  >>  Getting authentication token: "
TOKEN=$($ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/authenticate.sh localhost 8080 POST test test)
echo -e $TOKEN

echo -ne "  >>  Creating test user: "
RESULT=$($ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/create-user.sh localhost 8080 POST $TOKEN user0 password0)
echo -e $RESULT

echo -ne "  >>  Creating test project: "
RESULT=$($ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/create-project.sh localhost 8080 POST $TOKEN project0)
echo -e $RESULT

echo -ne "  >>  Creating test file: "
RESULT=$($ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/create-file.sh localhost 8080 POST $TOKEN file0)
echo -e $RESULT

echo -ne "  >>  Attaching file to project: "
RESULT=$($ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/attach-file-in-project.sh localhost 8080 POST $TOKEN 1 1)
echo -e $RESULT

up 6
clear_line
echo -ne "\r${GREEN}[SUCCESS]${RESET} Running various application requests\r"
down 6

$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-services.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/clean.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-containers.sh
