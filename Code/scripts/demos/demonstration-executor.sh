#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../config.sh



# MAIN SCRIPT

echo -e "${BOLD}==============================  DEMONSTRATION - AUTONOMIC MANAGER EXECUTOR  ==============================\n${RESET}"

$ARBORE_PROJECT_FOLDER/Code/scripts/misc/compile.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-containers.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/start-containers.sh



echo -e "\r${BLUE}[RUNNING]${RESET} Starting up GDE services using plan at 'arbore/dsu/plan-parser/examples/start-gde.txt'"

cd $ARBORE_PROJECT_FOLDER/$AUTONOMIC_MANAGER_PATH; ./autonomic-manager run-plan $ARBORE_PROJECT_FOLDER/Code/ARBORE/DSU/plan-parser/examples/start-gde.txt



echo -e "\r${BLUE}[RUNNING]${RESET} Shutting down GDE services using plan at 'arbore/dsu/plan-parser/examples/stop-gde.txt'"

cd $ARBORE_PROJECT_FOLDER/$AUTONOMIC_MANAGER_PATH; ./autonomic-manager run-plan $ARBORE_PROJECT_FOLDER/Code/ARBORE/DSU/plan-parser/examples/stop-gde.txt


$ARBORE_PROJECT_FOLDER/Code/scripts/misc/kill-services.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/clean.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-containers.sh
