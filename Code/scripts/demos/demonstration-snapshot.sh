#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../config.sh



# MAIN SCRIPT

echo -e "${BOLD}==============================  DEMONSTRATION - SNAPSHOT ALGORITHM  ==============================\n${RESET}"

$ARBORE_PROJECT_FOLDER/Code/scripts/misc/compile.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-containers.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/start-containers.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/start-services.sh

echo -e "\r${BLUE}[RUNNING]${RESET} Snapshot algorithm demonstration"

echo -e "  >>  Sending an authentication request to the front-end in order to test snapshots"
TOKEN=$($ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/authenticate.sh localhost 8080 POST test test)

echo -e "  >>  Sending snapshot request to Autonomic Manager (this snapshot should only show a terminated collaboration for the previous authentication)"
cd $ARBORE_PROJECT_FOLDER/$AUTONOMIC_MANAGER_PATH
MAP=$(.//autonomic-manager take-snapshot 2> >(grep "map"))
echo -e "  >>  Snapshot content: $MAP"

echo -e "  >>  Sending a file creation request to the front-end"
$ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/create-file.sh localhost 8080 POST $TOKEN file0 >> /dev/null &

echo -e "  >>  Sending snapshot request to Autonomic Manager (this snapshot should show a currently running collaboration for the file creation)"
cd $ARBORE_PROJECT_FOLDER/$AUTONOMIC_MANAGER_PATH
MAP=$(.//autonomic-manager take-snapshot 2> >(grep "map"))
echo -e "  >>  Snapshot content: $MAP"

up 7
clear_line
echo -ne "\r${GREEN}[SUCCESS]${RESET} Snapshot algorithm demonstration\r"
down 7

$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-services.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/clean.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-containers.sh
