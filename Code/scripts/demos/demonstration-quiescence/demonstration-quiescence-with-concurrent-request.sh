#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../../config.sh



# MAIN SCRIPT

echo -e "${BOLD}==============================  DEMONSTRATION - QUIESCENCE  ==============================\n${RESET}"

$ARBORE_PROJECT_FOLDER/Code/scripts/misc/compile.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-containers.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/start-containers.sh

echo -e "\r${BLUE}[RUNNING]${RESET} Quiescence condition demonstration"

echo -e "  >>  Starting up GDE services using plan at 'arbore/dsu/plan-parser/examples/start-gde.txt'"
cd $ARBORE_PROJECT_FOLDER/$AUTONOMIC_MANAGER_PATH
./autonomic-manager run-plan $ARBORE_PROJECT_FOLDER/Code/ARBORE/DSU/plan-parser/examples/start-gde.txt

echo -e "  >>  Sending an authentication request"
TOKEN=$($ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/authenticate.sh localhost 8080 POST test test ) 

echo -e "  >>  Sending a file creation request"
$ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/create-file.sh localhost 8080 POST $TOKEN file0 &  > /tmp/result.txt 
pidcreationfile=$!

echo -e "  >>  Starting DSU"
cd $ARBORE_PROJECT_FOLDER/$AUTONOMIC_MANAGER_PATH &  
./autonomic-manager DSU-start &
pidDSUStart=$!
sleep 2
echo -e "  >>  Sending a project creation request"
result=$($ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/create-project.sh localhost 8080 POST $TOKEN project0 ) # > /tmp/result2.txt 
#pidcreationproject=$! 
#sleep 1 
#echo -e "  >>  Sending a project creation request"
#wait $pidcreationfile
#cat /tmp/result.txt 
#$ARBORE_PROJECT_FOLDER/Code/scripts/testing/requests/create-project.sh localhost 8080 POST $TOKEN project0 > /tmp/result2.txt
#pidcreationproject= $!
#wait $pidcreationproject
#cat /tmp/result2.txt 

echo -e "\r${GREEN}[SUCCESS]${RESET} Quiescence condition demonstration\r"

$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-services.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/clean.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-containers.sh
