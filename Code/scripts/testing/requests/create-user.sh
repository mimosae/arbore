#!/bin/bash



if (($# != 6)); then
    echo "usage: $0 [hostname] [port] [method] [authentication token] [new username] [new password]"
    exit
fi



REQUEST_PATH=createuser
REQUEST_ARGS="{\"Token\":\"$4\", \"NewUserName\":\"$5\", \"NewPassword\":\"$6\"}"

RESPONSE=$(curl -sX "$3" "$1":"$2"/"$REQUEST_PATH" -H "Content-Type: application/json" -d "$REQUEST_ARGS")
ID=$(echo $RESPONSE | tr [,{}] '\n' | grep -E user_id | tr '"' '\n' | sed -n '3 p' | sed 's/^://')



echo "User created with ID $ID"
