if (($# != 4)); then
    echo "usage: $0 [hostname] [port] [method] [authentication token]"
    exit
fi



REQUEST_PATH=accountability
REQUEST_ARGS="{\"Token\":\"$4\"}"

RESPONSE=$(curl -sX "$3" "$1":"$2"/"$REQUEST_PATH" -H "Content-Type: application/json" -d "$REQUEST_ARGS")
RESULT=$(echo $RESPONSE | tr [,{}] '\n' | grep -E Result | tr '"' '\n' | sed -n '4 p')



echo $RESULT