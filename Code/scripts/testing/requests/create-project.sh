#!/bin/bash



if (($# != 5)); then
    echo "usage: $0 [hostname] [port] [method] [authentication token] [project name]"
    exit
fi



REQUEST_PATH=createproject
REQUEST_ARGS="{\"Token\":\"$4\", \"ProjectName\":\"$5\"}"

RESPONSE=$(curl -sX "$3" "$1":"$2"/"$REQUEST_PATH" -H "Content-Type: application/json" -d "$REQUEST_ARGS")
RESULT=$(echo $RESPONSE | tr [,{}] '\n' | grep -E Result | tr '"' '\n' | sed -n '4 p')



echo $RESULT
