#!/bin/bash



if (($# != 6)); then
    echo "usage: $0 [hostname] [port] [method] [authentication token] [file ID] [project ID]"
    exit
fi



REQUEST_PATH=fileattachinproject
REQUEST_ARGS="{\"Token\":\"$4\", \"ProjectID\":\"$5\", \"FileID\":\"$6\"}"

RESPONSE=$(curl -sX "$3" "$1":"$2"/"$REQUEST_PATH" -H "Content-Type: application/json" -d "$REQUEST_ARGS")
RESULT=$(echo $RESPONSE | tr [,{}] '\n' | grep -E Result | tr '"' '\n' | sed -n '4 p')



echo $RESULT

