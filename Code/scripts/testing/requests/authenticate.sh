#!/bin/bash



if (($# != 5)); then
    echo "usage: $0 [hostname] [port] [method] [username] [password]"
    exit
fi



REQUEST_PATH=authenticate
REQUEST_ARGS="{\"UserName\":\"$4\", \"Password\":\"$5\"}"

RESPONSE=$(curl -sX "$3" "$1":"$2"/"$REQUEST_PATH" -H "Content-Type: application/json" -d "$REQUEST_ARGS")
TOKEN=$(echo $RESPONSE | tr [,{}] '\n' | grep -E Token | tr '"' '\n' | sed -n '4 p')



echo $TOKEN
