# Testing Scripts

This directory contains scripts that are primarly intended for
debugging purposes:

- `run-services.sh` launches each service in a separate terminal
  window, allowing us to see their respective log messages.

- `requests` directory contains small scripts to send HTTP requests to
  the front-end.

## HTTP requests

To make HTTP requests to the fron-end, you can use the small scripts
provided in the `requests` directory.  Please ensure services are
running before you try to make requests.

For instance, run the following commands:

```bash
# Get an authentication token for the "test" user
$ TOKEN=$(./requests/authenticate.sh localhost 8080 POST test test)

# Create a file named "file0"
$ ./requests/create-file.sh localhost 8080 POST $TOKEN "file0"
File file0 was created
```

Here follows a complete list of the possible requests and their
parameters:

```bash
# Always get a token first using ./authenticate.sh, then use it to request something else
./requests./accountability.sh <hostname> <port> <method> <token>
./requests./attache-file-in-project.sh <hostname> <port> <method> <token> <project-id> <file-id>
./requests./authenticate.sh <hostname> <port> <method> <username> <password>
./requests./create-file.sh <hostname> <port> <method> <token> <file-name>
./requests./create-project.sh <hostname> <port> <method> <token> <project-name>
./requests./create-user.sh <hostname> <port> <method> <token> <new-username> <new-password>
```
