#!/bin/bash



# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$(dirname $(readlink -f $0))

# Retrieving common script configuration
source $SCRIPT_DIR/../config.sh



echo -e "${BOLD}==============================  TESTING - MICROSERVICES  ==============================\n${RESET}"

$ARBORE_PROJECT_FOLDER/Code/scripts/misc/compile.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-containers.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/start-containers.sh



# START MICROSERVICES IN SEPARATE TERMINAL FOR LOGS LOOKUP

echo -e "\r${BLUE}[RUNNING]${RESET} Starting microservices"
for microservice in ${MICROSERVICES[*]}; do
    cd $ARBORE_PROJECT_FOLDER/Code/GDE/services/$microservice
    gnome-terminal -- ./$microservice &
done

echo -e "  >>  Press ENTER to stop all the services"

read

up 3
clear_line
echo -e "\r${GREEN}[SUCCESS]${RESET} Starting microservices\r"
down 1

$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-services.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/stop-containers.sh
$ARBORE_PROJECT_FOLDER/Code/scripts/misc/clean.sh
