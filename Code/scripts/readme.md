# ARBORE Scripts

This folder contains several scripts in order to easily interact with
ARBORE.

There exist scripts for using ARBORE and GDE, e.g. start
microservices, other scripts for testing particular situations or GDE
requests; and finally some scripts for demonstrating in the context of
scenarios, e.g. dynamic software updating with the quiescence
update condition.

## Suggested usage of the scripts

We suggest you to follow the following sequence.

The `Code/scripts/config.sh` script contains common shell variables
for the scripts. You should not run it on its own.

### Launching ARBORE

Run the following commands:

```bash
$ # First, we compile the microservices in order to obtain runnable binaries
$ ./scripts/misc/compile.sh

$ # Then, we start the required containers (RabbitMQ broker and PostgreSQL)
$ ./misc/start-containers.sh

$ # Finally, we launch the microservices
$ ./misc/start-services.sh
```

### Making requests to the microservices

We provide small scripts to help you make requests to the
system. These scripts are located in the `testing/requests`
directory.

Please note that you need to specify the host-name and port number of
the front-end HTTP server, as well as the HTTP method. In the current
configuration of the project, the values are "localhost", 8080 and
"POST", respectively.

For instance, run the following commands:

```bash
$ # We obtain an authentication token for the user "test" with password "test"
$ TOKEN=$(./testing/requests/authenticate.sh localhost 8080 POST test test)

$ # Using the retrieved token, we request to create the project "project0"
$ ./testing/requests/create-project.sh localhost 8080 POST $TOKEN project0
```

For more details, please see the `readme.md` file located in the
`scripts/testing` directory.

### Gracefully shutting down ARBORE

After you've played a bit with ARBORE, you will probably want to shut
it down cleanly.  You can do so by following these steps:

```bash
$ # Stopping all microservices
$ ./misc/stop-services.sh

$ # Stopping the Docker containers
$ ./misc/stop-containers.sh

$ # Cleaning up the microservices' binaries
$ ./misc/clean.sh
```

### Using the demonstration scripts

The demonstration scripts ensure everything is done properly from
starting up ARBORE to shutting it down cleanly.  You can use them to
see some typical example of how the DSU algorithm works.

## Content description

### `demos` scripts

These are scripts to demonstrate the implementation of the DSU
(Dynamic Software Update) algorithms. They take care of starting the
required Docker containers, compiling the binaries of the
microservices and launching them, before executing a predetermined
sequence of actions in order to demonstrate a property.

See the corresponding [readme file](demos/readme.md).

### `misc` scripts

These small scripts are often utilised in bigger scripts in order to
factorise common tasks like compiling microservices, starting and
stopping Docker containers, etc.

### `testing` scripts

These scripts are a way to test the application at a small scale.

- `run-services.sh` compiles microservices, starts Docker containers
  and launches separately each microservice.

- `requests` directory contains several small scripts to run requests
  against the front-end. See the `readme.md` file in the `testing`
  directory for more details.

See the corresponding [readme file](testing/readme.md).
