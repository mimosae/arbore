


# This nice one-liner gets the directory in which the current script is located
# Source: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )



# PROJECT-RELATED VARIABLES

# Project root
export ARBORE_PROJECT_FOLDER="$SCRIPT_DIR/../.."

# List of all microservices
export MICROSERVICES=("front-end" "authentication-service" "user-service" "permission-service" "project-service" "file-service" "accountability-service" "events-logger-service")
export MICROSERVICES_NAMES=("FrontEnd" "AuthenticationService" "UserService" "PermissionService" "ProjectService" "FileService" "AccountabilityService" "EventsLoggerService")

export AUTONOMIC_MANAGER_PATH="Code/ARBORE/DSU/autonomic-manager"

# List of containers' names
export RABBITMQ_CONTAINER_NAME="rabbitmq"
export POSTGRESQL_CONTAINER_NAME="postgresql"



# CONSTANTS FOR TERMINAL DISPLAY

GREEN='\033[1;32m'
RED='\033[1;31m'
BLUE='\033[1;34m'
RESET='\033[0m'

BOLD='\033[1m'

clear_line() { echo -ne "\033[2K"; }
up() { echo -ne "\033[""$1""A"; }
down() { echo -ne "\033[""$1""B"; }

export -f clear_line up down
