package main

import (
	"gde/services/events-logger-service/config"
	"gde/services/events-logger-service/core"
	"gde/services/events-logger-service/db"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"

	_ "github.com/jinzhu/gorm/dialects/postgres"

	"log"
)

func StartDB() {
	db.DB = &db.EventsPostgresDB{}
	db.DB.ConnectDB()
	db.DB.InitDB()
}

func StopDB() {
	db.DB.Close()
}

func StartConsumption() {
	core.AMQPHandler = amqp.InitAMQPHandler(config.DefaultIP, config.DefaultPort, config.ExchangeName, config.ExchangeType)
	core.AMQPHandler.CreateQueue(config.QueueName)
	core.AMQPHandler.BindQueue(config.QueueName, "#")

	log.Printf("[SERVICE %s] Start consumption of messages", config.ServiceName)
	go core.AMQPHandler.ConsumeMessages(config.QueueName, config.ServiceName, core.HandleApplicationMessage, core.HandleControlMessage, nil, nil)
}

func StopConsumption() {
	log.Printf("[SERVICE %s] Stopping", config.ServiceName)
	core.AMQPHandler.Close()
}

func main() {
	log.Printf("[SERVICE %s] Starting", config.ServiceName)

	defer StopDB()
	StartDB()

	defer StopConsumption()
	StartConsumption()

	<-core.AMQPHandler.Ready
	// Publish a message to the Autonomic Manager to confirm service startup
	routingKey := msghandler.BuildRoutingKey(config.ServiceName, services.AUTONOMIC_MANAGER, "Control", "MicroserviceStarted")
	msg := msghandler.BuildMessage(msghandler.Metadata{CollaborationID: "", Algorithms: make(map[string]map[string]interface{})}, make(map[string]interface{}))
	core.AMQPHandler.PublishMessage(routingKey, msg)
	<-core.AMQPHandler.StopService
}
