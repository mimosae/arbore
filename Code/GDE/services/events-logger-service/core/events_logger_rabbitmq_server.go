package core

import (
	"encoding/json"
	"gde/common/gde-common/events"
	"gde/services/events-logger-service/chain"
	"gde/services/events-logger-service/config"
	"gde/services/events-logger-service/db"
	"log"

	amqp "gde/common/amqp-handler"
	msghandler "gde/common/message-handler"
)

var AMQPHandler *amqp.AMQPHandler

func HandleApplicationMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	var eventPosition int
	evts, err := db.DB.FindEventsByCorrelationID(metadata.CollaborationID)
	if err != nil || len(evts) == 0 {
		log.Printf("collaboration id %v not yet existed...\n", metadata.CollaborationID)
		eventPosition = 0 //event is the first with this collaboration id
	} else {
		eventPosition = len(evts) //add to the end of event chain
	}
	body, _ := json.Marshal(content)
	event := chain.Event{
		CorrelationID: metadata.CollaborationID,
		EventPosition: eventPosition,
		EventType:     routingKey.EventType,
		RoutingKey:    routingKey.ToString(),
		EventBody:     body,
	}
	db.DB.InsertEvent(&event)
	log.Println(event)

	newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
	newContent := make(map[string]interface{})

	return false, newRoutingKey, newContent, nil
}

func HandleControlMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	switch routingKey.EventType {
	case "DeleteMicroservice":
		if routingKey.Destination != config.ServiceName {
			return false, msghandler.BuildRoutingKey("", "", "", ""), make(map[string]interface{}), nil
		}

		AMQPHandler.StopConsumption = true

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", "DeletedMicroservice")
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil
	case events.DSU_START:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", events.DSU_START_ACKNOWLEDGED)
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil

	default:
		log.Printf("[SERVICE %s]Other control messages MUST have managed in function LocalHandler called before this function in ConsumeMessage", config.ServiceName)
		//log.Printf("[SERVICE %s] Received unhandled control message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})

		return false, newRoutingKey, newContent, nil
	}
}
