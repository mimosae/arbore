package chain

import "fmt"

type Event struct {
	ID            int64  `json:"event_id" gorm:"primary_key;AUTO_INCREMENT"`
	CorrelationID string `json:"correlation_id"` //client_name + '_' + transaction_number
	EventPosition int    `json:"event_position"`
	EventType     string `json:"event_type"`
	RoutingKey    string `json:"routing_key"`
	EventBody     []byte `json:"event_body"`
}

func (evt Event) String() string {
	return fmt.Sprintf("Event [ID:%v, CorrelationID:%v, EventPosition:%v, EventType:%v, RoutingKey:%v, EventBody:%v]",
		evt.ID, evt.CorrelationID, evt.EventPosition, evt.EventType, evt.RoutingKey, string(evt.EventBody))
}
