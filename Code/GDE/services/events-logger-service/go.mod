module gde/services/events-logger-service

go 1.16

require (
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
)
