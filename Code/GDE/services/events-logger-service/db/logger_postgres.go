package db

import (
	"gde/services/events-logger-service/chain"

	"github.com/jinzhu/gorm"

	"fmt"
	"log"
)

type EventsPostgresDB struct {
	db *gorm.DB
}

func (pg *EventsPostgresDB) GetDB() *gorm.DB {
	return pg.db
}

//ConnectDB connect to postgres database
func (pg *EventsPostgresDB) ConnectDB() {
	var err error
	dbURI := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		"postgres",   //user
		"postgres",   //password
		"127.0.0.1",  //ip
		"5432",       //port
		"gde_events", // name
	)
	pg.db, err = gorm.Open("postgres", dbURI)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Logger Postgres opened ...")
}

//InitDB ...
func (pg *EventsPostgresDB) InitDB() {
	if err := pg.db.DropTableIfExists(&chain.Event{}).Error; err != nil {
		log.Fatal("dropTableIfExist fails: ", err)
	}

	if err := pg.db.AutoMigrate(&chain.Event{}).Error; err != nil {
		log.Fatal("automigrate fails: ", err)
	}
}

//Close postgres database
func (pg *EventsPostgresDB) Close() {
	pg.db.Close()
}

func (pg *EventsPostgresDB) InsertEvent(evt *chain.Event) (*chain.Event, error) {
	//log.Println("Creating Events ...")
	if err := pg.db.Create(&evt).Error; err != nil {
		return nil, fmt.Errorf("insert into db fail: %v", err)
	}
	return evt, nil
}

func (pg *EventsPostgresDB) DeleteEvent(id int64) error {
	log.Println("Deleting Events ...")
	if err := pg.db.Where("id = ?", id).Delete(&chain.Event{}).Error; err != nil {
		return fmt.Errorf("delete from db fail: %v", err)
	}
	return nil
}

func (pg *EventsPostgresDB) FindEventByID(id int64) (chain.Event, error) {
	log.Printf("Finding event %v... \n", id)
	evt := chain.Event{}
	if err := pg.db.Where(&chain.Event{ID: id}).Take(&evt).Error; err != nil {
		log.Fatal("Events not found")
		return chain.Event{}, err
	}
	return evt, nil
}

func (pg *EventsPostgresDB) FindEventsByCorrelationID(correlationID string) ([]chain.Event, error) {
	//log.Printf("Finding events by %v... \n", correlationID)
	var evts []chain.Event
	if err := pg.db.Where(&chain.Event{CorrelationID: correlationID}).Find(&evts).Error; err != nil {
		log.Fatal("Events not found")
		return nil, err
	}
	return evts, nil
}

func (pg *EventsPostgresDB) FindEventByCorrelationIDAndPosition(correlationID string, position int) (chain.Event, error) {
	log.Printf("Finding event by %v and %v... \n", correlationID, position)
	evt := chain.Event{}
	if err := pg.db.Where(&chain.Event{CorrelationID: correlationID, EventPosition: position}).Take(&evt).Error; err != nil {
		log.Fatal("Events not found")
		return chain.Event{}, err
	}
	return evt, nil
}

func (pg *EventsPostgresDB) FindEventsByCorrelationIDBeforePosition(correlationID string, position int) ([]chain.Event, error) {
	log.Printf("Finding events by %v before %v... \n", correlationID, position)
	var evts []chain.Event
	if err := pg.db.Where(&chain.Event{CorrelationID: correlationID}).Find(&evts, "event_position < ?", position).Error; err != nil {
		log.Fatal("Events not found")
		return nil, err
	}
	return evts, nil
}

func (pg *EventsPostgresDB) FindEventsByCorrelationIDAfterPosition(correlationID string, position int) ([]chain.Event, error) {
	log.Printf("Finding events by %v before %v... \n", correlationID, position)
	var evts []chain.Event
	if err := pg.db.Where(&chain.Event{CorrelationID: correlationID}).Find(&evts, "event_position > ?", position).Error; err != nil {
		log.Fatal("Events not found")
		return nil, err
	}
	return evts, nil
}

func (pg *EventsPostgresDB) FindEventsByEventType(eventType string) ([]chain.Event, error) {
	log.Printf("Finding events by %v... \n", eventType)
	var evts []chain.Event
	if err := pg.db.Where(&chain.Event{EventType: eventType}).Find(&evts).Error; err != nil {
		log.Fatal("Events not found")
		return nil, err
	}
	return evts, nil
}

func (pg *EventsPostgresDB) FindEventsByRoutingKey(routingKey string) ([]chain.Event, error) {
	log.Printf("Finding events by %v... \n", routingKey)
	var evts []chain.Event
	if err := pg.db.Where(&chain.Event{RoutingKey: routingKey}).Find(&evts).Error; err != nil {
		log.Fatal("Events not found")
		return nil, err
	}
	return evts, nil
}
