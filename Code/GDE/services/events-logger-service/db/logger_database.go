package db

import (
	"gde/services/events-logger-service/chain"
)

var DB EventsDatabase

type EventsDatabase interface {
	ConnectDB()
	InitDB()
	Close()

	InsertEvent(evt *chain.Event) (*chain.Event, error)
	DeleteEvent(id int64) error
	FindEventByID(id int64) (chain.Event, error)
	FindEventsByCorrelationID(correlationID string) ([]chain.Event, error)
	FindEventByCorrelationIDAndPosition(correlationID string, position int) (chain.Event, error)
	FindEventsByCorrelationIDBeforePosition(correlationID string, position int) ([]chain.Event, error)
	FindEventsByCorrelationIDAfterPosition(correlationID string, position int) ([]chain.Event, error)
	FindEventsByEventType(eventType string) ([]chain.Event, error)
	FindEventsByRoutingKey(routingKey string) ([]chain.Event, error)
}
