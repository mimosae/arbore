package config

import (
	"gde/common/gde-common/services"
)

// Queue name for this microservice
const QueueName = "project_queue"

// Service name
const ServiceName = services.PROJECT_SERVICE

// Default IP of AMQP broker
const DefaultIP = "localhost"

// Default port of AMQP broker
const DefaultPort = "5672"

// Global AMQP exchange name
const ExchangeName = "gde_exchange"

// Global AMQP exchange type
const ExchangeType = "topic"

// Database Host
const DBHost = "localhost"

// Database Port
const DBPort = "5432"

// Database Name
const DBName = "gde_project_service"

// Database User
const DBUser = "postgres"

// Database Password
const DBPassword = "postgres"
