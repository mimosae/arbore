package entities

import (
	"gde/common/gde-common/types"

	"github.com/lib/pq"

	"time"
)

type Project struct {
	ID           int64             `json:"project_id" gorm:"primary_key;AUTO_INCREMENT"`
	Name         string            `json:"project_name"`
	Attributes   []types.Attribute `json:"project_attributes" gorm:"type:jsonb"`
	Deleted      bool              `json:"deleted"`
	CreatedAt    time.Time         `json:"created_at"`
	UpdatedAt    time.Time         `json:"updated_at"`
	DeletedAt    *time.Time        `json:"deleted_at"`
	RestoratedAt time.Time         `json:"restorated_at"`
	Files        pq.Int64Array     `json:"files" gorm:"type:integer[]"` //id
	Locker       int64             `json:"locker" gorm:"default:-1"`
}
