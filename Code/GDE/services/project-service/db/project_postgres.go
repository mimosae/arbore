package db

import (
	"gde/services/project-service/config"
	"gde/services/project-service/entities"

	"gde/common/gde-common/types"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"

	"fmt"
	"os"
	"time"
)

//ProjectPostgresDB repository of postgresql database
type ProjectPostgresDB struct {
	db *gorm.DB
}

//GetDB ...
func (pg *ProjectPostgresDB) GetDB() *gorm.DB {
	return pg.db
}

//ConnectDB connect to postgres database
func (pg *ProjectPostgresDB) ConnectDB() {
	var err error
	dbHost := os.Getenv("DBHOST")
	if dbHost == "" {
		dbHost = config.DBHost
	}
	dbPort := os.Getenv("DBPORT")
	if dbPort == "" {
		dbPort = config.DBPort
	}
	dbUser := os.Getenv("DBUSER")
	if dbUser == "" {
		dbUser = config.DBUser
	}
	dbName := os.Getenv("DBNAME")
	if dbName == "" {
		dbName = config.DBName
	}
	dbPassword := os.Getenv("DBPASSWORD")
	if dbPassword == "" {
		dbPassword = config.DBPassword
	}
	dbURI := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		dbUser,
		dbPassword,
		dbHost,
		dbPort,
		dbName)
	pg.db, err = gorm.Open("postgres", dbURI)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Postgres opened ...")
}

//InitDB ...
func (pg *ProjectPostgresDB) InitDB() {
	if err := pg.db.DropTableIfExists(&entities.Project{}).Error; err != nil {
		log.Fatal("dropTableIfExist fails: ", err)
	}

	if err := pg.db.AutoMigrate(&entities.Project{}).Error; err != nil {
		log.Fatal("automigrate fails: ", err)
	}

}

//Close postgres database
func (pg *ProjectPostgresDB) Close() {
	pg.db.Close()
}

/* project */

//FindProjects ...
func (pg *ProjectPostgresDB) FindProjects() ([]entities.Project, error) {
	log.Println("Listing All Projects ...")
	var list []entities.Project
	if err := pg.db.Find(&list).Error; err != nil {
		log.Errorf("empty project list")
	}
	return list, nil
}

//InsertProject ...
func (pg *ProjectPostgresDB) InsertProject(p entities.Project) (*entities.Project, error) {
	log.Println("Creating Project ...")
	if err := pg.db.Create(&p).Error; err != nil {
		return nil, fmt.Errorf("insert into db fail: %v", err)
	}
	return &p, nil
}

//FindProjectByID ...
func (pg *ProjectPostgresDB) FindProjectByID(id int64) (entities.Project, error) {
	log.Printf("Finding project %v... \n", id)
	p := entities.Project{}
	if err := pg.db.Where(&entities.Project{ID: id}).Take(&p).Error; err != nil {
		log.Errorf("Error finding project")
		return entities.Project{}, err
	}
	return p, nil
}

//UpdateProject ...
func (pg *ProjectPostgresDB) UpdateProject(p entities.Project) (*entities.Project, error) {
	log.Printf("Updating Project %v... \n", p.ID)
	old, err := pg.FindProjectByID(p.ID)
	if err != nil {
		return nil, err
	}
	err = pg.db.Model(&old).Updates(p).Error
	return &p, err
}

//DeleteProject ...
func (pg *ProjectPostgresDB) DeleteProject(id int64) error {
	log.Printf("Deleting Project %v... \n", id)
	if err := pg.db.Where("id = ?", id).Delete(&entities.Project{}).Error; err != nil {
		log.Errorf("delete project by id %d error", id)
		return err
	}
	return nil
}

//RestoreProject ...
func (pg *ProjectPostgresDB) RestoreProject(id int64) error {
	log.Printf("Restoring Project %v... \n", id)
	p := entities.Project{}

	if err := pg.db.Unscoped().Where("id = ?", id).Find(&p).Error; err != nil {
		log.Errorf("find deleted project by id %d error", id)
		return err
	}
	p.DeletedAt = nil
	p.RestoratedAt = time.Now()
	if err := pg.db.Unscoped().Save(&p).Error; err != nil {
		log.Errorf("restore project by id %d error", id)
		return err
	}
	return nil
}

//FindProjectsByName ...
func (pg *ProjectPostgresDB) FindProjectsByName(name string) ([]entities.Project, error) {
	log.Printf("Finding project named %v... \n", name)
	var projects []entities.Project
	if err := pg.db.Where(&entities.Project{Name: name}).Find(&projects).Error; err != nil {
		log.Errorf("Error finding project with this name")
		return nil, err
	}
	return projects, nil
}

//FindProjectsByCreationDate ...
func (pg *ProjectPostgresDB) FindProjectsByCreationDate(creationDate *time.Time, comparator types.DateComparator) ([]entities.Project, error) {
	log.Printf("Finding project created at %v... \n", creationDate)
	var projects []entities.Project
	var err error
	switch comparator {
	case types.DATE_EQUAL:
		err = pg.db.Find(&projects, "created_at = ?", creationDate).Error
	case types.DATE_DIFFER:
		err = pg.db.Find(&projects, "created_at <> ?", creationDate).Error
	case types.DATE_GREATER:
		err = pg.db.Find(&projects, "created_at > ?", creationDate).Error
	case types.DATE_LOWER:
		err = pg.db.Find(&projects, "created_at < ?", creationDate).Error
	case types.DATE_GREATER_EQUAL:
		err = pg.db.Find(&projects, "created_at >= ?", creationDate).Error
	case types.DATE_LOWER_EQUAL:
		err = pg.db.Find(&projects, "created_at <= ?", creationDate).Error
	default:
		err = fmt.Errorf("date comparator error")
	}
	if err != nil {
		log.Errorf("Error finding project created on this date")
		return nil, err
	}
	return projects, nil
}

//FindProjectsByDeletionDate ...
func (pg *ProjectPostgresDB) FindProjectsByDeletionDate(deletionDate *time.Time, comparator types.DateComparator) ([]entities.Project, error) {
	log.Printf("Finding project deleted at %v... \n", deletionDate)
	var projects []entities.Project
	var err error
	switch comparator {
	case types.DATE_EQUAL:
		err = pg.db.Find(&projects, "deleted_at = ?", deletionDate).Error
	case types.DATE_DIFFER:
		err = pg.db.Find(&projects, "deleted_at <> ?", deletionDate).Error
	case types.DATE_GREATER:
		err = pg.db.Find(&projects, "deleted_at > ?", deletionDate).Error
	case types.DATE_LOWER:
		err = pg.db.Find(&projects, "deleted_at < ?", deletionDate).Error
	case types.DATE_GREATER_EQUAL:
		err = pg.db.Find(&projects, "deleted_at >= ?", deletionDate).Error
	case types.DATE_LOWER_EQUAL:
		err = pg.db.Find(&projects, "deleted_at <= ?", deletionDate).Error
	default:
		err = fmt.Errorf("date comparator error")
	}
	if err != nil {
		log.Errorf("Error finding project deleted on this date")
		return nil, err
	}
	return projects, nil
}

//FindProjectsByUpdateDate ...
func (pg *ProjectPostgresDB) FindProjectsByUpdateDate(updateDate *time.Time, comparator types.DateComparator) ([]entities.Project, error) {
	log.Printf("Finding project updated at %v... \n", updateDate)
	var projects []entities.Project
	var err error
	switch comparator {
	case types.DATE_EQUAL:
		err = pg.db.Find(&projects, "updated_at = ?", updateDate).Error
	case types.DATE_DIFFER:
		err = pg.db.Find(&projects, "updated_at <> ?", updateDate).Error
	case types.DATE_GREATER:
		err = pg.db.Find(&projects, "updated_at > ?", updateDate).Error
	case types.DATE_LOWER:
		err = pg.db.Find(&projects, "updated_at < ?", updateDate).Error
	case types.DATE_GREATER_EQUAL:
		err = pg.db.Find(&projects, "updated_at >= ?", updateDate).Error
	case types.DATE_LOWER_EQUAL:
		err = pg.db.Find(&projects, "updated_at <= ?", updateDate).Error
	default:
		err = fmt.Errorf("date comparator error")
	}
	if err != nil {
		log.Errorf("Error finding project updated on this date")
		return nil, err
	}
	return projects, nil
}

//FindProjectsByRestorationDate ...
func (pg *ProjectPostgresDB) FindProjectsByRestorationDate(restorationDate *time.Time, comparator types.DateComparator) ([]entities.Project, error) {
	log.Printf("Finding project restorated at %v... \n", restorationDate)
	var projects []entities.Project
	var err error
	switch comparator {
	case types.DATE_EQUAL:
		err = pg.db.Find(&projects, "restorated_at = ?", restorationDate).Error
	case types.DATE_DIFFER:
		err = pg.db.Find(&projects, "restorated_at <> ?", restorationDate).Error
	case types.DATE_GREATER:
		err = pg.db.Find(&projects, "restorated_at > ?", restorationDate).Error
	case types.DATE_LOWER:
		err = pg.db.Find(&projects, "restorated_at < ?", restorationDate).Error
	case types.DATE_GREATER_EQUAL:
		err = pg.db.Find(&projects, "restorated_at >= ?", restorationDate).Error
	case types.DATE_LOWER_EQUAL:
		err = pg.db.Find(&projects, "restorated_at <= ?", restorationDate).Error
	default:
		err = fmt.Errorf("date comparator error")
	}
	if err != nil {
		log.Errorf("Error finding project updated on this date")
		return nil, err
	}
	return projects, nil
}

//FindProjectLocker ...
func (pg *ProjectPostgresDB) FindProjectLocker(projectId int64) (int64, error) {
	log.Printf("Finding project %v's locker ... \n", projectId)
	p, err := pg.FindProjectByID(projectId)
	if err != nil {
		return -1, err
	}
	return p.Locker, nil
}

//IsProjectLocked ...
func (pg *ProjectPostgresDB) IsProjectLocked(userId, projectId int64) (bool, error) {
	log.Printf("Verifying if project %v is locked ... \n", projectId)
	locker, err := pg.FindProjectLocker(projectId)
	if err != nil {
		return true, err
	}
	if locker >= 0 && locker != userId {
		return true, nil
	}
	return false, nil
}

func (pg *ProjectPostgresDB) SetProjectLockState(userId int64, projectId int64, locked bool) error {
	log.Printf("Set project %v lock state ... \n", projectId)
	p, _ := pg.FindProjectByID(projectId)
	if p.Locker >= 0 && p.Locker != userId {
		return fmt.Errorf("project %v is locked", projectId)
	}
	if locked {
		p.Locker = userId
	} else {
		p.Locker = -1
	}
	if _, err := pg.UpdateProject(p); err != nil {
		return err
	}
	return nil
}
