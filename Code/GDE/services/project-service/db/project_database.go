package db

import (
	"gde/services/project-service/entities"

	"gde/common/gde-common/types"

	"time"
)

var Database ProjectDatabase

type ProjectDatabase interface {
	ConnectDB()
	InitDB()
	Close()

	InsertProject(p entities.Project) (*entities.Project, error)
	FindProjects() ([]entities.Project, error)
	FindProjectByID(id int64) (entities.Project, error)
	FindProjectsByName(name string) ([]entities.Project, error)
	FindProjectsByCreationDate(creationDate *time.Time, comparator types.DateComparator) ([]entities.Project, error)
	FindProjectsByDeletionDate(deletionDate *time.Time, comparator types.DateComparator) ([]entities.Project, error)
	FindProjectsByUpdateDate(updateDate *time.Time, comparator types.DateComparator) ([]entities.Project, error)
	FindProjectsByRestorationDate(restorationDate *time.Time, comparator types.DateComparator) ([]entities.Project, error)
	UpdateProject(p entities.Project) (*entities.Project, error)
	RestoreProject(id int64) error
	DeleteProject(id int64) error
	FindProjectLocker(projectId int64) (int64, error)
	IsProjectLocked(userId, projectId int64) (bool, error)
	SetProjectLockState(userId int64, projectId int64, locked bool) error
}
