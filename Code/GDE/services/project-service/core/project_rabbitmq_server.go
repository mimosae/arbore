package core

import (
	"gde/services/project-service/config"
	"gde/services/project-service/entities"
	"gde/services/project-service/service"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/events"
	"gde/common/gde-common/methods"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"
	servicestate "gde/common/service-state"

	"encoding/json"
	"fmt"
	"log"
	"strconv"
)

var AMQPHandler *amqp.AMQPHandler

var State *servicestate.ServiceState

func HandleApplicationMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	var ps service.ProjectService = &service.ProjectServiceImpl{}

	switch routingKey.EventType {
	case events.PROJECT_TO_CREATE:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.AUTHENTICATION_SERVICE, "Application", events.TOKEN_TO_CHECK)

		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]

		data := map[string]interface{}{
			"Name":                     content["ProjectName"],
			"ServiceName":              services.PROJECT_SERVICE,
			"MethodIndex":              methods.CREATE_PROJECT,
			"CreateProjectEventSender": routingKey.Source,
		}
		State.SaveCollaborationState(metadata.CollaborationID, routingKey.EventType, data)
		log.Printf("[Project-Service] save data")

		return true, newRoutingKey, newContent, nil

	case events.TOKEN_CHECKED:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.PERMISSION_SERVICE, "Application", events.PERMISSION_TO_CHECK_BY_LOGIN)

		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]
		newContent["Login"] = content["Login"]

		return true, newRoutingKey, newContent, nil

	case events.FILE_TO_ATTACH_IN_PROJECT:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.AUTHENTICATION_SERVICE, "Application", events.TOKEN_TO_CHECK)

		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]

		var projectIDFileIDMap = make(map[string]interface{})
		projectIDFileIDMap["ProjectID"] = content["ProjectID"]
		projectIDFileIDMap["FileID"] = content["FileID"]
		projectIDFileID, _ := json.Marshal(projectIDFileIDMap)

		data := map[string]interface{}{
			"FileToAttachInProjectMessageBody": string(projectIDFileID),
			"ServiceName":                      services.PROJECT_SERVICE,
			"MethodIndex":                      methods.ATTACH_FILE_TO_PROJECT,
			"FileToAttachInProjectEventSender": routingKey.Source,
		}
		State.SaveCollaborationState(metadata.CollaborationID, routingKey.EventType, data)
		log.Printf("[Project-Service] save data")

		return true, newRoutingKey, newContent, nil

	case events.FILE_FOUND_BY_ID:
		event := State.RemoveLastCollaborationEvent(metadata.CollaborationID)
		if content["Result"].(string) == "true" {
			eventTypeNumber := event.Data["EventTypeNumber"]
			switch {
			case eventTypeNumber == methods.ATTACH_FILE_TO_PROJECT:
				projectIDFileID := event.Data["FileToAttachInProjectMessageBody"].(string)
				var projectIDFileIDMap = make(map[string]interface{})
				if err := json.Unmarshal([]byte(projectIDFileID), &projectIDFileIDMap); err != nil {
					log.Fatalf("decode command error")
				}
				projectID, _ := strconv.Atoi(projectIDFileIDMap["ProjectID"].(string))
				fileID, _ := strconv.Atoi(projectIDFileIDMap["FileID"].(string))
				err := ps.AttachFileToProject(int64(projectID), int64(fileID))
				var result string
				if err != nil {
					result = fmt.Sprintf("Error attaching file to project: %v", err)
				} else {
					result = fmt.Sprintf("File was attached to Project")
				}
				receiver := event.Data["FileToAttachInProjectEventSender"].(string)
				newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, receiver, "Application", events.FILE_ATTACHED_IN_PROJECT)

				newContent := make(map[string]interface{})
				newContent["Token"] = content["Token"]
				newContent["Result"] = result

				return true, newRoutingKey, newContent, nil
			}
		} else {
			receiver := event.Data["FileToAttachInProjectEventSender"].(string)

			newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, receiver, "Application", events.FILE_ATTACHED_IN_PROJECT)

			newContent := make(map[string]interface{})
			newContent["Token"] = content["Token"]
			newContent["Result"] = "Error: File not found!"

			return true, newRoutingKey, newContent, nil
		}

	case events.PERMISSION_CHECKED_BY_LOGIN:
		if content["PermissionChecked"].(bool) {
			event := State.RemoveLastCollaborationEvent(metadata.CollaborationID)
			switch event.Data["MethodIndex"] {
			case methods.CREATE_PROJECT:

				name := event.Data["Name"].(string)

				project := entities.Project{Name: name}
				r, err := ps.CreateProject(project)
				log.Printf("CREATED PROJECT ID: %d\n", r.ID)
				var result string
				if err != nil {
					result = fmt.Sprintf("Error creating project %v: %v", name, err)
				} else {
					result = fmt.Sprintf("Project %v was created", name)
				}

				receiver := event.Data["CreateProjectEventSender"].(string)

				newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, receiver, "Application", events.PROJECT_CREATED)

				newContent := make(map[string]interface{})
				newContent["Token"] = content["Token"]
				newContent["Result"] = result

				return true, newRoutingKey, newContent, nil

			case methods.ATTACH_FILE_TO_PROJECT:
				projectIDFileID := event.Data["FileToAttachInProjectMessageBody"].(string)

				var projectIDFileIDMap = make(map[string]interface{})
				err := json.Unmarshal([]byte(projectIDFileID), &projectIDFileIDMap)
				if err != nil {
					log.Printf("Error unmarshaling information: %v", err)
				}

				newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.FILE_SERVICE, "Application", events.FILE_TO_FIND_BY_ID)

				newContent := make(map[string]interface{})
				newContent["Token"] = content["Token"]
				newContent["FileID"] = projectIDFileIDMap["FileID"].(string)

				log.Printf("[project service] %s", projectIDFileIDMap["FileID"].(string))
				data := event.Data
				data["EventTypeNumber"] = methods.ATTACH_FILE_TO_PROJECT
				State.SaveCollaborationState(metadata.CollaborationID, routingKey.EventType, data)

				return true, newRoutingKey, newContent, nil
			}
		}

	default:
		log.Printf("[SERVICE %s] Received unhandled application message", config.ServiceName)
	}

	newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
	newContent := make(map[string]interface{})

	return false, newRoutingKey, newContent, nil
}

func HandleControlMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	switch routingKey.EventType {
	case "DeleteMicroservice":
		AMQPHandler.StopConsumption = true

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", "DeletedMicroservice")
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil
	case events.DSU_START:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", events.DSU_START_ACKNOWLEDGED)
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil

	default:
		log.Printf("[SERVICE %s]Other control messages MUST have managed in function LocalHandler called before this function in ConsumeMessage", config.ServiceName)
		//log.Printf("[SERVICE %s] Received unhandled control message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})

		return false, newRoutingKey, newContent, nil
	}
}
