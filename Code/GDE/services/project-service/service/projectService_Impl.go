package service

import (
	"gde/services/project-service/db"
	"gde/services/project-service/entities"

	"gde/common/gde-common/types"

	"fmt"
	"log"
	"time"
)

//ProjectServiceImpl ...
type ProjectServiceImpl struct{}

//ListProjects ...
func (ps *ProjectServiceImpl) ListProjects() ([]entities.Project, error) {
	return db.Database.FindProjects()
}

//CreateProject ...
func (ps *ProjectServiceImpl) CreateProject(p entities.Project) (*entities.Project, error) {
	return db.Database.InsertProject(p)
}

//FindProjectById ...
func (ps *ProjectServiceImpl) FindProjectById(id int64) (entities.Project, error) {
	return db.Database.FindProjectByID(id)
}

//UpdateProject ...
func (ps *ProjectServiceImpl) UpdateProject(p entities.Project) (*entities.Project, error) {
	return db.Database.UpdateProject(p)
}

//DeleteProject ...
func (ps *ProjectServiceImpl) DeleteProject(id int64) error {
	return db.Database.DeleteProject(id)
}

//RestoreProject ...
func (ps *ProjectServiceImpl) RestoreProject(id int64) error {
	return db.Database.RestoreProject(id)
}

//FindProjectsByName ...
func (ps *ProjectServiceImpl) FindProjectsByName(name string) ([]entities.Project, error) {
	return db.Database.FindProjectsByName(name)
}

//FindProjectsByCreationDate ...
func (ps *ProjectServiceImpl) FindProjectsByCreationDate(creationDate *time.Time, comparator types.DateComparator) ([]entities.Project, error) {
	return db.Database.FindProjectsByCreationDate(creationDate, comparator)
}

//FindProjectsByDeletionDate ...
func (ps *ProjectServiceImpl) FindProjectsByDeletionDate(deletionDate *time.Time, comparator types.DateComparator) ([]entities.Project, error) {
	return db.Database.FindProjectsByDeletionDate(deletionDate, comparator)
}

//FindProjectsByUpdateDate ...
func (ps *ProjectServiceImpl) FindProjectsByUpdateDate(updateDate *time.Time, comparator types.DateComparator) ([]entities.Project, error) {
	return db.Database.FindProjectsByUpdateDate(updateDate, comparator)
}

//FindProjectsByRestorationDate ...
func (ps *ProjectServiceImpl) FindProjectsByRestorationDate(restorationDate *time.Time, comparator types.DateComparator) ([]entities.Project, error) {
	return db.Database.FindProjectsByRestorationDate(restorationDate, comparator)
}

//GetProjectLocker ...
func (ps *ProjectServiceImpl) GetProjectLocker(projectId int64) (int64, error) {
	return db.Database.FindProjectLocker(projectId)
}

//IsProjectLocked ...
func (ps *ProjectServiceImpl) IsProjectLocked(userId, projectId int64) (bool, error) {
	return db.Database.IsProjectLocked(userId, projectId)
}

func (ps *ProjectServiceImpl) SetProjectLockState(userId int64, projectId int64, locked bool) error {
	return db.Database.SetProjectLockState(userId, projectId, locked)
}

////ListProjectFiles ...
//func (ps *ProjectServiceImpl) ListProjectFiles(projectId int64, token string) ([]common.GDEFileTO, error) {
//	log.Printf("Listing project %v's files\n", projectId)
//	p, err := db.Database.FindProjectByID(projectId)
//	if err != nil {
//		return nil, err
//	}
//	files := make([]common.GDEFileTO, 0)
//	for _, fileID := range p.Files {
//		file, err := GetFileById(fileID, token)
//		if err != nil {
//			return nil, err
//		}
//		files = append(files, file)
//	}
//	return files, nil
//}

//AttachFileToProject ...
func (ps *ProjectServiceImpl) AttachFileToProject(projectId int64, fileId int64) error {
	p, err := db.Database.FindProjectByID(projectId)
	if err != nil {
		return err
	}

	for _, f := range p.Files {
		if f == fileId {
			return fmt.Errorf("Already attached")
		}
	}
	p.Files = append(p.Files, fileId)
	_, err = db.Database.UpdateProject(p)
	if err != nil {
		panic(err)
	}
	log.Printf("Attach file %v to project %v\n", fileId, projectId)
	return nil
}

////DetachFileFromProject ...
//func (ps *ProjectServiceImpl) DetachFileFromProject(userId int64, projectId int64, fileId int64, token string) error {
//	test, err := db.Database.IsProjectLocked(userId, projectId)
//	if err != nil {
//		return err
//	}
//	if test {
//		return fmt.Errorf("This project is Locked")
//	}
//	p, err := db.Database.FindProjectByID(projectId)
//	if err != nil {
//		return err
//	}
//	ok, err := CheckFileIsValid(fileId, token)
//	if err != nil || !ok {
//		log.Printf("check file is valid error")
//		return err
//	}
//	if ok {
//		for i, file := range p.Files {
//			if fileId == file {
//				p.Files[i] = p.Files[len(p.Files)-1]
//				p.Files = p.Files[:len(p.Files)-1]
//				_, err = db.Database.UpdateProject(p)
//				if err != nil {
//					panic(err)
//				}
//				log.Printf("Detach file %v from project %v\n", fileId, projectId)
//				return nil
//			}
//		}
//	} else {
//		return fmt.Errorf("This file does not exist")
//	}
//	return nil
//}
