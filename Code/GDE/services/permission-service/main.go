package main

import (
	"gde/services/permission-service/config"
	"gde/services/permission-service/core"
	"gde/services/permission-service/db"
	"gde/services/permission-service/dsu"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"
	servicestate "gde/common/service-state"

	"log"
)

func StartDB() {
	db.Database = &db.PermissionPostgresDB{}
	db.Database.ConnectDB()
	db.Database.InitDB()
}

func StopDB() {
	db.Database.Close()
}

func StartMicroservice() {
	core.AMQPHandler = amqp.InitAMQPHandler(config.DefaultIP, config.DefaultPort, config.ExchangeName, config.ExchangeType)
	core.AMQPHandler.CreateQueue(config.QueueName)
	core.AMQPHandler.BindQueue(config.QueueName, "*."+config.ServiceName+".*.*")

	core.State = servicestate.InitServiceState()

	log.Printf("[SERVICE %s] Start consumption of messages", config.ServiceName)
	go core.AMQPHandler.ConsumeMessages(config.QueueName, config.ServiceName, core.HandleApplicationMessage, core.HandleControlMessage, dsu.Handlers, dsu.CommonVariables)
}

func StopMicroservice() {
	log.Printf("[SERVICE %s] Stopping", config.ServiceName)
	core.AMQPHandler.Close()
}

func main() {
	log.Printf("[SERVICE %s] Starting", config.ServiceName)

	defer StopDB()
	defer StopMicroservice()

	StartDB()
	StartMicroservice()

	<-core.AMQPHandler.Ready
	// Publish a message to the Autonomic Manager to confirm service startup
	routingKey := msghandler.BuildRoutingKey(config.ServiceName, services.AUTONOMIC_MANAGER, "Control", "MicroserviceStarted")
	msg := msghandler.BuildMessage(msghandler.Metadata{CollaborationID: "", Algorithms: make(map[string]map[string]interface{})}, make(map[string]interface{}))
	core.AMQPHandler.PublishMessage(routingKey, msg)
	<-core.AMQPHandler.StopService
}
