package db

import (
	"gde/services/permission-service/entities"

	"gde/common/gde-common/methods"
	"gde/common/gde-common/services"

	log "github.com/sirupsen/logrus"
)

const TestGroupID = 1

func InitPermissionDatabase(pg *PermissionPostgresDB) {
	log.Printf("Init Permission Database ...")

	authService := &entities.Service{
		Name:        services.AUTHENTICATION_SERVICE,
		Description: "authentication service",
	}
	permissionService := &entities.Service{
		Name:        services.PERMISSION_SERVICE,
		Description: "permission service",
	}
	userService := &entities.Service{
		Name:        services.USER_SERVICE,
		Description: "user service",
	}
	projectService := &entities.Service{
		Name:        services.PROJECT_SERVICE,
		Description: "project service",
	}
	fileService := &entities.Service{
		Name:        services.FILE_SERVICE,
		Description: "file service",
	}
	authService, _ = pg.CreateService(authService)
	permissionService, _ = pg.CreateService(permissionService)
	userService, _ = pg.CreateService(userService)
	projectService, _ = pg.CreateService(projectService)
	fileService, _ = pg.CreateService(fileService)

	// init method
	method1 := &entities.Method{
		MethodIndex: methods.CREATE_USER,
		Name:        "CreateUser",
		Description: "create user",
		ServiceID:   userService.ID,
	}
	method2 := &entities.Method{
		MethodIndex: methods.FIND_USER_BY_NAME,
		Name:        "Find_user_by_name",
		Description: "find user by name",
		ServiceID:   userService.ID,
	}
	method3 := &entities.Method{
		MethodIndex: methods.LIST_GROUPS_OF_USER,
		Name:        "List_groups_of_user",
		Description: "list groups of user",
		ServiceID:   userService.ID,
	}
	method4 := &entities.Method{
		MethodIndex: methods.HAS_GROUP_THE_PERMISSION_ON,
		Name:        "Has_group_permission",
		Description: "has group permission",
		ServiceID:   permissionService.ID,
	}
	method5 := &entities.Method{
		MethodIndex: methods.HAS_USER_THE_PERMISSION_ON,
		Name:        "Has_user_permission",
		Description: "has user permission",
		ServiceID:   permissionService.ID,
	}
	method6 := &entities.Method{
		MethodIndex: methods.DELETE_USER,
		Name:        "DeleteUser",
		Description: "delete user",
		ServiceID:   userService.ID,
	}
	method7 := &entities.Method{
		MethodIndex: methods.FIND_USER_BY_ID,
		Name:        "FindUserById",
		Description: "Find User By Id",
		ServiceID:   userService.ID,
	}
	method8 := &entities.Method{
		MethodIndex: methods.LIST_USERS,
		Name:        "ListUsers",
		Description: "List Users",
		ServiceID:   userService.ID,
	}
	method9 := &entities.Method{
		MethodIndex: methods.CREATE_GROUP,
		Name:        "CreateGroup",
		Description: "Create Group",
		ServiceID:   userService.ID,
	}
	method10 := &entities.Method{
		MethodIndex: methods.DELETE_GROUP,
		Name:        "DeleteGroup",
		Description: "Delete Group",
		ServiceID:   userService.ID,
	}
	method11 := &entities.Method{
		MethodIndex: methods.FIND_GROUP_BY_ID,
		Name:        "FindGroupByID",
		Description: "Find Group by ID",
		ServiceID:   userService.ID,
	}
	method12 := &entities.Method{
		MethodIndex: methods.FIND_GROUP_BY_NAME,
		Name:        "FindGroupByName",
		Description: "Find Group by Name",
		ServiceID:   userService.ID,
	}
	method13 := &entities.Method{
		MethodIndex: methods.LIST_GROUPS,
		Name:        "ListGroups",
		Description: "List Groups",
		ServiceID:   userService.ID,
	}
	method14 := &entities.Method{
		MethodIndex: methods.ADD_USER_TO_GROUP,
		Name:        "AddUserToGroup",
		Description: "Add User to Group",
		ServiceID:   userService.ID,
	}
	method15 := &entities.Method{
		MethodIndex: methods.REMOVE_USER_FROM_GROUP,
		Name:        "RemoveUserFromGroup",
		Description: "Remove User From Group",
		ServiceID:   userService.ID,
	}
	method16 := &entities.Method{
		MethodIndex: methods.IS_USER_IN_GROUP,
		Name:        "IsUserInGroup",
		Description: "Is User in Group",
		ServiceID:   userService.ID,
	}
	method17 := &entities.Method{
		MethodIndex: methods.LIST_USERS_IN_GROUP,
		Name:        "ListUsersInGroup",
		Description: "List Users in Group",
		ServiceID:   userService.ID,
	}
	method18 := &entities.Method{
		MethodIndex: methods.LIST_GROUPS_OF_USER,
		Name:        "ListGroupsOfUser",
		Description: "List Groups of User",
		ServiceID:   userService.ID,
	}
	method19 := &entities.Method{
		MethodIndex: methods.CREATE_PROJECT,
		Name:        "CreateProject",
		Description: "Create Project",
		ServiceID:   projectService.ID,
	}
	method20 := &entities.Method{
		MethodIndex: methods.CREATE_FILE,
		Name:        "CreateFile",
		Description: "Create File",
		ServiceID:   fileService.ID,
	}
	method21 := &entities.Method{
		MethodIndex: methods.CREATE_CHUNK,
		Name:        "CreateChunk",
		Description: "Create Chunk",
		ServiceID:   fileService.ID,
	}
	method22 := &entities.Method{
		MethodIndex: methods.ATTACH_CHUNK_TO_FILE,
		Name:        "AttachChunkToFile",
		Description: "Attach Chunk to File",
		ServiceID:   fileService.ID,
	}
	method23 := &entities.Method{
		MethodIndex: methods.ATTACH_FILE_TO_PROJECT,
		Name:        "AttachFileToProject",
		Description: "Attach File to Project",
		ServiceID:   projectService.ID,
	}
	method24 := &entities.Method{
		MethodIndex: methods.FIND_FILE_BY_ID,
		Name:        "FindFileByID",
		Description: "Find File By ID",
		ServiceID:   fileService.ID,
	}

	method1, _ = pg.CreateMethod(method1)
	method2, _ = pg.CreateMethod(method2)
	method3, _ = pg.CreateMethod(method3)
	method4, _ = pg.CreateMethod(method4)
	method5, _ = pg.CreateMethod(method5)
	method6, _ = pg.CreateMethod(method6)
	method7, _ = pg.CreateMethod(method7)
	method8, _ = pg.CreateMethod(method8)
	method9, _ = pg.CreateMethod(method9)
	method10, _ = pg.CreateMethod(method10)
	method11, _ = pg.CreateMethod(method11)
	method12, _ = pg.CreateMethod(method12)
	method13, _ = pg.CreateMethod(method13)
	method14, _ = pg.CreateMethod(method14)
	method15, _ = pg.CreateMethod(method15)
	method16, _ = pg.CreateMethod(method16)
	method17, _ = pg.CreateMethod(method17)
	method18, _ = pg.CreateMethod(method18)
	method19, _ = pg.CreateMethod(method19)
	method20, _ = pg.CreateMethod(method20)
	method21, _ = pg.CreateMethod(method21)
	method22, _ = pg.CreateMethod(method22)
	method23, _ = pg.CreateMethod(method23)
	method24, _ = pg.CreateMethod(method24)

	permission1 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method1.ID,
	}
	permission2 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method2.ID,
	}
	permission3 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method3.ID,
	}
	permission4 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method4.ID,
	}
	permission5 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method5.ID,
	}
	permission6 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method6.ID,
	}
	permission7 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method7.ID,
	}
	permission8 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method8.ID,
	}
	permission9 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method9.ID,
	}
	permission10 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method10.ID,
	}
	permission11 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method11.ID,
	}
	permission12 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method12.ID,
	}
	permission13 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method13.ID,
	}
	permission14 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method14.ID,
	}
	permission15 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method15.ID,
	}
	permission16 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method16.ID,
	}
	permission17 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method17.ID,
	}
	permission18 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method18.ID,
	}
	permission19 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method19.ID,
	}
	permission20 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method20.ID,
	}
	permission21 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method21.ID,
	}
	permission22 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method22.ID,
	}
	permission23 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method23.ID,
	}
	permission24 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method24.ID,
	}

	pg.CreatePermission(permission1)
	pg.CreatePermission(permission2)
	pg.CreatePermission(permission3)
	pg.CreatePermission(permission4)
	pg.CreatePermission(permission5)
	pg.CreatePermission(permission6)
	pg.CreatePermission(permission7)
	pg.CreatePermission(permission8)
	pg.CreatePermission(permission9)
	pg.CreatePermission(permission10)
	pg.CreatePermission(permission11)
	pg.CreatePermission(permission12)
	pg.CreatePermission(permission13)
	pg.CreatePermission(permission14)
	pg.CreatePermission(permission15)
	pg.CreatePermission(permission16)
	pg.CreatePermission(permission17)
	pg.CreatePermission(permission18)
	pg.CreatePermission(permission19)
	pg.CreatePermission(permission20)
	pg.CreatePermission(permission21)
	pg.CreatePermission(permission22)
	pg.CreatePermission(permission23)
	pg.CreatePermission(permission24)

}
