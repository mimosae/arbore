package core

import (
	"gde/services/permission-service/config"
	"gde/services/permission-service/service"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/events"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"
	servicestate "gde/common/service-state"

	"log"
	"strconv"
	"strings"
)

var AMQPHandler *amqp.AMQPHandler

var State *servicestate.ServiceState

func HandleApplicationMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	var ps service.PermissionService = &service.PermissionServiceImpl{}

	switch routingKey.EventType {
	case events.USER_PERMISSION_TO_CHECK:
		serviceName := routingKey.Source
		methodIndex := int(content["MethodIndex"].(float64))
		groupIds := strings.Split(content["GroupIds"].(string), ",")
		result, err := ps.CheckGroupsPermission(groupIds, serviceName, methodIndex)

		if err != nil {
			log.Printf("Error checking permission: \"%v\"", err)
			// TODO: Send a message to the request sender to signal the error in permission checking
		} else {
			newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.USER_SERVICE, "Application", events.USER_PERMISSION_CHECKED)

			newContent := make(map[string]interface{})
			newContent["Token"] = content["Token"]
			newContent["PermissionChecked"] = result

			return true, newRoutingKey, newContent, nil
		}

	case events.PERMISSION_TO_CHECK_BY_LOGIN:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.USER_SERVICE, "Application", events.GROUPS_TO_LIST_BY_LOGIN)

		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]
		newContent["Login"] = content["Login"]

		data := make(map[string]interface{})
		data["EventSender"] = routingKey.Source
		data["MethodIndex"] = routingKey.EventType
		State.SaveCollaborationState(metadata.CollaborationID, routingKey.EventType, data)

		return true, newRoutingKey, newContent, nil

	case events.GROUPS_LISTED_BY_LOGIN:
		groupIds := strings.Split(content["GroupIds"].(string), ",")
		event := State.RemoveLastCollaborationEvent(metadata.CollaborationID)
		methodIndex := event.Data["MethodIndex"].(string)
		eventSender := event.Data["EventSender"].(string)
		methodIndexInt, _ := strconv.Atoi(methodIndex)
		result, err := ps.CheckGroupsPermission(groupIds, eventSender, methodIndexInt)

		if err != nil {
			log.Printf("Error checking permission: \"%v\"", err)
			// TODO: Send a message to the request sender to signal the error in permission checking
		} else {
			newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, eventSender, "Application", events.PERMISSION_CHECKED_BY_LOGIN)

			newContent := make(map[string]interface{})
			newContent["Token"] = content["Token"]
			newContent["PermissionChecked"] = result

			return true, newRoutingKey, newContent, nil
		}

	default:
		log.Printf("[SERVICE %s] Received unhandled application message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})
		return false, newRoutingKey, newContent, nil
	}

	newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
	newContent := make(map[string]interface{})

	return false, newRoutingKey, newContent, nil
}

func HandleControlMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	switch routingKey.EventType {
	case "DeleteMicroservice":
		AMQPHandler.StopConsumption = true

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", "DeletedMicroservice")
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil
	case events.DSU_START:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", events.DSU_START_ACKNOWLEDGED)
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil

	default:
		log.Printf("[SERVICE %s]Other control messages MUST have managed in function LocalHandler called before this function in ConsumeMessage", config.ServiceName)
		//log.Printf("[SERVICE %s] Received unhandled control message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})

		return false, newRoutingKey, newContent, nil
	}
}
