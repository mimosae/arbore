package service

import (
	"gde/services/permission-service/db"
	"gde/services/permission-service/entities"

	"log"
	"strconv"
)

type PermissionServiceImpl struct{}

func (ps *PermissionServiceImpl) CreateService(service *entities.Service) (*entities.Service, error) {
	return db.Database.CreateService(service)
}

func (ps *PermissionServiceImpl) DeleteService(id int64) (bool, error) {
	return db.Database.DeleteService(id)
}

func (ps *PermissionServiceImpl) UpdateService(service *entities.Service) (*entities.Service, error) {
	return db.Database.UpdateService(service)
}

func (ps *PermissionServiceImpl) FindServiceByID(id int64) (entities.Service, error) {
	return db.Database.FindServiceByID(id)
}

func (ps *PermissionServiceImpl) FindServiceByName(name string) (entities.Service, error) {
	return db.Database.FindServiceByName(name)
}

func (ps *PermissionServiceImpl) ListServices() ([]entities.Service, error) {
	return db.Database.ListServices()
}

func (ps *PermissionServiceImpl) CreateMethod(method *entities.Method) (*entities.Method, error) {
	return db.Database.CreateMethod(method)
}

func (ps *PermissionServiceImpl) DeleteMethod(id int64) (bool, error) {
	return db.Database.DeleteMethod(id)
}

func (ps *PermissionServiceImpl) UpdateMethod(method *entities.Method) (*entities.Method, error) {
	return db.Database.UpdateMethod(method)
}

func (ps *PermissionServiceImpl) FindMethodByID(id int64) (entities.Method, error) {
	return db.Database.FindMethodByID(id)
}

func (ps *PermissionServiceImpl) FindMethodByName(name string) (entities.Method, error) {
	return db.Database.FindMethodByName(name)
}

func (ps *PermissionServiceImpl) FindMethodByServiceNameMethodIndex(serviceName string, methodIndex int) (entities.Method, error) {
	return db.Database.FindMethodByServiceNameMethodIndex(serviceName, methodIndex)
}

func (ps *PermissionServiceImpl) FindMethodsByService(serviceId int64) ([]entities.Method, error) {
	return db.Database.FindMethodsByService(serviceId)
}

func (ps *PermissionServiceImpl) ListMethods() ([]entities.Method, error) {
	return db.Database.ListMethods()
}

func (ps *PermissionServiceImpl) CreatePermission(permission *entities.Permission) (*entities.Permission, error) {
	return db.Database.CreatePermission(permission)
}

func (ps *PermissionServiceImpl) DeletePermission(id int64) (bool, error) {
	return db.Database.DeletePermission(id)
}

func (ps *PermissionServiceImpl) UpdatePermission(permission *entities.Permission) (*entities.Permission, error) {
	return db.Database.UpdatePermission(permission)
}

func (ps *PermissionServiceImpl) FindPermissionByID(id int64) (entities.Permission, error) {
	return db.Database.FindPermissionByID(id)
}

func (ps *PermissionServiceImpl) CheckGroupsPermission(groupIds []string, serviceName string, methodIndex int) (bool, error) {
	var err error
	for _, groupId := range groupIds {
		gid, _ := strconv.ParseInt(groupId, 10, 64)
		log.Printf("[Permession service ] gid: %T\n", gid)
		log.Printf("[Permession service ] methodIndex: %v, %T \n", methodIndex, methodIndex)
		ok, err := db.Database.HasGroupThePermissionOn(gid, serviceName, methodIndex)
		if err != nil {
			return false, err
		}
		if ok {
			return true, nil
		}
	}
	return false, err
}
