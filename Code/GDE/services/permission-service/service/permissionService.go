package service

import (
	"gde/services/permission-service/entities"
)

type PermissionService interface {
	// Service
	CreateService(service *entities.Service) (*entities.Service, error)
	DeleteService(id int64) (bool, error)
	UpdateService(service *entities.Service) (*entities.Service, error)
	FindServiceByID(id int64) (entities.Service, error)
	FindServiceByName(name string) (entities.Service, error)
	ListServices() ([]entities.Service, error)

	// Method
	CreateMethod(method *entities.Method) (*entities.Method, error)
	DeleteMethod(id int64) (bool, error)
	UpdateMethod(method *entities.Method) (*entities.Method, error)
	FindMethodByID(id int64) (entities.Method, error)
	FindMethodByName(name string) (entities.Method, error)
	FindMethodByServiceNameMethodIndex(serviceName string, methodIndex int) (entities.Method, error)
	FindMethodsByService(serviceId int64) ([]entities.Method, error)
	ListMethods() ([]entities.Method, error)

	// Permission
	CreatePermission(permission *entities.Permission) (*entities.Permission, error)
	DeletePermission(id int64) (bool, error)
	UpdatePermission(permission *entities.Permission) (*entities.Permission, error)
	FindPermissionByID(id int64) (entities.Permission, error)

	CheckGroupsPermission(groupIds []string, serviceName string, methodIndex int) (bool, error)
}
