package entities

import (
	"gde/common/gde-common/types"

	"time"
)

type GDEFile struct {
	ID           int64             `json:"file_id" gorm:"primary_key;AUTO_INCREMENT"`
	Name         string            `json:"file_name"`
	Attributes   []types.Attribute `json:"file_attributes"`
	Size         int64             `json:"file_size"`
	Checksum     string            `json:"file_checksum"`
	Valid        bool              `json:"file_valid"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time //soft delete
	RestoratedAt time.Time  `json:"restorated_at"`
	Indexed      bool       `json:"file_indexed"`
	Chunks       []Chunk    `json:"chunks" gorm:"many2many:file_chunks;"`
}

type Chunk struct {
	ID       int64  `json:"chunk_id" gorm:"primary_key;AUTO_INCREMENT"`
	Rank     int64  `json:"chunk_rank"`
	Checksum string `json:"chunk_checksum"`
	Size     int64  `json:"chunk_size"`
	Data     []byte `json:"chunk_data"`
}

type ChunkInfo struct {
	ID       int64
	Rank     int64
	Checksum string
	Size     int64
}
