package service

import (
	"gde/services/file-service/db"
	"gde/services/file-service/entities"

	"gde/common/gde-common/types"

	log "github.com/sirupsen/logrus"

	"crypto/md5"
	"encoding/hex"
	"fmt"
	"strings"
	"time"
)

type FileServiceImpl struct{}

func (fs *FileServiceImpl) CreateFile(f *entities.GDEFile) (*entities.GDEFile, error) {
	return db.Database.CreateFile(f)
}

func (fs *FileServiceImpl) UpdateFile(f *entities.GDEFile) (*entities.GDEFile, error) {
	if v, err := fs.IsValid(f.ID); !v || err != nil {
		log.Printf("file %v is not valid, update error... \n", f.ID)
		return nil, err
	}
	return db.Database.UpdateFile(f)
}

func (fs *FileServiceImpl) DeleteFile(id int64) error {
	if v, err := fs.IsValid(id); !v || err != nil {
		log.Printf("file %v is not valid, deleted error... \n", id)
		return err
	}
	return db.Database.DeleteFile(id)
}

func (fs *FileServiceImpl) RestoreFile(id int64) error {
	if v, err := fs.IsValid(id); !v || err != nil {
		log.Printf("file %v is not valid, deleted error... \n", id)
		return err
	}
	return db.Database.RestoreFile(id)
}

func (fs *FileServiceImpl) FindFileById(id int64) (entities.GDEFile, error) {
	return db.Database.FindFileById(id)
}

func (fs *FileServiceImpl) ReadFile(id int64) (entities.GDEFile, error) {
	if v, err := fs.IsValid(id); !v || err != nil {
		log.Printf("file %v is not valid, read error... \n", id)
		return entities.GDEFile{}, err
	}
	return db.Database.ReadFile(id)
}

func (fs *FileServiceImpl) FindFilesByName(name string) ([]entities.GDEFile, error) {
	return db.Database.FindFilesByName(name)
}
func (fs *FileServiceImpl) FindFilesByCreationDate(creationDate *time.Time, comparator types.DateComparator) ([]entities.GDEFile, error) {
	return db.Database.FindFilesByCreationDate(creationDate, comparator)
}
func (fs *FileServiceImpl) FindFilesByDeletionDate(deletionDate *time.Time, comparator types.DateComparator) ([]entities.GDEFile, error) {
	return db.Database.FindFilesByDeletionDate(deletionDate, comparator)
}
func (fs *FileServiceImpl) FindFilesByUpdateDate(updateDate *time.Time, comparator types.DateComparator) ([]entities.GDEFile, error) {
	return db.Database.FindFilesByUpdateDate(updateDate, comparator)
}
func (fs *FileServiceImpl) FindFilesByRestorationDate(restorationDate *time.Time, comparator types.DateComparator) ([]entities.GDEFile, error) {
	return db.Database.FindFilesByRestorationDate(restorationDate, comparator)
}
func (fs *FileServiceImpl) ListFiles() ([]entities.GDEFile, error) {
	return db.Database.ListFiles()
}
func (fs *FileServiceImpl) FindFilesToIndex() ([]entities.GDEFile, error) {
	return db.Database.FindFilesToIndex()
}
func (fs *FileServiceImpl) FindFilesDeleted() ([]entities.GDEFile, error) {
	return db.Database.FindFilesDeleted()
}

func (fs *FileServiceImpl) OpenFile(userId int64, fileId int64) (bool, error) { //LockFile
	log.Printf("Opening file %v by user %v... \n", fileId, userId)
	return GetFileLocker().LockFile(userId, fileId)
}

func (fs *FileServiceImpl) CloseFile(userId int64, fileId int64) (bool, error) { //UnlockFile
	log.Printf("Closing file %v by user %v... \n", fileId, userId)
	fs.SetValid(fileId, true)
	return GetFileLocker().UnlockFile(userId, fileId)
}

func (fs *FileServiceImpl) SetValid(fileId int64, isValid bool) error {
	return db.Database.SetValid(fileId, isValid)
}

func (fs *FileServiceImpl) IsValid(fileId int64) (bool, error) {
	return db.Database.IsValid(fileId)
}

func (fs *FileServiceImpl) SetIndexed(fileId int64, isIndexed bool) error {
	return db.Database.SetIndexed(fileId, isIndexed)
}

func (fs *FileServiceImpl) IsIndexed(fileId int64) (bool, error) {
	return db.Database.IsIndexed(fileId)
}

//Chunks
func (fs *FileServiceImpl) CreateChunk(c *entities.Chunk) (*entities.Chunk, error) {
	log.Printf("Checksum...")
	var err error
	check := md5.Sum([]byte(c.Data))
	checkStr := hex.EncodeToString(check[:])
	if c.Checksum == "" {
		err = fmt.Errorf("null checksum")
		return nil, err
	}
	if !strings.EqualFold(checkStr, c.Checksum) {
		err = fmt.Errorf("bad checksum")
		return nil, err
	}
	c.Size = int64(len(c.Data))
	return db.Database.CreateChunk(c)
}

func (fs *FileServiceImpl) ReadChunk(chunkId int64) (entities.Chunk, error) {
	return db.Database.ReadChunk(chunkId)
}

//File-Chunk association
func (fs *FileServiceImpl) AttachChunkToFile(fileId int64, chunkId int64) error {
	var err error
	if err = db.Database.AttachChunkToFile(fileId, chunkId); err != nil {
		return err
	}
	if err = db.Database.SetValid(fileId, false); err != nil {
		return err
	}
	return nil
}

func (fs *FileServiceImpl) DetachChunkFromFile(fileId int64, chunkId int64) error {
	var err error
	if err = db.Database.DetachChunkFromFile(fileId, chunkId); err != nil {
		return err
	}
	if err = db.Database.SetValid(fileId, false); err != nil {
		return err
	}
	return nil
}

func (fs *FileServiceImpl) FindChunksByFileId(fileId int64) ([]entities.Chunk, error) {
	return db.Database.FindChunksByFileId(fileId)
}

func (fs *FileServiceImpl) FindChunksInfoByFileId(fileId int64) ([]entities.ChunkInfo, error) {
	return db.Database.FindChunksInfoByFileId(fileId)
}
