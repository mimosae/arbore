package core

import (
	"gde/services/file-service/config"
	"gde/services/file-service/entities"
	"gde/services/file-service/service"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/events"
	"gde/common/gde-common/methods"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"
	servicestate "gde/common/service-state"

	"fmt"
	"log"
	"strconv"
	"time"
)

var AMQPHandler *amqp.AMQPHandler

var State *servicestate.ServiceState

func HandleApplicationMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	var fs service.FileService = &service.FileServiceImpl{}

	switch routingKey.EventType {
	case events.FILE_TO_CREATE:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.AUTHENTICATION_SERVICE, "Application", events.TOKEN_TO_CHECK)

		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]
		newContent["FileName"] = content["FileName"]

		data := map[string]interface{}{
			"Name":                  content["FileName"],
			"ServiceName":           services.FILE_SERVICE,
			"MethodIndex":           methods.CREATE_USER,
			"CreateFileEventSender": routingKey.Source,
		}
		State.SaveCollaborationState(metadata.CollaborationID, routingKey.EventType, data)

		return true, newRoutingKey, newContent, nil

	case events.TOKEN_CHECKED:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.PERMISSION_SERVICE, "Application", events.PERMISSION_TO_CHECK_BY_LOGIN)

		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]
		newContent["Login"] = content["Login"].(string)

		return true, newRoutingKey, newContent, nil

	case events.FILE_TO_FIND_BY_ID:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.AUTHENTICATION_SERVICE, "Application", events.TOKEN_TO_CHECK)
		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]

		data := map[string]interface{}{
			"FileToFindByIDMessageBody": content["FileID"],
			"ServiceName":               services.FILE_SERVICE,
			"MethodIndex":               methods.FIND_FILE_BY_ID,
			"FileToFindByIDEventSender": routingKey.Source,
		}
		State.SaveCollaborationState(metadata.CollaborationID, routingKey.EventType, data)

		return true, newRoutingKey, newContent, nil

	case events.PERMISSION_CHECKED_BY_LOGIN:

		if content["PermissionChecked"].(bool) {
			event := State.RemoveLastCollaborationEvent(metadata.CollaborationID)

			switch event.Data["MethodIndex"] {
			case methods.CREATE_FILE:
				name := event.Data["Name"].(string)
				receiver := event.Data["CreateFileEventSender"].(string)

				newFile := entities.GDEFile{Name: name}
				r, err := fs.CreateFile(&newFile)
				fmt.Printf("CREATED FILE ID: %d\n", r.ID)
				var result string
				if err != nil {
					result = fmt.Sprintf("Error creating file %v: %v", name, err)
				} else {
					result = fmt.Sprintf("File %v was created", name)
				}
				time.Sleep(5 * time.Second)

				newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, receiver, "Application", events.FILE_CREATED)

				newContent := make(map[string]interface{})
				newContent["Token"] = content["Token"]
				newContent["Result"] = result

				return true, newRoutingKey, newContent, nil

			case methods.FIND_FILE_BY_ID:
				fileID, _ := strconv.Atoi(event.Data["FileToFindByIDMessageBody"].(string))
				receiver := event.Data["FileToFindByIDEventSender"].(string)
				fileIDInt := fileID
				_, err := fs.FindFileById(int64(fileIDInt))
				var result string
				if err != nil {
					result = fmt.Sprintf("Error finding file by id: %v", err)
				} else {
					result = fmt.Sprintf("true")
				}

				newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, receiver, "Application", events.FILE_FOUND_BY_ID)

				newContent := make(map[string]interface{})
				newContent["Token"] = content["Token"]
				newContent["Result"] = result

				return true, newRoutingKey, newContent, nil
			}

		}

	default:
		log.Printf("[SERVICE %s] Received unhandled application message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})

		return false, newRoutingKey, newContent, nil
	}

	newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
	newContent := make(map[string]interface{})

	return false, newRoutingKey, newContent, nil
}

func HandleControlMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	switch routingKey.EventType {
	case "DeleteMicroservice":
		AMQPHandler.StopConsumption = true

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", "DeletedMicroservice")
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil
	case events.DSU_START:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", events.DSU_START_ACKNOWLEDGED)
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil

	default:
		log.Printf("[SERVICE %s]Other control messages MUST have managed in function LocalHandler called before this function in ConsumeMessage", config.ServiceName)
		//log.Printf("[SERVICE %s] Received unhandled control message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})

		return false, newRoutingKey, newContent, nil
	}
}
