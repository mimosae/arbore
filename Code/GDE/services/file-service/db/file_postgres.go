package db

import (
	"gde/services/file-service/config"
	"gde/services/file-service/entities"

	"gde/common/gde-common/types"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //...
	log "github.com/sirupsen/logrus"

	"fmt"
	"os"
	"time"
)

type FilePostgresDB struct {
	db *gorm.DB
}

//GetDB ...
func (pg *FilePostgresDB) GetDB() *gorm.DB {
	return pg.db
}

//ConnectDB connect to postgres database
func (pg *FilePostgresDB) ConnectDB() {
	var err error
	dbHost := os.Getenv("DBHOST")
	if dbHost == "" {
		dbHost = config.DBHost
	}
	dbPort := os.Getenv("DBPORT")
	if dbPort == "" {
		dbPort = config.DBPort
	}
	dbUser := os.Getenv("DBUSER")
	if dbUser == "" {
		dbUser = config.DBUser
	}
	dbName := os.Getenv("DBNAME")
	if dbName == "" {
		dbName = config.DBName
	}
	dbPassword := os.Getenv("DBPASSWORD")
	if dbPassword == "" {
		dbPassword = config.DBPassword
	}
	dbURI := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		dbUser,
		dbPassword,
		dbHost,
		dbPort,
		dbName)
	pg.db, err = gorm.Open("postgres", dbURI)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Postgres opened ...")
}

//InitDB ...
func (pg *FilePostgresDB) InitDB() {
	if err := pg.db.DropTableIfExists(&entities.GDEFile{}, &entities.Chunk{}, "file_chunks").Error; err != nil {
		log.Fatal("dropTableIfExist fails: ", err)
	}
	if err := pg.db.AutoMigrate(&entities.GDEFile{}, &entities.Chunk{}).Error; err != nil {
		log.Fatal("automigrate fails: ", err)
	}
}

//Close postgres database
func (pg *FilePostgresDB) Close() {
	pg.db.Close()
}

/*** File ***/
func (pg *FilePostgresDB) CreateFile(f *entities.GDEFile) (*entities.GDEFile, error) {
	log.Println("Creating File ...")
	if err := pg.db.Create(&f).Error; err != nil {
		return nil, fmt.Errorf("insert file into db fail: %v", err)
	}
	return f, nil
}

func (pg *FilePostgresDB) UpdateFile(f *entities.GDEFile) (*entities.GDEFile, error) {
	log.Println("Updating File ...")
	if err := pg.db.Save(&f).Error; err != nil {
		return nil, fmt.Errorf("update file fail: %v", err)
	}
	return f, nil
}

func (pg *FilePostgresDB) DeleteFile(id int64) error { //soft delete
	log.Printf("Deleting File %v... \n", id)
	if err := pg.db.Delete(&entities.GDEFile{}, id).Error; err != nil {
		log.Errorf("delete file by id %d error", id)
		return err
	}
	return nil
}

func (pg *FilePostgresDB) RestoreFile(id int64) error {
	log.Printf("Restoring File %v... \n", id)
	file := entities.GDEFile{}
	if err := pg.db.Unscoped().Where("id = ?", id).Find(&file).Error; err != nil {
		log.Errorf("find deleted file by id %d error", id)
		return err
	}
	file.DeletedAt = nil
	file.RestoratedAt = time.Now()
	if err := pg.db.Save(&file).Error; err != nil {
		log.Errorf("restore file by id %d error", id)
		return err
	}
	return nil
}

func (pg *FilePostgresDB) FindFileById(id int64) (entities.GDEFile, error) { //without chunks
	log.Printf("Finding file %v... \n", id)
	file := entities.GDEFile{}
	if err := pg.db.First(&file, id).Error; err != nil {
		log.Errorf("Error finding file")
		return entities.GDEFile{}, err
	}
	return file, nil
}

func (pg *FilePostgresDB) ReadFile(id int64) (entities.GDEFile, error) { //with chunks
	log.Printf("Finding file %v... \n", id)
	file := entities.GDEFile{}
	if err := pg.db.Preload("Chunks").First(&file, id).Error; err != nil {
		log.Errorf("Error finding file")
		return entities.GDEFile{}, err
	}
	return file, nil
}

func (pg *FilePostgresDB) FindFilesByName(name string) ([]entities.GDEFile, error) {
	log.Printf("Finding file named %v... \n", name)
	var files []entities.GDEFile
	if err := pg.db.Preload("Chunks").Where(&entities.GDEFile{Name: name}).Find(&files).Error; err != nil {
		log.Errorf("Error finding file with this name")
		return nil, err
	}
	return files, nil
}

func (pg *FilePostgresDB) FindFilesByCreationDate(creationDate *time.Time, comparator types.DateComparator) ([]entities.GDEFile, error) {
	log.Printf("Finding file created at %v... \n", creationDate)
	var files []entities.GDEFile
	var err error
	switch comparator {
	case types.DATE_EQUAL:
		err = pg.db.Find(&files, "created_at = ?", creationDate).Error
	case types.DATE_DIFFER:
		err = pg.db.Find(&files, "created_at <> ?", creationDate).Error
	case types.DATE_GREATER:
		err = pg.db.Find(&files, "created_at > ?", creationDate).Error
	case types.DATE_LOWER:
		err = pg.db.Find(&files, "created_at < ?", creationDate).Error
	case types.DATE_GREATER_EQUAL:
		err = pg.db.Find(&files, "created_at >= ?", creationDate).Error
	case types.DATE_LOWER_EQUAL:
		err = pg.db.Find(&files, "created_at <= ?", creationDate).Error
	default:
		err = fmt.Errorf("date comparator error")
	}
	if err != nil {
		log.Errorf("Error finding file created on this date")
		return nil, err
	}
	return files, nil
}

func (pg *FilePostgresDB) FindFilesByDeletionDate(deletionDate *time.Time, comparator types.DateComparator) ([]entities.GDEFile, error) {
	log.Printf("Finding file deleted at %v... \n", deletionDate)
	var files []entities.GDEFile
	var err error
	switch comparator {
	case types.DATE_EQUAL:
		err = pg.db.Find(&files, "deleted_at = ?", deletionDate).Error
	case types.DATE_DIFFER:
		err = pg.db.Find(&files, "deleted_at <> ?", deletionDate).Error
	case types.DATE_GREATER:
		err = pg.db.Find(&files, "deleted_at > ?", deletionDate).Error
	case types.DATE_LOWER:
		err = pg.db.Find(&files, "deleted_at < ?", deletionDate).Error
	case types.DATE_GREATER_EQUAL:
		err = pg.db.Find(&files, "deleted_at >= ?", deletionDate).Error
	case types.DATE_LOWER_EQUAL:
		err = pg.db.Find(&files, "deleted_at <= ?", deletionDate).Error
	default:
		err = fmt.Errorf("date comparator error")
	}
	if err != nil {
		log.Errorf("Error finding file deleted on this date")
		return nil, err
	}
	return files, nil
}

func (pg *FilePostgresDB) FindFilesByUpdateDate(updateDate *time.Time, comparator types.DateComparator) ([]entities.GDEFile, error) {
	log.Printf("Finding file updated at %v... \n", updateDate)
	var files []entities.GDEFile
	var err error
	switch comparator {
	case types.DATE_EQUAL:
		err = pg.db.Find(&files, "updated_at = ?", updateDate).Error
	case types.DATE_DIFFER:
		err = pg.db.Find(&files, "updated_at <> ?", updateDate).Error
	case types.DATE_GREATER:
		err = pg.db.Find(&files, "updated_at > ?", updateDate).Error
	case types.DATE_LOWER:
		err = pg.db.Find(&files, "updated_at < ?", updateDate).Error
	case types.DATE_GREATER_EQUAL:
		err = pg.db.Find(&files, "updated_at >= ?", updateDate).Error
	case types.DATE_LOWER_EQUAL:
		err = pg.db.Find(&files, "updated_at <= ?", updateDate).Error
	default:
		err = fmt.Errorf("date comparator error")
	}
	if err != nil {
		log.Errorf("Error finding file updated on this date")
		return nil, err
	}
	return files, nil
}

func (pg *FilePostgresDB) FindFilesByRestorationDate(restorationDate *time.Time, comparator types.DateComparator) ([]entities.GDEFile, error) {
	log.Printf("Finding file restorated at %v... \n", restorationDate)
	var files []entities.GDEFile
	var err error
	switch comparator {
	case types.DATE_EQUAL:
		err = pg.db.Find(&files, "restorated_at = ?", restorationDate).Error
	case types.DATE_DIFFER:
		err = pg.db.Find(&files, "restorated_at <> ?", restorationDate).Error
	case types.DATE_GREATER:
		err = pg.db.Find(&files, "restorated_at > ?", restorationDate).Error
	case types.DATE_LOWER:
		err = pg.db.Find(&files, "restorated_at < ?", restorationDate).Error
	case types.DATE_GREATER_EQUAL:
		err = pg.db.Find(&files, "restorated_at >= ?", restorationDate).Error
	case types.DATE_LOWER_EQUAL:
		err = pg.db.Find(&files, "restorated_at <= ?", restorationDate).Error
	default:
		err = fmt.Errorf("date comparator error")
	}
	if err != nil {
		log.Errorf("Error finding file updated on this date")
		return nil, err
	}
	return files, nil
}

func (pg *FilePostgresDB) ListFiles() ([]entities.GDEFile, error) {
	log.Println("Listing All Files ...")
	var files []entities.GDEFile
	if err := pg.db.Preload("Chunks").Find(&files).Error; err != nil {
		log.Errorf("Error listing files")
		return nil, err
	}
	return files, nil
}

func (pg *FilePostgresDB) FindFilesToIndex() ([]entities.GDEFile, error) {
	log.Println("Listing Files to index...")
	var files []entities.GDEFile
	if err := pg.db.Preload("Chunks").Find(&files, "file_indexed = ?", false).Error; err != nil {
		log.Errorf("Error listing files to index")
		return nil, err
	}
	return files, nil
}

func (pg *FilePostgresDB) FindFilesDeleted() ([]entities.GDEFile, error) {
	log.Println("Listing Files deleted...")
	var files []entities.GDEFile
	if err := pg.db.Unscoped().Find(&files, "deleted_at <> ?", nil).Error; err != nil {
		log.Errorf("Error listing files deleted")
		return nil, err
	}
	return files, nil
}

func (pg *FilePostgresDB) SetValid(fileId int64, isValid bool) error {
	log.Printf("Setting File %v valid... \n", fileId)
	file := entities.GDEFile{}
	if err := pg.db.Where("id = ?", fileId).Find(&file).Error; err != nil {
		log.Errorf("find file by id %d error", fileId)
		return err
	}
	file.Valid = isValid
	if err := pg.db.Save(&file).Error; err != nil {
		log.Errorf("set file %d valid error", fileId)
		return err
	}
	return nil
}

func (pg *FilePostgresDB) IsValid(fileId int64) (bool, error) {
	log.Printf("Is File %v valid or not... \n", fileId)
	file := entities.GDEFile{}
	if err := pg.db.Where("id = ?", fileId).Find(&file).Error; err != nil {
		log.Errorf("find file by id %d error", fileId)
		return false, err
	}
	return file.Valid, nil
}

func (pg *FilePostgresDB) SetIndexed(fileId int64, isIndexed bool) error {
	log.Printf("Setting File %v indexed... \n", fileId)
	file := entities.GDEFile{}
	if err := pg.db.Where("id = ?", fileId).Find(&file).Error; err != nil {
		log.Errorf("find file by id %d error", fileId)
		return err
	}
	file.Indexed = isIndexed
	if err := pg.db.Save(&file).Error; err != nil {
		log.Errorf("set file %d indexed error", fileId)
		return err
	}
	return nil
}

func (pg *FilePostgresDB) IsIndexed(fileId int64) (bool, error) {
	log.Printf("Is File %v indexed or not... \n", fileId)
	file := entities.GDEFile{}
	if err := pg.db.Where("id = ?", fileId).Find(&file).Error; err != nil {
		log.Errorf("find file by id %d error", fileId)
		return false, err
	}
	return file.Indexed, nil
}

/*** Chunk ***/
func (pg *FilePostgresDB) CreateChunk(c *entities.Chunk) (*entities.Chunk, error) {
	log.Println("Creating Chunk ...")
	if err := pg.db.Create(&c).Error; err != nil {
		return nil, fmt.Errorf("insert chunk into db fail: %v", err)
	}
	return c, nil
}

func (pg *FilePostgresDB) ReadChunk(chunkId int64) (entities.Chunk, error) {
	log.Printf("Reading chunk %v... \n", chunkId)
	chunk := entities.Chunk{}
	if err := pg.db.Where("id = ?", chunkId).Find(&chunk).Error; err != nil {
		log.Errorf("Error finding file")
		return entities.Chunk{}, err
	}
	return chunk, nil
}

/*** File-Chunk association ***/
func (pg *FilePostgresDB) FindChunksByFileId(fileId int64) ([]entities.Chunk, error) {
	log.Printf("Finding chunks by file %v... \n", fileId)
	file := entities.GDEFile{}
	if err := pg.db.Preload("Chunks").Where(&entities.GDEFile{ID: fileId}).Find(file).Error; err != nil {
		log.Errorf("Error finding file %v", fileId)
		return nil, err
	}
	return file.Chunks, nil
}

func (pg *FilePostgresDB) FindChunksInfoByFileId(fileId int64) ([]entities.ChunkInfo, error) {
	chunks, err := pg.FindChunksByFileId(fileId)
	if err != nil {
		log.Errorf("Error finding chunks by file id %v", fileId)
		return nil, err
	}
	var infos []entities.ChunkInfo
	for _, c := range chunks {
		info := &entities.ChunkInfo{
			ID:       c.ID,
			Rank:     c.Rank,
			Checksum: c.Checksum,
			Size:     c.Size,
		}
		infos = append(infos, *info)
	}
	return infos, nil
}

func (pg *FilePostgresDB) AttachChunkToFile(fileId int64, chunkId int64) error {
	log.Printf("Attaching chunk %v to File %v... \n", chunkId, fileId)
	file, err := pg.FindFileById(fileId)
	if err != nil {
		log.Errorf("Error finding file %v", fileId)
		return err
	}
	chunk, err := pg.ReadChunk(chunkId)
	if err != nil {
		log.Errorf("Error finding chunks %v", chunkId)
		return err
	}
	if err = pg.db.Model(&file).Association("Chunks").Append(chunk).Error; err != nil {
		return err
	}
	return nil
}

func (pg *FilePostgresDB) DetachChunkFromFile(fileId int64, chunkId int64) error {
	log.Printf("Detaching chunk %v from File %v... \n", chunkId, fileId)
	file, err := pg.FindFileById(fileId)
	if err != nil {
		log.Errorf("Error finding file %v", fileId)
		return err
	}
	chunk, err := pg.ReadChunk(chunkId)
	if err != nil {
		log.Errorf("Error finding chunks %v", chunkId)
		return err
	}
	if err = pg.db.Model(&file).Association("Chunks").Delete(chunk).Error; err != nil {
		return err
	}
	return nil
}
