module gde/services/file-service

go 1.18

require (
	github.com/jinzhu/gorm v1.9.16
	github.com/sirupsen/logrus v1.9.0
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/lib/pq v1.1.1 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
