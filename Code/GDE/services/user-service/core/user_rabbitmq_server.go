package core

import (
	"gde/services/user-service/config"
	"gde/services/user-service/service"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/events"
	"gde/common/gde-common/methods"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"
	servicestate "gde/common/service-state"

	"encoding/json"
	"log"
	"strconv"
)

var AMQPHandler *amqp.AMQPHandler

var State *servicestate.ServiceState

func HandleApplicationMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	var us service.UserService = &service.UserServiceImpl{}

	switch routingKey.EventType {
	case events.PASSWORD_TO_CHECK:
		result, err := us.CheckPassword(content["UserName"].(string), content["Password"].(string))
		log.Printf("Result after checking password: %v", result)

		if err != nil {
			log.Printf("Error \"%v\": %v", err, content["UserName"].(string))
			// TODO: Send a message to the request sender to signal that the user does not exists
		} else {
			newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.AUTHENTICATION_SERVICE, "Application", events.PASSWORD_CHECKED)

			newContent := make(map[string]interface{})
			newContent["Token"] = content["Token"]
			newContent["UserName"] = content["UserName"].(string)
			newContent["Password"] = content["Password"].(string)
			newContent["PasswordCheck"] = result

			return true, newRoutingKey, newContent, nil
		}

	case events.USER_TO_CREATE_BY_NAME_PASSWORD:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.AUTHENTICATION_SERVICE, "Application", events.TOKEN_TO_CHECK)

		newContent := make(map[string]interface{})
		newContent["Operation"] = methods.CREATE_USER
		newContent["Token"] = content["Token"]

		data := map[string]interface{}{
			"ServiceName":           services.USER_SERVICE,
			"MethodIndex":           methods.CREATE_USER,
			"CreateUserEventSender": routingKey.Source,
			"UserName":              content["NewUserName"].(string),
			"Password":              content["NewPassword"].(string),
		}
		State.SaveCollaborationState(metadata.CollaborationID, routingKey.EventType, data)

		return true, newRoutingKey, newContent, nil

	case events.TOKEN_CHECKED:
		event := State.RemoveLastCollaborationEvent(metadata.CollaborationID)
		methodIndex := event.Data["MethodIndex"]
		State.SaveCollaborationState(metadata.CollaborationID, events.TOKEN_CHECKED, event.Data)

		user, err := us.FindUserByName(content["Login"].(string))
		if err != nil {
			log.Fatalf("user not found")
		}
		groups, err := us.ListGroupsOfUser(user.ID)
		if err != nil {
			log.Fatalf("groups of user not found")
		}
		groupIds := ""
		for _, group := range groups {
			groupIds = groupIds + strconv.FormatInt(group.ID, 10) + ","
		}

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.PERMISSION_SERVICE, "Application", events.USER_PERMISSION_TO_CHECK)

		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]
		newContent["MethodIndex"] = methodIndex
		newContent["GroupIds"] = groupIds
		log.Printf("[user service %s", groupIds)
		return true, newRoutingKey, newContent, nil

	case events.GROUPS_TO_LIST_BY_LOGIN:
		user, err := us.FindUserByName(content["Login"].(string))
		if err != nil {
			log.Fatalf("user not found")
		}
		groups, err := us.ListGroupsOfUser(user.ID)
		if err != nil {
			log.Fatalf("groups of user not found")
		}
		groupIds := ""
		for _, group := range groups {
			groupIds = groupIds + strconv.FormatInt(group.ID, 10) + ","
		}

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Application", events.GROUPS_LISTED_BY_LOGIN)

		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]
		newContent["GroupIds"] = groupIds

		return true, newRoutingKey, newContent, nil

	case events.USER_PERMISSION_CHECKED:
		if content["PermissionChecked"].(bool) {
			event := State.RemoveLastCollaborationEvent(metadata.CollaborationID)
			switch event.Data["MethodIndex"] {
			case methods.CREATE_USER:
				username := event.Data["UserName"].(string)
				pwd := event.Data["Password"].(string)

				user, _ := us.CreateUser(username, pwd)
				messageBodyQueryResult, _ := json.Marshal(user)
				receiver := event.Data["CreateUserEventSender"].(string)
				newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, receiver, "Application", events.USER_CREATED)

				newContent := make(map[string]interface{})
				newContent["Token"] = content["Token"]
				if err := json.Unmarshal(messageBodyQueryResult, &newContent); err != nil {
					log.Fatalf("decode command error")
				}

				return true, newRoutingKey, newContent, nil
			}
		}

	default:
		log.Printf("[SERVICE %s] Received unhandled application message", config.ServiceName)
	}

	newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
	newContent := make(map[string]interface{})

	return false, newRoutingKey, newContent, nil
}

func HandleControlMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	switch routingKey.EventType {
	case "DeleteMicroservice":
		AMQPHandler.StopConsumption = true

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", "DeletedMicroservice")
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil
	case events.DSU_START:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", events.DSU_START_ACKNOWLEDGED)
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil

	default:
		log.Printf("[SERVICE %s]Other control messages MUST have managed in function LocalHandler called before this function in ConsumeMessage", config.ServiceName)
		//log.Printf("[SERVICE %s] Received unhandled control message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})

		return false, newRoutingKey, newContent, nil
	}
}
