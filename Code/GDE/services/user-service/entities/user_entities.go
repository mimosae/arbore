package entities

import (
	"gde/common/gde-common/types"
)

//User ...
type User struct {
	ID         int64             `json:"user_id" gorm:"primary_key;AUTO_INCREMENT"`
	Name       string            `json:"user_name"`
	Password   string            `json:"user_password"`
	Attributes []types.Attribute `json:"user_attributes"`
	Groups     []Group           `json:"groups" gorm:"many2many:user_groups;"`
}

type Group struct {
	ID         int64             `json:"group_id" gorm:"primary_key;AUTO_INCREMENT"`
	Name       string            `json:"group_name"`
	Attributes []types.Attribute `json:"group_attributes"`
	Users      []User            `json:"users" gorm:"many2many:user_groups;"`
}
