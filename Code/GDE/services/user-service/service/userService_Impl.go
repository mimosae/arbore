package service

import (
	"gde/services/user-service/db"
	"gde/services/user-service/entities"
)

//UserServiceImpl ...
type UserServiceImpl struct{}

/* User */
//ListUsers ...
func (u *UserServiceImpl) ListUsers() ([]entities.User, error) {
	return db.Database.FindUsers()
}

//CreateUser ...
func (u *UserServiceImpl) CreateUser(userName string, password string) (*entities.User, error) {
	user := &entities.User{}
	user.Name = userName
	user.Password = password
	return db.Database.InsertUser(user)
}

func (u *UserServiceImpl) CreateUserByUser(user *entities.User) (*entities.User, error) {
	return db.Database.InsertUser(user)
}

//DeleteUser ...
func (u *UserServiceImpl) DeleteUser(id int64) error {
	_, err := db.Database.FindUserByID(id)
	if err != nil {
		return err
	}
	return db.Database.DeleteUser(id)
}

//FindUserByID ...
func (u *UserServiceImpl) FindUserByID(id int64) (entities.User, error) {
	return db.Database.FindUserByID(id)
}

//FindUserByName ...
func (u *UserServiceImpl) FindUserByName(userName string) (entities.User, error) {
	return db.Database.FindUserByName(userName)
}

/* Group */

//CreateGroup ...
func (u *UserServiceImpl) CreateGroup(groupName string) (*entities.Group, error) {
	group := &entities.Group{}
	group.Name = groupName
	return db.Database.InsertGroup(group)
}

func (u *UserServiceImpl) CreateGroupByGroup(group *entities.Group) (*entities.Group, error) {
	return db.Database.InsertGroup(group)
}

//ListGroups ...
func (u *UserServiceImpl) ListGroups() ([]entities.Group, error) {
	return db.Database.FindGroups()
}

//FindGroupByID ...
func (u *UserServiceImpl) FindGroupByID(id int64) (entities.Group, error) {
	return db.Database.FindGroupByID(id)
}

//FindGroupByName ...
func (u *UserServiceImpl) FindGroupByName(groupName string) (entities.Group, error) {
	return db.Database.FindGroupByName(groupName)
}

//DeleteGroup ...
func (u *UserServiceImpl) DeleteGroup(id int64) error {
	_, err := db.Database.FindGroupByID(id)
	if err != nil {
		return err
	}
	return db.Database.DeleteGroup(id)
}

/* User and Group */
//AddUserToGroup ...
func (u *UserServiceImpl) AddUserToGroup(userID int64, groupID int64) error {
	return db.Database.AddUserToGroup(userID, groupID)
}

//AddUsersToGroup ...
func (u *UserServiceImpl) AddUsersToGroup(userIDs []int64, groupID int64) error {
	return db.Database.AddUsersToGroup(userIDs, groupID)
}

//RemoveUserFromGroup ...
func (u *UserServiceImpl) RemoveUserFromGroup(userID int64, groupID int64) error {
	return db.Database.RemoveUserFromGroup(userID, groupID)
}

//IsUserInGroup ...
func (u *UserServiceImpl) IsUserInGroup(userID int64, groupID int64) bool {
	return db.Database.IsUserInGroup(userID, groupID)
}

//ListUsersInGroup ...
func (u *UserServiceImpl) ListUsersInGroup(groupID int64) ([]entities.User, error) {
	return db.Database.ListUsersInGroup(groupID)
}

//ListGroupsOfUser ...
func (u *UserServiceImpl) ListGroupsOfUser(userID int64) ([]entities.Group, error) {
	return db.Database.ListGroupsOfUser(userID)
}

func (u *UserServiceImpl) CheckPassword(login, password string) (bool, error) {
	return db.Database.CheckPassword(login, password)
}
