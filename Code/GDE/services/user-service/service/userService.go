package service

import (
	"gde/services/user-service/entities"
)

//UserService ...
type UserService interface {
	// User
	CreateUser(userName string, password string) (*entities.User, error)
	CreateUserByUser(user *entities.User) (*entities.User, error)
	DeleteUser(id int64) error
	FindUserByName(userName string) (entities.User, error)
	FindUserByID(id int64) (entities.User, error)
	ListUsers() ([]entities.User, error)

	CheckPassword(login, password string) (bool, error)

	// Group
	CreateGroup(groupName string) (*entities.Group, error)
	CreateGroupByGroup(group *entities.Group) (*entities.Group, error)
	DeleteGroup(id int64) error
	FindGroupByName(groupName string) (entities.Group, error)
	FindGroupByID(id int64) (entities.Group, error)
	ListGroups() ([]entities.Group, error)

	// User-Group association
	AddUserToGroup(userID int64, groupID int64) error
	AddUsersToGroup(userIDs []int64, groupID int64) error
	RemoveUserFromGroup(userID int64, groupID int64) error
	IsUserInGroup(userID int64, groupID int64) bool
	ListUsersInGroup(groupID int64) ([]entities.User, error)
	ListGroupsOfUser(userID int64) ([]entities.Group, error)
}
