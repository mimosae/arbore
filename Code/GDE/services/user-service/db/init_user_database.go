package db

import (
	"gde/services/user-service/entities"

	"log"
)

func InitUserDatabase(pg *UserPostgresDB) {
	log.Printf("Init User Database ...")

	testGroup := &entities.Group{
		Name: "test-group",
	}
	testGroup, _ = pg.InsertGroup(testGroup)

	testUser := &entities.User{
		Name:     "test",
		Password: "test",
	}
	testUser, _ = pg.InsertUser(testUser)
	pg.AddUserToGroup(testUser.ID, testGroup.ID)
}
