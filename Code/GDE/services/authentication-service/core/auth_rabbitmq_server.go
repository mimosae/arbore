package core

import (
	"gde/services/authentication-service/config"
	"gde/services/authentication-service/entities"
	"gde/services/authentication-service/service"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/events"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"

	"log"
	"strings"
)

var AMQPHandler *amqp.AMQPHandler

var SecretKey = []byte("private_key")

func HandleApplicationMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	as := service.AuthService{}

	switch routingKey.EventType {
	case events.AUTHENTICATE:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.USER_SERVICE, "Application", events.PASSWORD_TO_CHECK)

		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]
		newContent["UserName"] = content["UserName"]
		newContent["Password"] = content["Password"]
		log.Printf("[SERVICE %s] (%s,%s)", config.ServiceName, content["UserName"], content["Password"])

		return true, newRoutingKey, newContent, nil

	case events.TOKEN_TO_CHECK:
		jwtToken, err := stripTokenPrefix(content["Token"].(string))
		if err != nil {
			log.Fatal("Failed get JWT token")
		}
		claims, err := as.ParseJWT(jwtToken, string(SecretKey))
		if err != nil {
			log.Fatal("Failed to parse token")
		}
		login, ok := claims["login"].(string)
		if !ok {
			log.Fatal("login not exist in token")
		}

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Application", events.TOKEN_CHECKED)

		newContent := make(map[string]interface{})
		newContent["Token"] = content["Token"]
		newContent["Login"] = login

		return true, newRoutingKey, newContent, nil

	case events.LOGIN_TO_REQUEST:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.USER_SERVICE, "Application", events.PASSWORD_TO_CHECK)

		newContent := make(map[string]interface{})
		newContent["senderLoginRequest"] = routingKey.Source

		return true, newRoutingKey, newContent, nil

	case events.PASSWORD_CHECKED:
		// Obtain client token
		credentials := entities.Credentials{
			Login:    content["UserName"].(string),
			Password: content["Password"].(string),
		}
		var token string
		if content["PasswordCheck"] == true {
			claims := service.NewClaims(service.Credentials(credentials))
			var err error
			token, err = as.EncodeJWT(string(SecretKey), claims)
			if err != nil {
				log.Fatal("Error encode JWT token")
			}
		}

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.FRONT_END, "Application", events.AUTHENTICATION_TOKEN_RECEIVED)
		newContent := make(map[string]interface{})
		newContent["Token"] = token

		return true, newRoutingKey, newContent, nil

	default:
		log.Printf("[SERVICE %s] Received unhandled application message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})

		return false, newRoutingKey, newContent, nil
	}
}

func HandleControlMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	switch routingKey.EventType {
	case "DeleteMicroservice":
		AMQPHandler.StopConsumption = true

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", "DeletedMicroservice")
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil
	case events.DSU_START:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", events.DSU_START_ACKNOWLEDGED)
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil

	default:
		log.Printf("[SERVICE %s]Other control messages MUST have managed in function LocalHandler called before this function in ConsumeMessage", config.ServiceName)
		//log.Printf("[SERVICE %s] Received unhandled control message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})

		return false, newRoutingKey, newContent, nil
	}
}

// strip token from authorization header
func stripTokenPrefix(tok string) (string, error) {
	tokenParts := strings.Split(tok, " ")
	if len(tokenParts) < 2 {
		return tokenParts[0], nil
	}
	return tokenParts[1], nil
}
