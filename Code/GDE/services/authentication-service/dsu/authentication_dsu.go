package dsu

import (
	"arbore/dsu/algorithms"
	"arbore/dsu/algorithms/snapshot"
)

var CommonVariables *algorithms.CommonVariables = &algorithms.CommonVariables{
	StatusActive: false,
	UpdateStart:  false,
}

// The handler for the snapshot algorithm
var SnapshotHandler *snapshot.SnapshotHandler = &snapshot.SnapshotHandler{
	Variables: snapshot.SnapshotVars{
		SnapshotCounter:   0,
		MessageCounterMap: make(map[string]int),
	},
}

var Handlers *algorithms.Handlers = &algorithms.Handlers{
	SnapshotHandler: SnapshotHandler,
}
