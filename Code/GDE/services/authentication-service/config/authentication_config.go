package config

import (
	"gde/common/gde-common/services"
)

// Queue name for this microservice
const QueueName = "auth_queue"

// Service name
const ServiceName = services.AUTHENTICATION_SERVICE

// Default IP of AMQP broker
const DefaultIP = "localhost"

// Default port of AMQP broker
const DefaultPort = "5672"

// Global AMQP exchange name
const ExchangeName = "gde_exchange"

// Global AMQP exchange type
const ExchangeType = "topic"
