package service

import (
	"fmt"

	jwt "github.com/dgrijalva/jwt-go"
)

type AuthService struct{}

type Credentials struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type Claims = jwt.MapClaims

type StandardClaims jwt.StandardClaims

func NewClaims(data Credentials) Claims {
	c := map[string]interface{}{
		"login": data.Login,
	}
	newClaims := Claims(c)
	return newClaims
}

func (as *AuthService) ParseJWT(tokenString string, key string) (Claims, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(key), nil
	})
	if token.Valid {
		if claims, ok := token.Claims.(Claims); ok {
			return claims, nil
		}
		return nil, err
	} else {
		return nil, err
	}
}

func (as *AuthService) EncodeJWT(secretKey string, claims Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	secret := []byte(secretKey)
	tokenString, err := token.SignedString(secret)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
