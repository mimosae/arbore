package config

import (
	"gde/common/gde-common/services"
)

// Queue name for this microservice
const QueueName = "frontend_queue"

// Service name
const ServiceName = services.FRONT_END

// Default IP of AMQP broker
const DefaultIP = "localhost"

// Default port of AMQP broker
const DefaultPort = "5672"

// Name of the RabbitMQ Broker
const ExchangeName = "gde_exchange"

// Global AMQP exchange type
const ExchangeType = "topic"

// Port for listening to HTTP requests
const HTTPPort = "8080"
