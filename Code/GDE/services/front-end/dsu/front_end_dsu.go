package dsu

import (
	"arbore/dsu/algorithms"
	"arbore/dsu/algorithms/snapshot"
	commondsu "arbore/dsu/common"
	"container/list"
)

var CommonVariables *algorithms.CommonVariables = &algorithms.CommonVariables{
	StatusActive: false,
	UpdateStart:  false,
}

// The handler for the snapshot algorithm
var SnapshotHandler *snapshot.SnapshotHandler = &snapshot.SnapshotHandler{
	Variables: snapshot.SnapshotVars{
		SnapshotCounter:   0,
		MessageCounterMap: make(map[string]int),
	},
}

var Handlers *algorithms.Handlers = &algorithms.Handlers{
	SnapshotHandler: SnapshotHandler,
}

// Slice to save the quiescent set
var MsNamesSlide []string

type MessageStruct struct {
	Destination     string
	MessageBody     []byte
	CollaborationID string
}

// Queue to store the messages
var MessageQueue = list.New()

func Contains(sliceToSearch []string, x string) bool {
	for _, element := range sliceToSearch {
		if x == element {
			return true
		}
	}
	return false
}

var OngoingDSUQuiescence *commondsu.DSUQuiescence = &commondsu.DSUQuiescence{
	Strategy:   "",
	OngoingDSU: false,
}

//func main() {
//	// Simply append to enqueue.
//	MessageQueue.PushBack(MessageStruct{Routingkey: "Sean", MessageBody: []byte("Julio")})
//	MessageQueue.PushBack(MessageStruct{Routingkey: "Pierre", MessageBody: []byte("Jorge")})
//
//	// Dequeue
//	front := MessageQueue.Front()
//	messageInQueue := front.Value.(MessageStruct)
//	fmt.Printf("Tipo: %T\n", messageInQueue)
//	fmt.Println(messageInQueue.Routingkey)
//
//	// Iterate the list
//	for element := MessageQueue.Front(); element != nil; element = element.Next() {
//		itemPerson := element.Value.(MessageStruct)
//		fmt.Println(itemPerson.Routingkey)
//	}
//}
