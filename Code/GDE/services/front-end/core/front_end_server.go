package core

import (
	"gde/services/front-end/config"

	amqp "gde/common/amqp-handler"

	"github.com/gorilla/mux"

	"log"
)

var Server *FrontEndServer

type FrontEndServer struct {
	*mux.Router
	AmqpHandler *amqp.AMQPHandler
}

func NewFrontEndServer() *FrontEndServer {
	AMQPHandler = amqp.InitAMQPHandler(config.DefaultIP, config.DefaultPort, config.ExchangeName, config.ExchangeType)
	AMQPHandler.CreateQueue(config.QueueName)
	AMQPHandler.BindQueue(config.QueueName, "*."+config.ServiceName+".*.*")
	log.Printf("[SERVICE front-end] Start consumption of messages\n")

	frontEndServer := &FrontEndServer{
		Router:      mux.NewRouter().StrictSlash(true),
		AmqpHandler: AMQPHandler,
	}
	frontEndServer.routes()
	return frontEndServer
}

func (s *FrontEndServer) routes() {
	s.HandleFunc("/authenticate", handleRequestType("authenticate")).Methods("POST")
	s.HandleFunc("/createuser", handleRequestType("createUser")).Methods("POST")
	s.HandleFunc("/createproject", handleRequestType("createProject")).Methods("POST")
	s.HandleFunc("/fileattachinproject", handleRequestType("fileAttachInProject")).Methods("POST")
	s.HandleFunc("/createfile", handleRequestType("createFile")).Methods("POST")
	s.HandleFunc("/accountability", handleRequestType("accountability")).Methods("POST")
}
