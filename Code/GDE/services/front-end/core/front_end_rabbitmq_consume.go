package core

import (
	commondsu "arbore/dsu/common"
	"gde/services/front-end/config"
	dsu "gde/services/front-end/dsu"

	"gde/common/gde-common/events"
	msghandler "gde/common/message-handler"

	"encoding/json"
	"fmt"
	"log"
)

var StopChannel = make(chan bool)
var UnblockChannel = make(chan bool, 10)

func HandleApplicationMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	switch {
	case routingKey.EventType == events.AUTHENTICATION_TOKEN_RECEIVED ||
		routingKey.EventType == events.USER_CREATED ||
		routingKey.EventType == events.PROJECT_CREATED ||
		routingKey.EventType == events.FILE_ATTACHED_IN_PROJECT ||
		routingKey.EventType == events.FILE_CREATED:
		responseChannel := ClientRequests[metadata.CollaborationID].GoChannel
		metadata := msghandler.Metadata{
			Algorithms:      make(map[string]map[string]interface{}),
			CollaborationID: "",
		}
		message := msghandler.BuildMessage(metadata, content)
		response, _ := message.ToJSON()
		log.Printf("[SERVICE %s] [AMQP HANDLER] response to client %s", config.ServiceName, string(response))
		responseChannel <- string(response)

	case routingKey.EventType == events.GROUPS_LISTED_BY_LOGIN:
		responseChannel := ClientRequests[metadata.CollaborationID].GoChannel
		groupID := content["GroupIds"]
		responseChannel <- "test user belong to group(s): " + groupID.(string)

	default:
		log.Printf("[SERVICE %s] Received unhandled application message", config.ServiceName)
	}

	newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
	newContent := make(map[string]interface{})

	return false, newRoutingKey, newContent, nil
}

func HandleControlMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {

	switch routingKey.EventType {
	case events.DSU_BLCOCK_NEW_COLLABORATIONS:
		dsu.OngoingDSUQuiescence.Strategy = commondsu.Blocking

		msg := fmt.Sprintf("%v", content["MsNamesSlide"])
		type msNamesSlideStruct struct {
			MsNamesSlide []string
		}
		var msNamesSlide msNamesSlideStruct
		err := json.Unmarshal([]byte(msg), &msNamesSlide)
		if err != nil {
			log.Printf("Error unmarshaling information: %v", err)
		}
		dsu.MsNamesSlide = msNamesSlide.MsNamesSlide

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", events.DSU_NEW_COLLABORATION_BLOCKED)
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil

	case events.DSU_UNBLOCK_COLLABORATION:

		UnblockChannel <- true
		dsu.OngoingDSUQuiescence.OngoingDSU = false
		//DSU.MsNamesSlide = []string{}

		/*for element := DSU.MessageQueue.Front(); element != nil; element = element.Next() {
			message := element.Value.(DSU.MessageStruct)
			newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, message.Destination, "Control", "TakeSnpshot")
			metadata = msghandler.Metadata{
				CollaborationID: message.CollaborationID,
				Algorithms:      make(map[string]map[string]interface{}),
			}
			content := make(map[string]interface{})
			content["message"] = message.MessageBody
			newMsg := msghandler.BuildMessage(metadata, content)
			AMQPHandler.PublishMessage(newRoutingKey, newMsg)
			DSU.MessageQueue.Remove(element)
		}*/
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", events.DSU_COLLABORATION_UNBLOCKED)
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil

	case "DeleteMicroservice":
		AMQPHandler.StopConsumption = true

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", "DeletedMicroservice")
		newContent := make(map[string]interface{})

		StopChannel <- true

		return true, newRoutingKey, newContent, nil
	case events.DSU_START:
		dsu.OngoingDSUQuiescence.OngoingDSU = true
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", events.DSU_START_ACKNOWLEDGED)
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil

	default:
		log.Printf("[SERVICE %s]Other control messages MUST have managed in function LocalHandler called before this function in ConsumeMessage", config.ServiceName)

	}
	newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
	newContent := make(map[string]interface{})

	return false, newRoutingKey, newContent, nil
}
