package core

import (
	commondsu "arbore/dsu/common"
	"gde/services/front-end/config"
	"gde/services/front-end/dsu"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/events"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"

	"encoding/json"
	"fmt"
	"io/ioutil"

	"log"
	"net/http"
)

var AMQPHandler *amqp.AMQPHandler

type ClientRequestsDataStructure struct {
	MessageCounter int
	EventName      string
	ReceiverName   string
	GoChannel      chan string
	Parameters     []byte
}

var ClientRequests = make(map[string]ClientRequestsDataStructure)

func handleRequestType(requestType string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		a := dsu.OngoingDSUQuiescence != nil && dsu.OngoingDSUQuiescence.OngoingDSU && dsu.OngoingDSUQuiescence.Strategy == commondsu.Blocking
		log.Printf("[ FRONTENSD SERVER] [HTTP HANDLER ] ongoing dsu is  %t with , %s strategy ", dsu.OngoingDSUQuiescence.OngoingDSU, dsu.OngoingDSUQuiescence.Strategy)
		log.Printf("%t", a)
		if dsu.OngoingDSUQuiescence != nil && dsu.OngoingDSUQuiescence.OngoingDSU && dsu.OngoingDSUQuiescence.Strategy == commondsu.Blocking {
			log.Print("[ FRONTENSD SERVER] [HTTP HANDLER ] waiting for DSU ")
			<-UnblockChannel
		}

		var eventType string
		var destinationService string

		body, _ := ioutil.ReadAll(r.Body)
		content := parseMessage(body, requestType)

		switch requestType {
		case "authenticate":
			eventType = events.AUTHENTICATE
			destinationService = services.AUTHENTICATION_SERVICE

		case "createUser":
			eventType = events.USER_TO_CREATE_BY_NAME_PASSWORD
			destinationService = services.USER_SERVICE

		case "createProject":
			eventType = events.PROJECT_TO_CREATE
			destinationService = services.PROJECT_SERVICE

		case "createFile":
			eventType = events.FILE_TO_CREATE
			destinationService = services.FILE_SERVICE

		case "fileAttachInProject":
			eventType = events.FILE_TO_ATTACH_IN_PROJECT
			destinationService = services.PROJECT_SERVICE

		case "accountability":
			eventType = events.ACCOUNTABILITY
			destinationService = services.ACCOUNTABILITY_SERVICE
		}

		collaborationID := msghandler.CreateCollaborationID(destinationService, eventType).String()

		goChannel := make(chan string)

		ClientRequests[collaborationID] = ClientRequestsDataStructure{
			MessageCounter: 0,
			EventName:      eventType,
			ReceiverName:   destinationService,
			GoChannel:      goChannel,
			Parameters:     body,
		}

		dsu.Handlers.SnapshotHandler.Variables.MessageCounterMap[collaborationID]++

		sendMessageToMicroservice(collaborationID, eventType, destinationService, content)

		response := <-goChannel
		log.Printf("[ FRONTENSD SERVER] [HTTP HANDLER ] Response for the client %s", response)
		if !GoChannelIsClosed(goChannel) {
			close(goChannel)
		}

		fmt.Fprintf(w, response)
	}
}

func parseMessage(body []byte, requestType string) map[string]interface{} {
	var result map[string]interface{}
	json.Unmarshal(body, &result)

	return result
}

func sendMessageToMicroservice(collaborationID string, eventType string, destinationService string, content map[string]interface{}) {
	newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, destinationService, "Application", eventType)

	algorithms := make(map[string]map[string]interface{})
	algorithms["Snapshot"] = make(map[string]interface{})
	algorithms["Snapshot"]["SnapshotCounter"] = dsu.Handlers.SnapshotHandler.Variables.SnapshotCounter
	metadata := msghandler.Metadata{
		CollaborationID: collaborationID,
		Algorithms:      algorithms,
	}

	message := msghandler.BuildMessage(metadata, content)

	AMQPHandler.PublishMessage(newRoutingKey, message)
}

func GoChannelIsClosed(ch <-chan string) bool {

	select {
	case <-ch:
		return false
	default:
	}
	return true
}
