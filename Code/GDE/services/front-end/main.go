package main

import (
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"
	"gde/services/front-end/config"
	"gde/services/front-end/core"
	"gde/services/front-end/dsu"

	"log"
	"net/http"
)

// var Server *core.FrontEndServer

func main() {
	log.Printf("[SERVICE %s] Starting", config.ServiceName)

	core.Server = core.NewFrontEndServer()
	defer core.Server.AmqpHandler.Close()

	// Creates a new Goroutine to consume the AMQP messages
	log.Printf("[SERVICE %s] Start consumption of messages", config.ServiceName)
	go func() {
		core.AMQPHandler.ConsumeMessages(config.QueueName, config.ServiceName, core.HandleApplicationMessage, core.HandleControlMessage, dsu.Handlers, dsu.CommonVariables)
	}()

	// Starts to listen for HTTP REST requests
	//log.Printf("[SERVICE %s] Front End HTTP core serving on port %s", config.ServiceName, config.HTTPPort)

	go func() {
		log.Printf("[SERVICE %s] Front End HTTP core serving on port %s", config.ServiceName, config.HTTPPort)
		http.ListenAndServe(config.DefaultIP+":"+config.HTTPPort, core.Server)
	}()

	<-core.AMQPHandler.Ready
	// Publish a message to the Autonomic Manager to confirm service startup
	routingKey := msghandler.BuildRoutingKey(config.ServiceName, services.AUTONOMIC_MANAGER, "Control", "MicroserviceStarted")
	msg := msghandler.BuildMessage(msghandler.Metadata{CollaborationID: "", Algorithms: make(map[string]map[string]interface{})}, make(map[string]interface{}))
	core.AMQPHandler.PublishMessage(routingKey, msg)
	<-core.StopChannel

	log.Printf("[SERVICE %s] Stopping", config.ServiceName)
}
