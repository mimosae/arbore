package core

import (
	"gde/services/accountability-service/config"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/events"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"

	"log"
)

var AMQPHandler *amqp.AMQPHandler

func HandleApplicationMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	switch routingKey.EventType {
	case events.ACCOUNTABILITY:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.USER_SERVICE, "Application", events.GROUPS_TO_LIST_BY_LOGIN)

		newContent := make(map[string]interface{})
		newContent["Login"] = "test"

		return true, newRoutingKey, newContent, nil

	default:
		log.Printf("[SERVICE %s] Received unhandled application message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})

		return false, newRoutingKey, newContent, nil
	}
}

func HandleControlMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	switch routingKey.EventType {
	case "DeleteMicroservice":
		AMQPHandler.StopConsumption = true

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", "DeletedMicroservice")
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil
	case events.DSU_START:
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, routingKey.Source, "Control", events.DSU_START_ACKNOWLEDGED)
		newContent := make(map[string]interface{})

		return true, newRoutingKey, newContent, nil

	default:
		log.Printf("[SERVICE %s] Received unhandled control message", config.ServiceName)
		newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
		newContent := make(map[string]interface{})
		return false, newRoutingKey, newContent, nil
	}
}
