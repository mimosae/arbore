package events

const (
	AUTHENTICATE                  = "Authenticate"
	CHECK_TOKEN                   = "CheckToken"
	LOGIN_TO_REQUEST              = "LoginToRequest"
	AUTHENTICATION_TOKEN_RECEIVED = "AuthenticationTokenReceived"
	LOGIN_NAME_TO_REQUEST         = "LoginNameToRequest"
	LOGIN_NAME_RECEIVED           = "LoginNameReceived"
	TOKEN_TO_CHECK                = "TokenToCheck"
	TOKEN_CHECKED                 = "TokenChecked"
)
