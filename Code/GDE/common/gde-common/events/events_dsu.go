package events

const (
	DSU_START                     = "DSUStart"
	DSU_START_ACKNOWLEDGED        = "DSUStartAcknowledged"
	DSU_NEW_COLLABORATION_BLOCKED = "DSUNewCollaborationBlocked"
	DSU_BLCOCK_NEW_COLLABORATIONS = "DSUBlockNewCollaborations"
	DSU_Take_Snapshot             = "DSUTakeSnapshot"
	DSU_Snapshot_Taken            = "DSUSnapshotTaken"
	DSU_COLLABORATION_UNBLOCKED   = "DSUCollaborationUnblocked"
	DSU_UNBLOCK_COLLABORATION     = "DSUUnblockCollaboration"
)
