package services

const (
	ACCOUNTABILITY_SERVICE = "AccountabilityService"
	AUTHENTICATION_SERVICE = "AuthenticationService"
	EVENTS_LOGGER_SERVICE  = "EventsLoggerService"
	FILE_SERVICE           = "FileService"
	PERMISSION_SERVICE     = "PermissionService"
	PROJECT_SERVICE        = "ProjectService"
	USER_SERVICE           = "UserService"
	FRONT_END              = "FrontEnd"

	AUTONOMIC_MANAGER = "AutonomicManager"
)

// This array is used by the Autonomic Manager
// It is used, for example, when sending a snapshot request to all services
// It is also used in the ServiceExists function below
var ServicesList = []string{
	"AccountabilityService",
	"AuthenticationService",
	"EventsLoggerService",
	"FileService",
	"PermissionService",
	"ProjectService",
	"UserService",
	"FrontEnd",
}
