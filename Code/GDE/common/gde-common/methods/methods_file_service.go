package methods

const (
	CREATE_FILE = iota
	UPDATE_FILE
	DELETE_FILE
	RESTORE_FILE
	FIND_FILE_BY_ID
	READ_FILE
	FIND_FILES_BY_NAME
	FIND_FILES_BY_CREATTION_DATE
	FIND_FILES_BY_DELETION_DATE
	FIND_FILES_BY_UPDATE_DATE
	FIND_FILES_BY_RESTORATION_DATE
	LIST_FILES
	FIND_FILES_TO_INDEX
	FIND_FILES_DELETED
	SET_FILE_VALID
	IS_FILE_VALID
	SET_FILE_INDEXED
	IS_FILE_INDEXED
	OPEN_FILE
	CLOSE_FILE
	CREATE_CHUNK
	READ_CHUNK
	ATTACH_CHUNK_TO_FILE
	DETACH_CHUNK_FROM_FILE
	FIND_CHUNKS_BY_FILE_ID
	FIND_CHUNKS_INFO_BY_FILE_ID
)
