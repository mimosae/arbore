package types

type Attribute struct {
	ID        int64
	Name      string
	Type      string
	Value     string
	Mandatory bool
	Deleted   bool
}
