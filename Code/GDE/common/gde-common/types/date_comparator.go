package types

type DateComparator int

const (
	DATE_EQUAL         DateComparator = 1
	DATE_DIFFER        DateComparator = 2
	DATE_GREATER       DateComparator = 3
	DATE_LOWER         DateComparator = 4
	DATE_GREATER_EQUAL DateComparator = 5
	DATE_LOWER_EQUAL   DateComparator = 6
)
