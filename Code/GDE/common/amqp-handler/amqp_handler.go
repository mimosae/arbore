/*
A common package for all services allowing them to handle easily the AMQP communications.
*/
package amqphandler

import (
	"fmt"
	"log"

	//"github.com/sirupsen/logrus"

	"arbore/dsu/algorithms"

	msghandler "gde/common/message-handler"

	amqp "github.com/rabbitmq/amqp091-go"
)

// TYPE STRUCTURES

/*
Structure for handling AMQP communications.
  - AMQPConnection: the connection to the AMQP broker.
  - AMQPChannel: the channel linked to the AMQP broker.
  - StopConsumption: a boolean stating whether the consumption of messages should continue or not.
    This variable is used inside services: when a service receives a message from the Autonomic Manager telling him to stop running,
    the service changes this value to true. Thus, after finishing to handle the message from the AM, the consumption stops.
*/
type AMQPHandler struct {
	AMQPConnection  *amqp.Connection
	AMQPChannel     *amqp.Channel
	StopConsumption bool
	Ready           chan bool
	StopService     chan bool
	exchangeName    string
	exchangeType    string
}

// HELPING FUNCTIONS

// Builds an URL for connecting to the AMQP broker using the provided IP and port
// Note: for now, services connect to the broker as "guests"
func buildURL(ip string, port string) string {
	return fmt.Sprintf("amqp://guest:guest@%s:%s/", ip, port)
}

// Triggers an error in case an error has occured, displaying the provided description message
func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s\n", msg, err)
	}
}

// AMQP HANDLING FUNCTIONS

// Initializes an AMQPHandler structure.
func InitAMQPHandler(ip string, port string, exchangeName string, exchangeType string) *AMQPHandler {
	amqpURL := buildURL(ip, port)

	conn, err := amqp.Dial(amqpURL)
	failOnError(err, "Failed to open connection to AMQP broker")

	ch, err := conn.Channel()
	failOnError(err, "Failed to create channel to AMQP broker")

	err = ch.ExchangeDeclare(
		exchangeName, // Exchange name
		exchangeType, // Exchange type
		true,         // Durable
		false,        // Auto-delete
		false,        // Internal
		false,        // No-wait
		nil,          // Extra arguments
	)
	failOnError(err, "Failed to declare AMQP exchange")

	amqpHandler := &AMQPHandler{
		AMQPConnection:  conn,
		AMQPChannel:     ch,
		StopConsumption: false,
		Ready:           make(chan bool),
		StopService:     make(chan bool),
		exchangeName:    exchangeName,
		exchangeType:    exchangeType,
	}

	return amqpHandler
}

// Creates a queue for receiving messages from the AMQP broker.
func (amqpHandler *AMQPHandler) CreateQueue(queueName string) {
	_, err := amqpHandler.AMQPChannel.QueueDeclare(
		queueName, // Queue name
		false,     // Durable
		false,     // Delete when unused
		true,      // Exclusive
		false,     // No-wait
		nil,       // Extra arguments
	)
	failOnError(err, "Failed to declare a queue")
}

// Binds a existing queue to the provided binding key.
func (amqpHandler *AMQPHandler) BindQueue(queueName string, bindingKey string) {
	err := amqpHandler.AMQPChannel.QueueBind(
		queueName,                // Queue name
		bindingKey,               // Binding key
		amqpHandler.exchangeName, // Exhange name
		false,                    // No-wait
		nil,                      // Extra arguments
	)
	failOnError(err, "Failed to bind queue")
}

// Closes the channel and the connection to the AMQP broker.
func (amqpHandler *AMQPHandler) Close() {
	err := amqpHandler.AMQPChannel.Close()
	failOnError(err, "Failed to close AMQP channel")

	err = amqpHandler.AMQPConnection.Close()
	failOnError(err, "Failed to close AMQP connection")
}

// Publishes a message to the AMQP broker with the associated routing key.
func (amqpHandler *AMQPHandler) PublishMessage(routingKey msghandler.RoutingKey, msg msghandler.Message) {
	msgJson, _ := msg.ToJSON()
	err := amqpHandler.AMQPChannel.Publish(
		amqpHandler.exchangeName,
		routingKey.ToString(),
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        msgJson,
		},
	)
	failOnError(err, "Failed to publish message to AMQP broker")

	log.Printf("[SERVICE %s] [AMQP HANDLER] Sending message <routingKey=\"%s\">", routingKey.Source, routingKey)

	// TODO: Handle microservice state change
	// dsu.Status = false
}

// Tells the AMQPHandler to start consuming messages using the specified handlers. The consumption will not stop unless AMQPHandler.StopConsumption is set to true.
func (amqpHandler *AMQPHandler) ConsumeMessages(
	queueName string,
	consumerName string,
	handlerApplication func(msghandler.RoutingKey, map[string]interface{}, msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error),
	handlerControl func(msghandler.RoutingKey, map[string]interface{}, msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error),
	algorithmHandlers *algorithms.Handlers,
	commonAlgorithmVariables *algorithms.CommonVariables) {

	msgs, err := amqpHandler.AMQPChannel.Consume(
		queueName,    // Queue name
		consumerName, // Consumer name
		true,         // Auto-ACK
		false,        // Exclusive
		false,        // No-local
		false,        // No-wait
		nil,          // Extra arguments
	)
	failOnError(err, "Failed to register a consumer")

	amqpHandler.Ready <- true

	for msg := range msgs {
		log.Printf("[SERVICE %s] [AMQP HANDLER] Received message <routingKey=\"%s\">", consumerName, msg.RoutingKey)

		routingKey := msghandler.ParseRoutingKey(msg.RoutingKey)
		message := msghandler.FromJSON(msg.Body)

		// Handling algorithms
		//log.Print(*algorithmHandlers)
		if algorithmHandlers != nil {
			log.Printf("[SERVICE %s] [AMQP HANDLER] This is a control message", consumerName)
			var publishMessage, newRoutingKey, newMessage = algorithmHandlers.SnapshotHandler.LocalHandler(consumerName, routingKey, message)

			if publishMessage {
				amqpHandler.PublishMessage(newRoutingKey, newMessage)
			}
		}

		// We start handling the received message, the service becomes active
		if commonAlgorithmVariables != nil {
			commonAlgorithmVariables.StatusActive = true
		}

		var publishMessage bool
		var newRoutingKey msghandler.RoutingKey
		var newContent map[string]interface{}
		var err error
		switch {
		case routingKey.MessageType == "Control":
			publishMessage, newRoutingKey, newContent, err = handlerControl(routingKey, message.Content, message.Metadata)
		case routingKey.MessageType == "Application":
			publishMessage, newRoutingKey, newContent, err = handlerApplication(routingKey, message.Content, message.Metadata)
			log.Printf("[SERVICE %s] [AMQP HANDLER] This is an application message", consumerName)

		default:
			log.Printf("[SERVICE %s] [AMQP HANDLER] Unhandled message type \"%s\"", consumerName, routingKey.MessageType)
		}
		failOnError(err, "Failed to handle message")

		if publishMessage {
			// Handle algorithms metadata
			metadata := msghandler.Metadata{
				CollaborationID: message.Metadata.CollaborationID,
				Algorithms:      make(map[string]map[string]interface{}),
			}
			if algorithmHandlers != nil {
				metadata.AddAlgorithmMetadata("Snapshot", algorithmHandlers.SnapshotHandler.GetAlgorithmMetadata())
			}

			newMessage := msghandler.BuildMessage(metadata, newContent)
			amqpHandler.PublishMessage(newRoutingKey, newMessage)

			if routingKey.MessageType == "Application" && routingKey.Destination != "FrontEnd" {
				log.Printf("[SERVICE %s] [AMQP HANDLER] [SNAPSHOT] Incrementing message counter for collaboration %s", consumerName, message.Metadata.CollaborationID)
				algorithmHandlers.SnapshotHandler.Variables.MessageCounterMap[message.Metadata.CollaborationID]++
			}
		}

		// We've finished handling the message, the service becomes passive again
		if commonAlgorithmVariables != nil {
			commonAlgorithmVariables.StatusActive = false
		}

		if amqpHandler.StopConsumption {
			break
		}
	}

	amqpHandler.StopService <- true
}

// TODO: Use the original ConsumeMessages method and adapt it so the Autonomic Manager can use it, instead of duplicating the method.
func (amqpHandler *AMQPHandler) ConsumeMessagesAutonomicManager(
	queueName string,
	consumerName string,
	handlerApplication func(msghandler.RoutingKey, map[string]interface{}, msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error),
	handlerControl func(msghandler.RoutingKey, map[string]interface{}, msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error),
	status chan bool) {

	msgs, err := amqpHandler.AMQPChannel.Consume(
		queueName,    // Queue name
		consumerName, // Consumer name
		true,         // Auto-ACK
		false,        // Exclusive
		false,        // No-local
		false,        // No-wait
		nil,          // Extra arguments
	)
	failOnError(err, "Failed to register a consumer")

	status <- true

	for msg := range msgs {
		routingKey := msghandler.ParseRoutingKey(msg.RoutingKey)
		message := msghandler.FromJSON(msg.Body)

		log.Printf("[SERVICE %s] [AMQP HANDLER] Received message <routingKey=\"%s\">", consumerName, routingKey)

		var publishMessage bool
		var newRoutingKey msghandler.RoutingKey
		var newContent map[string]interface{}
		var err error
		switch {
		case routingKey.MessageType == "Control":
			publishMessage, newRoutingKey, newContent, err = handlerControl(routingKey, message.Content, message.Metadata)
		case routingKey.MessageType == "Application":
			publishMessage, newRoutingKey, newContent, err = handlerApplication(routingKey, message.Content, message.Metadata)
		default:
			log.Printf("[SERVICE %s] [AMQP HANDLER] Unhandled message type \"%s\"", consumerName, routingKey.MessageType)
		}
		failOnError(err, "Failed to handle message")

		if publishMessage {
			metadata := msghandler.Metadata{
				CollaborationID: "",
				Algorithms:      make(map[string]map[string]interface{}),
			}

			newMessage := msghandler.BuildMessage(metadata, newContent)
			amqpHandler.PublishMessage(newRoutingKey, newMessage)
		}

		if amqpHandler.StopConsumption {
			break
		}
	}
}
