/*
A common package for all services allowing them to save informations about their state.
*/
package servicestate

// TYPE STRUCTURES

/*
Structure representing an event that occured in a service.
	- Name: event name.
	- Data: data related to the event.
*/
type Event struct {
	Name string
	Data map[string]interface{}
}

/*
Structure representing a collaboration state from a local point of view of a sevice.
	- Events: a stack to save collaboration events.
*/
type CollaborationState struct {
	Events []Event
}

/*
Structure representing a service state.
	- Collaborations: a set of active collaborations identified by the collaboration id.
*/
type ServiceState struct {
	Collaborations map[string]CollaborationState
}


// FUNCTIONS

// Builds a structured service state.
func InitServiceState() *ServiceState {
	return &ServiceState{
		Collaborations: make(map[string]CollaborationState),
	}
}

// Returns the events saved for a collaboration.
func (state *ServiceState) GetCollaborationState(collaborationID string) CollaborationState {
	return state.Collaborations[collaborationID]
}

// Removes a collaboration from the service state.
func (state *ServiceState) RemoveCollaborationState(collaborationID string) {
	delete(state.Collaborations, collaborationID)
}

// Saves an event for a collaboration.
func (state *ServiceState) SaveCollaborationState(collaborationID string, eventName string, eventData map[string]interface{}) {
	_, exists := state.Collaborations[collaborationID]
	if !exists {
		state.Collaborations[collaborationID] = CollaborationState{
			Events: []Event{},
		}
	}

	event := Event{
		Name: eventName,
		Data: eventData,
	}
	collaborationState := state.Collaborations[collaborationID]
	collaborationState.Events = append(collaborationState.Events, event)
	state.Collaborations[collaborationID] = collaborationState
}

// Returns true if a collboration has no events saved.
func (state *ServiceState) isEmpty(collaborationID string) bool {
	return len(state.Collaborations) == 0
}

// Returns the last event in the stack for a collaboration.
func (state *ServiceState) GetLastCollaborationEvent(collaborationID string) Event {
	collaboration := state.Collaborations[collaborationID]
	index := len(collaboration.Events) - 1

	return collaboration.Events[index]
}

// Pops an event for a collaboration and returns it.
func (state *ServiceState) RemoveLastCollaborationEvent(collaborationID string) Event {
	index := len(state.Collaborations[collaborationID].Events) - 1
	collaborationState := state.Collaborations[collaborationID]
	event := collaborationState.Events[index]
	collaborationState.Events = collaborationState.Events[:index]
	state.Collaborations[collaborationID] = collaborationState

	return event
}
