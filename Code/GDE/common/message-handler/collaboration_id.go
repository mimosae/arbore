package messagehandler

import (
	"fmt"
	"strconv"
	"strings"
)

type CollaborationID struct {
	Emitter       string
	OperationName string
	Counter       int
}

var CollaborationCounter = 0

func CreateCollaborationID(emitter string, operationName string) CollaborationID {
	CollaborationCounter++
	return CollaborationID{
		Emitter:       emitter,
		OperationName: operationName,
		Counter:       CollaborationCounter,
	}
}

func ParseCollaborationID(str string) CollaborationID {
	elements := strings.Split(str, ".")

	counter, _ := strconv.Atoi(elements[2])

	return CollaborationID{
		Emitter:       elements[0],
		OperationName: elements[1],
		Counter:       counter,
	}
}

func (cid CollaborationID) String() string {
	return fmt.Sprintf("%s.%s.%d", cid.Emitter, cid.OperationName, cid.Counter)
}
