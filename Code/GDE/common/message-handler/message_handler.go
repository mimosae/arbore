/*
A common package for all services allowing them to easily manipulate structured messages.
*/
package messagehandler

import (
	"encoding/json"
	"strings"
)

/*
Structure representing a routing key.
This structure makes the routing key format reliable abroad all services, this format being:
<source>.<destination>.<messageType>.<eventType>.
	- Source: the name of the microservice sending the message.
	- Destination: the name of the microservice the message is destined to.
	- MessageType: the type of the message, either "Application" or "Control".
	- EventType: the name of the event (see complete list in gde-common/event-type-list).
*/
type RoutingKey struct {
	Source      string
	Destination string
	MessageType string
	EventType   string
}

/*
Structure representing the metadata part of a message.
	- Algorithms: metadata related to algorithms.
	- CollaborationID: the ID of the collaboration the message is related to, if any.
*/
type Metadata struct {
	Algorithms      map[string]map[string]interface{}
	CollaborationID string
}

/*
Structure representing a message.
	- Metadata: the metadata of the message
	- Content: the data related to the application functionalities
*/
type Message struct {
	Metadata Metadata
	Content  map[string]interface{}
}

// Builds a structured routing key from source, destination, message type and event type.
func BuildRoutingKey(source string, destination string, messageType string, eventType string) RoutingKey {
	return RoutingKey{
		Source:      source,
		Destination: destination,
		MessageType: messageType,
		EventType:   eventType,
	}
}

// Parses a routing key string into a RoutingKey structure.
func ParseRoutingKey(routingKey string) RoutingKey {
	tags := strings.Split(routingKey, ".")
	source := tags[0]
	destination := tags[1]
	messageType := tags[2]
	eventType := tags[3]

	return BuildRoutingKey(source, destination, messageType, eventType)
}

// Converts a RoutingKey structure to a string.
func (routingKey *RoutingKey) ToString() string {
	return routingKey.Source + "." + routingKey.Destination + "." + routingKey.MessageType + "." + routingKey.EventType
}

// Adds data related to an algorithm into the metadata.
func (metadata *Metadata) AddAlgorithmMetadata(algorithm string, algorithmMetadata map[string]interface{}) {
	metadata.Algorithms[algorithm] = make(map[string]interface{})

	for key, value := range algorithmMetadata {
		metadata.Algorithms[algorithm][key] = value
	}
}

// Builds a Message structure from a Metadata structure and a content.
func BuildMessage(metadata Metadata, content map[string]interface{}) Message {
	return Message{
		Metadata: metadata,
		Content:  content,
	}
}

// Converts a Message structure into a JSON string for communication over AMQP.
func (msg *Message) ToJSON() ([]byte, error) {
	// return json.MarshalIndent(msg, "", "    ")
	return json.Marshal(msg)
}

// Converts a JSON string into a Message structure.
// IMPORTANT NOTE: when unmarshalling from JSON, the resulting map[string]interface{} will contain numbers considered as float64,
// even if they were put into the map as integer. This is why you will see weird conversions like int(m[index].(float64)) in the code.
func FromJSON(msg []byte) Message {
	var msgMap Message
	json.Unmarshal(msg, &msgMap)

	return msgMap
}
