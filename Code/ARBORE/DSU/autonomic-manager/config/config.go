package config

import (
	"gde/common/gde-common/services"
)

// Queue name for this microservice
const QueueName = "am_queue"

// Service name
const ServiceName = services.AUTONOMIC_MANAGER

// Default IP of AMQP broker
const DefaultRabbitMQHost = "localhost"

// Default port of AMQP broker
const DefaultRabbitMQPort = "5672"

// Global AMQP exchange name
const ExchangeName = "gde_exchange"

// Global AMQP exchange type
const ExchangeType = "topic"

// waiting time befor resending snapshot message
const WaitTime = 5
