package core

import (
	"encoding/json"
	"io/ioutil"
)

// This map keeps track of each service's status
var ServicesStatus map[string]bool = make(map[string]bool)

func SaveServicesStatus() {
	data, _ := json.Marshal(ServicesStatus)
	_ = ioutil.WriteFile("statuses.json", data, 0644)
}

func LoadServicesStatus() {
	ServicesStatus = make(map[string]bool)

	data, _ := ioutil.ReadFile("statuses.json")
	json.Unmarshal(data, &ServicesStatus)
}
