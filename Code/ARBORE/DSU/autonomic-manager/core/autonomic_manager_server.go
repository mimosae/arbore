package core

import (
	"arbore/dsu/autonomic-manager/config"
	"arbore/dsu/autonomic-manager/dsu"

	graphs "arbore/dsu/knowledge-graphs/core"
	"encoding/json"
	"gde/common/gde-common/events"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"

	"log"
	//"time"
)

var MSGraph graphs.MultiGraph = graphs.CreateMSMultiGraphFromConfigurationFile("../knowledge-graphs/configuration-files/gde_link_type.txt")
var AMQPHandler *amqp.AMQPHandler
var Computeongingcollab int

// Common response channel to send response for the executed command.
// Once the asynchronous part of the command is complete, we send a response on this channel
// in order to unlock the executed command.
var ResponseChannel = make(chan string)
var DSUStartAcknowledgementCounter int = 0
var SnapshotChannel = make(chan string)
var StartChannel = make(chan string)

func HandleApplicationMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	// The Autonomic Manager should not receive any application message
	// Thus, this function can remain empty

	newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
	newContent := make(map[string]interface{})

	return false, newRoutingKey, newContent, nil
}

func HandleControlMessage(routingKey msghandler.RoutingKey, content map[string]interface{}, metadata msghandler.Metadata) (bool, msghandler.RoutingKey, map[string]interface{}, error) {
	log.Printf("[SERVICE %s] Received %s notification from microservice %s\n", config.ServiceName, routingKey.EventType, routingKey.Source)

	switch routingKey.EventType {
	case "MicroserviceStarted":
		ResponseChannel <- "OK"

	case "DeletedMicroservice":
		ServicesStatus[routingKey.Source] = false
		ResponseChannel <- "OK"

	case events.DSU_START_ACKNOWLEDGED:
		DSUStartAcknowledgementCounter++
		log.Printf("%d", DSUStartAcknowledgementCounter)
		if DSUStartAcknowledgementCounter == len(services.ServicesList)-2 {
			log.Printf("[SERVICE %s] Received %d acknowledgement ", config.ServiceName, DSUStartAcknowledgementCounter)
			//ResponseChannel <- "OK"
			log.Printf("[SERVICE %s] 1st Phase finished, next phase block new collaboration ", config.ServiceName)
			go DSUBlockNewCollaboration(AMQPHandler)
		}

	case events.DSU_NEW_COLLABORATION_BLOCKED:
		//ResponseChannel <- "OK"
		log.Printf("[SERVICE %s] start checking quiescence  ", config.ServiceName)
		log.Printf("[SERVICE %s] the update state %t withe startegy %s", config.ServiceName, dsu.OngoingDSUQuiescence.OngoingDSU, dsu.OngoingDSUQuiescence.Strategy)
		go DSUCheckQuiescence(AMQPHandler)
	case events.DSU_COLLABORATION_UNBLOCKED:
		log.Printf("[SERVICE %s] all collaboration are unblocked  ", config.ServiceName)
		log.Printf("[SERVICE %s] the update state %t with startegy %s", config.ServiceName, dsu.OngoingDSUQuiescence.OngoingDSU, dsu.OngoingDSUQuiescence.Strategy)
		ResponseChannel <- "OK"
	case events.DSU_Snapshot_Taken:

		if dsu.SnapshotCounter == int(metadata.Algorithms["Snapshot"]["SnapshotCounter"].(float64)) {
			record := dsu.ResponseRecord[dsu.SnapshotCounter]
			record.Responses[routingKey.Source] = true
			dsu.ResponseRecord[dsu.SnapshotCounter] = record

			messageCounterMap := metadata.Algorithms["Snapshot"]["MessageCounterMap"].(map[string]interface{})
			for collaborationID, messageCounter := range messageCounterMap {
				element, found := dsu.SnapshotCollection[collaborationID]

				if found {
					element.MessageCounters[routingKey.Source] = int(messageCounter.(float64))
					dsu.SnapshotCollection[collaborationID] = element
				} else {
					element := dsu.SnapshotCollectionData{
						MessageCounters:                       make(map[string]int),
						Status:                                "Started",
						SnapshotCounterOfFirstDetection:       int(metadata.Algorithms["Snapshot"]["SnapshotCounter"].(float64)),
						SnapshotCounterOfTerminationDetection: -1,
					}
					element.MessageCounters[routingKey.Source] = int(messageCounter.(float64))
					dsu.SnapshotCollection[collaborationID] = element
				}
			}

			if AllSnapshotReceived(dsu.SnapshotCounter) {
				Computeongingcollab++
				log.Printf("[Service %s] all Snapshot received %d", config.ServiceName, Computeongingcollab)
				ongoingCollaborations := make(map[string]string)
				for collaborationID, snapshotCollectionElement := range dsu.SnapshotCollection {
					if snapshotCollectionElement.Status == "Started" {
						terminationSum := CountMicroservicesMessages(snapshotCollectionElement)

						log.Printf("[Service %s] Termination sum: %d", config.ServiceName, terminationSum)

						if terminationSum == 0 {
							snapshotCollectionElement.Status = "Terminated"
							snapshotCollectionElement.SnapshotCounterOfTerminationDetection = dsu.SnapshotCounter
							dsu.SnapshotCollection[collaborationID] = snapshotCollectionElement
						} else {
							ongoingCollaborations[collaborationID] = msghandler.ParseCollaborationID(collaborationID).OperationName
						}
					}
				}

				jsonString, _ := json.Marshal(ongoingCollaborations)
				log.Printf("[Service %s] on going collab %s", config.ServiceName, ongoingCollaborations)
				SnapshotChannel <- string(jsonString)

			}
		}

	default:
		log.Printf("[SERVICE %s] Received unhandled control message", config.ServiceName)
	}

	newRoutingKey := msghandler.BuildRoutingKey("", "", "", "")
	newContent := make(map[string]interface{})

	return false, newRoutingKey, newContent, nil
}

/**
 * Checks that the Autonomic Manager received a snapshot response from all microservices.
 */
func AllSnapshotReceived(snapshotCounter int) bool {
	for _, microservice := range services.ServicesList {
		// TODO: For now, the Events Logger Service and Accountability Service are excluded from snapshots
		if microservice == "EventsLoggerService" || microservice == "AccountabilityService" {
			continue
		}

		if !ServicesStatus[microservice] {
			continue
		}

		if !dsu.ResponseRecord[snapshotCounter].Responses[microservice] {
			return false
		}
	}

	return true
}

/**
 * Sums up all microservices message counters.
 */
func CountMicroservicesMessages(snapshotCollectionElement dsu.SnapshotCollectionData) int {
	var sum = 0

	for _, microservice := range services.ServicesList {
		sum += snapshotCollectionElement.MessageCounters[microservice]
	}

	return sum
}
