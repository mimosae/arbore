package core

import (
	"arbore/dsu/autonomic-manager/config"
	"arbore/dsu/autonomic-manager/dsu"
	commondsu "arbore/dsu/common"

	//graphs "arbore/dsu/knowledge-graphs/core"
	"encoding/json"
	//"gde/common/gde-common/events"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/events"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"

	"log"
	"time"
)

func DSUCheckQuiescence(AMQPHandler *amqp.AMQPHandler) {
	quiescenceReached := false
	for !quiescenceReached {
		log.Printf("[SERVICE %s] Taking a snapshot, SnapshotCounter = %d\n", config.ServiceName, dsu.SnapshotCounter+1)
		log.Printf("[SERVICE %s] Snapshot requesting ", config.ServiceName)
		responseChannel := make(chan string)
		dsu.SnapshotCounter++
		dsu.ResponseRecord[dsu.SnapshotCounter] = dsu.ResponseRecordData{
			Channel:   responseChannel,
			Responses: make(map[string]bool),
		}

		algorithms := make(map[string]map[string]interface{})
		algorithms["Snapshot"] = make(map[string]interface{})
		algorithms["Snapshot"]["SnapshotCounter"] = dsu.SnapshotCounter

		metadata := msghandler.Metadata{
			CollaborationID: "",
			Algorithms:      algorithms,
		}

		msg := msghandler.BuildMessage(metadata, make(map[string]interface{}))

		for _, serviceName := range services.ServicesList {
			// TODO: For now, the Events Logger Service and Accountability Service are excluded from snapshots
			if serviceName == "EventsLoggerService" || serviceName == "AccountabilityService" {
				continue
			}

			if !ServicesStatus[serviceName] {
				continue
			}

			routingKey := msghandler.BuildRoutingKey(config.ServiceName, serviceName, "Control", events.DSU_Take_Snapshot)
			AMQPHandler.PublishMessage(routingKey, msg)
		}
		//var ongoingCollaborationsJsonString string
		ongoingCollaborationsJsonString := <-SnapshotChannel
		log.Printf("[SERVICE %s] ongoing collaboration %s", config.ServiceName, ongoingCollaborationsJsonString)
		log.Printf("[SERVICE %s]computing quiescence for each microcervices", config.ServiceName)
		for _, serviceName := range services.ServicesList {
			ongoingCollaborations := make(map[string]string)
			json.Unmarshal([]byte(ongoingCollaborationsJsonString), &ongoingCollaborations)

			var services []string
			//services = MSGraph.PathsTo(serviceName, services)
			services = append(services, serviceName)
			quiescenceReached = true
			for _, service := range services {
				collaborationExists := false
				for collaborationID := range ongoingCollaborations {
					firstms := msghandler.ParseCollaborationID(collaborationID).Emitter
					if firstms == service {
						collaborationExists = true
						break
					}
				}

				if collaborationExists {
					quiescenceReached = false
					break
				}
			}

			if !quiescenceReached {
				log.Printf("[SERVICE %s] Quiescence condition not reached, wait 5 seconds before taking another snapshot", serviceName)
				time.Sleep(config.WaitTime * time.Second)
				break
			}
		}
	}
	// start the phase 4
	log.Printf("[SERVICE %s] Continue the updating ", config.ServiceName)
	time.Sleep(5 * time.Second) // replace the time of updating
	go DSUUnblockAllCollaboration(AMQPHandler)
	log.Printf(" [SERVICE %s] finish  the updating ", config.ServiceName)

	//ResponseChannel <- "OK"
}

func DSUBlockNewCollaboration(AMQPHandler *amqp.AMQPHandler) {
	newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.FRONT_END, "Control", events.DSU_BLCOCK_NEW_COLLABORATIONS) // We have to add the BlockAllCollaboration to the set of events
	// handling algorithme metadata
	algorithms := make(map[string]map[string]interface{})
	algorithms["Snapshot"] = make(map[string]interface{})
	algorithms["Snapshot"]["SnapshotCounter"] = dsu.SnapshotCounter
	// creating metadata for the message
	metadata := msghandler.Metadata{
		CollaborationID: "",
		Algorithms:      algorithms,
	}
	// building the message
	newContent := make(map[string]interface{})
	//newContent["servicesTobepassive"] = Q                         // to be checked
	msg := msghandler.BuildMessage(metadata, newContent)
	AMQPHandler.PublishMessage(newRoutingKey, msg)

	//<-ResponseChannel
}
func UpdateFinished(AMQPHandler *amqp.AMQPHandler) string {
	// handling algorithme metadata
	algorithms := make(map[string]map[string]interface{})
	algorithms["Snapshot"] = make(map[string]interface{})
	algorithms["Snapshot"]["SnapshotCounter"] = dsu.SnapshotCounter
	// builing routing key with the microservice
	for _, service := range services.ServicesList {
		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, service, "Control", "Updatefinished")
		// creating metadata for the message
		metadata := msghandler.Metadata{
			CollaborationID: "",
			Algorithms:      algorithms,
		}

		// building the message
		msg := msghandler.BuildMessage(metadata, make(map[string]interface{}))
		// punlishing the message in the broker
		AMQPHandler.PublishMessage(newRoutingKey, msg)
		// waiting for ack from microservices

	}
	return <-ResponseChannel

}

func DSUUnblockAllCollaboration(AMQPHandler *amqp.AMQPHandler) {
	newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, services.FRONT_END, "Control", events.DSU_UNBLOCK_COLLABORATION) // We have to add the BlockAllCollaboration to the set of events
	// handling algorithme metadata
	algorithms := make(map[string]map[string]interface{})
	algorithms["Snapshot"] = make(map[string]interface{})
	algorithms["Snapshot"]["SnapshotCounter"] = dsu.SnapshotCounter
	// creating metadata for the message
	metadata := msghandler.Metadata{
		CollaborationID: "",
		Algorithms:      algorithms,
	}
	// building the message
	newContent := make(map[string]interface{})
	//newContent["servicesTobepassive"] = Q                         // to be checked
	msg := msghandler.BuildMessage(metadata, newContent)
	AMQPHandler.PublishMessage(newRoutingKey, msg)
	dsu.OngoingDSUQuiescence = &commondsu.DSUQuiescence{
		Strategy:   "",
		OngoingDSU: false,
	}
	log.Printf("[SERVICE %s] DSUUnblockAllCollaboration executed ", config.ServiceName)

	// <-ResponseChannel
}
