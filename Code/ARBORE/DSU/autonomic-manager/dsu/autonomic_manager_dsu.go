package dsu

import (
	commondsu "arbore/dsu/common"
	"encoding/json"
)

var SnapshotCounter int = 0

// type SnapshotCollectionDataStructure struct {
// 	MessageCounterFrontEnd                int
// 	MessageCounterAuthentication          int
// 	MessageCounterUser                    int
// 	MessageCounterPermission              int
// 	MessageCounterProject                 int
// 	MessageCounterFile                    int
// 	Status                                string
// 	SnapshotCounterOfFirstDetection       int
// 	SnapshotCounterOfTerminationDetection int
// }

type SnapshotCollectionData struct {
	MessageCounters                       map[string]int
	Status                                string
	SnapshotCounterOfFirstDetection       int
	SnapshotCounterOfTerminationDetection int
}

// Clean Data Structure to store all the snapshots
var SnapshotCollection = make(map[string]SnapshotCollectionData)

// type ResponseRecordDataStructure struct {
// 	ResponseFrontEnd      	bool
// 	ResponseAuthentication	bool
// 	ResponseUser          	bool
// 	ResponsePermission     	bool
// 	ResponseProject        	bool
// 	ResponseFile           	bool
// 	Channel             	chan string
// }

type ResponseRecordData struct {
	Responses map[string]bool
	Channel   chan string
}

// Clean Data Structure to store microservices responses per each microservice
var ResponseRecord = make(map[int]ResponseRecordData)

func ObtainJsonSnapshotCollection() string {
	bodyIndent, _ := json.MarshalIndent(SnapshotCollection, "", "    ")
	return string(bodyIndent)
}

var OngoingDSUQuiescence *commondsu.DSUQuiescence = &commondsu.DSUQuiescence{
	Strategy:   "",
	OngoingDSU: false,
}

// MsNameSlide to store the Quiescent set
var MsNameSlide []string
