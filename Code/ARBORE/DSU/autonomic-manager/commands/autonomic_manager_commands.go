package commands

import (
	"arbore/dsu/autonomic-manager/config"
	"arbore/dsu/autonomic-manager/core"
	"arbore/dsu/autonomic-manager/dsu"
	commondsu "arbore/dsu/common"

	graphs "arbore/dsu/knowledge-graphs/core"
	"gde/common/gde-common/events"

	amqp "gde/common/amqp-handler"
	"gde/common/gde-common/services"
	msghandler "gde/common/message-handler"

	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
)

var AcknowledgementCounter int = 0
var MSGraph graphs.MultiGraph = graphs.CreateMSMultiGraphFromConfigurationFile("../knowledge-graphs/configuration-files/gde_link_type.txt")

/**
 * Creates a microservice
 */
func CreateMicroservice(serviceName string) string {
	log.Printf("[SERVICE %s] Creating microservice \"%s\"", config.ServiceName, serviceName)

	if !ServiceExists(serviceName) {
		log.Printf("[SERVICE %s] ERROR: unknown microservice \"%s\"", config.ServiceName, serviceName)
		return "NOK"
	}

	ex, err := os.Executable()
	if err != nil {
		log.Printf("[SERVICE %s] Error: %v", config.ServiceName, err)
		return "NOK"
	}

	microservicePath := filepath.Dir(ex)
	microservicePath = strings.Replace(microservicePath, "autonomic-manager", "", 1)
	microservicePath = microservicePath + "../../GDE/services/" + ServiceNameToDirectoryName(serviceName) + "/" + ServiceNameToDirectoryName(serviceName)
	command := exec.Command(microservicePath)
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	errRun := command.Start()
	if errRun != nil {
		log.Printf("[SERVICE %s] Error while starting service \"%s\": %v", config.ServiceName, serviceName, errRun)
		return "NOK"
	} else {
		core.ServicesStatus[serviceName] = true
		return <-core.ResponseChannel
	}
}

/**
 * Deletes a microservice (shuts it down).
 */
func DeleteMicroservice(serviceName string, AMQPHandler *amqp.AMQPHandler) string {
	log.Printf("[SERVICE %s] Deleting microservice \"%s\"", config.ServiceName, serviceName)

	log.Printf("[SERVICE %s] Waiting for quiescence conditions to be reached...", config.ServiceName)

	//CheckQuiescence(serviceName, AMQPHandler)

	routingKey := msghandler.BuildRoutingKey(config.ServiceName, serviceName, "Control", "DeleteMicroservice")

	algorithms := make(map[string]map[string]interface{})
	algorithms["Snapshot"] = make(map[string]interface{})
	algorithms["Snapshot"]["SnapshotCounter"] = dsu.SnapshotCounter

	metadata := msghandler.Metadata{
		CollaborationID: "",
		Algorithms:      algorithms,
	}

	msg := msghandler.BuildMessage(metadata, make(map[string]interface{}))
	AMQPHandler.PublishMessage(routingKey, msg)

	return <-core.ResponseChannel
}

/**
 * Makes the Autonomic Manager take a snapshot of the entire system
 *   - Sends a AMQP message to all microservices so that they take a local snapshot
 *   - Waits for the snapshot confirmation from all microservices
 */

// TODO: Move the 3 below functions in the gde-common module

func ServiceExists(serviceName string) bool {
	for _, element := range services.ServicesList {
		if serviceName == element {
			return true
		}
	}

	return false
}

func ServiceNameToDirectoryName(serviceName string) string {
	re := regexp.MustCompile(`[A-Z][^A-Z]*`)

	submatchall := re.FindAllString(serviceName, -1)
	result := ""
	first := true
	for _, element := range submatchall {
		if first {
			first = false
			result = result + strings.ToLower(element)
		} else {
			result = result + "-" + strings.ToLower(element)
		}
	}

	return result
}

func DirectoryNameToServiceName(directoryName string) string {
	for _, element := range services.ServicesList {
		if ServiceNameToDirectoryName(element) == directoryName {
			return element
		}
	}

	return ""
}

// this function is used to inform microservices that dsu algorithm will start
func HandelDSUstartMessage(AMQPHandler *amqp.AMQPHandler) string {
	// handling algorithme metadata
	dsu.OngoingDSUQuiescence = &commondsu.DSUQuiescence{
		Strategy:   commondsu.Blocking,
		OngoingDSU: true,
	}

	algorithms := make(map[string]map[string]interface{})
	algorithms["Snapshot"] = make(map[string]interface{})
	algorithms["Snapshot"]["SnapshotCounter"] = dsu.SnapshotCounter
	// builing routing key with the microservice
	for _, service := range services.ServicesList {
		if service == "EventsLoggerService" || service == "AccountabilityService" {
			continue
		}

		newRoutingKey := msghandler.BuildRoutingKey(config.ServiceName, service, "Control", events.DSU_START)
		// creating metadata for the message
		metadata := msghandler.Metadata{
			CollaborationID: "",
			Algorithms:      algorithms,
		}

		// building the message
		msg := msghandler.BuildMessage(metadata, make(map[string]interface{}))
		// punlishing the message in the broker
		AMQPHandler.PublishMessage(newRoutingKey, msg)
		// waiting for ack from microservices

	}
	return <-core.ResponseChannel

}

/**
 * Makes the Autonomic Manager take a snapshot of the entire system
 *   - Sends a AMQP message to all microservices so that they take a local snapshot
 *   - Waits for the snapshot confirmation from all microservices
 */

// TODO: Move the 3 below functions in the gde-common module
func TakeSnapshot(AMQPHandler *amqp.AMQPHandler) string {
	log.Printf("[SERVICE %s] Taking a snapshot, SnapshotCounter = %d\n", config.ServiceName, dsu.SnapshotCounter+1)

	responseChannel := make(chan string)
	dsu.SnapshotCounter++
	dsu.ResponseRecord[dsu.SnapshotCounter] = dsu.ResponseRecordData{
		Channel:   responseChannel,
		Responses: make(map[string]bool),
	}

	algorithms := make(map[string]map[string]interface{})
	algorithms["Snapshot"] = make(map[string]interface{})
	algorithms["Snapshot"]["SnapshotCounter"] = dsu.SnapshotCounter

	metadata := msghandler.Metadata{
		CollaborationID: "",
		Algorithms:      algorithms,
	}

	msg := msghandler.BuildMessage(metadata, make(map[string]interface{}))

	for _, serviceName := range services.ServicesList {
		// TODO: For now, the Events Logger Service and Accountability Service are excluded from snapshots
		if serviceName == "EventsLoggerService" || serviceName == "AccountabilityService" {
			continue
		}

		if !core.ServicesStatus[serviceName] {
			continue
		}

		routingKey := msghandler.BuildRoutingKey(config.ServiceName, serviceName, "Control", events.DSU_Take_Snapshot)
		AMQPHandler.PublishMessage(routingKey, msg)
	}

	return <-core.ResponseChannel
}
