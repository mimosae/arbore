package main

import (
	"arbore/dsu/autonomic-manager/commands"
	"arbore/dsu/autonomic-manager/config"
	"arbore/dsu/autonomic-manager/core"
	"arbore/dsu/autonomic-manager/executor"
	"bufio"
	"fmt"
	"os"
	"strings"

	amqp "gde/common/amqp-handler"

	"log"
)

func StartAutonomicManager() {
	core.AMQPHandler = amqp.InitAMQPHandler(config.DefaultRabbitMQHost, config.DefaultRabbitMQPort, config.ExchangeName, config.ExchangeType)
	core.AMQPHandler.CreateQueue(config.QueueName)
	core.AMQPHandler.BindQueue(config.QueueName, "*."+config.ServiceName+".*.*")

	status := make(chan bool)
	go core.AMQPHandler.ConsumeMessagesAutonomicManager(config.QueueName, config.ServiceName, core.HandleApplicationMessage, core.HandleControlMessage, status)
	<-status
}

func StopAutonomicManager() {
	core.AMQPHandler.Close()
}

func main() {
	defer StopAutonomicManager()
	StartAutonomicManager()

	core.LoadServicesStatus()

	if len(os.Args) == 1 { // Execution in interactive mode
		fmt.Print("> ")
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			args := strings.Split(scanner.Text(), " ")
			res := HandleCommand(args)
			log.Printf("[SERVICE %s] Command result: %s", config.ServiceName, res)
			fmt.Print("> ")
		}
	} else { // Single-command mode
		args := os.Args[1:]
		res := HandleCommand(args)
		log.Printf("[SERVICE %s] Command result: %s", config.ServiceName, res)
	}

	core.SaveServicesStatus()
}

func HandleCommand(args []string) string {
	switch args[0] {
	case "create-microservice":
		return commands.CreateMicroservice(args[1])

	case "delete-microservice":
		return commands.DeleteMicroservice(args[1], core.AMQPHandler)

	case "DSU-start":
		return commands.HandelDSUstartMessage(core.AMQPHandler)

	case "run-plan":
		return executor.ExecutePlan(args[1], core.AMQPHandler)
	case "take-snapshot":
		return commands.TakeSnapshot(core.AMQPHandler)
	case "stop", "exit", "quit":
		core.SaveServicesStatus()
		os.Exit(0)
		return ""

	default:
		return "Unknown command"
	}
}
