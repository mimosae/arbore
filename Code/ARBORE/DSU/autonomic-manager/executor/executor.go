package executor

import (
	"arbore/dsu/autonomic-manager/commands"
	"arbore/dsu/autonomic-manager/config"
	planParser "arbore/dsu/plan-parser"
	"arbore/dsu/plan-parser/entities"
	amqp "gde/common/amqp-handler"
	"log"
)

func ExecutePlan(planFilename string, AMQPHandler *amqp.AMQPHandler) string {
	plan := planParser.LoadPlanFromFile(planFilename)

	log.Printf("[SERVICE %s] Executing plan '%s' located at '%s'", config.ServiceName, plan.Name, planFilename)

	sectionIndex := 0
	for _, parallelSection := range plan.ParallelSections {
		log.Printf("[SERVICE %s] Entering parallel section S%d", config.ServiceName, sectionIndex)

		actionIndex := 0
		for _, action := range parallelSection.Actions {
			log.Printf("[SERVICE %s] Executing action A%d.%d - %s", config.ServiceName, sectionIndex, actionIndex, action)

			res := "NOK"
			switch action.Type {
			case entities.ActionType_CreateMicroservice:
				serviceName := commands.DirectoryNameToServiceName(action.Arguments["service"])
				// TODO: This call is blocking, thus parallel sections are not parallel at the moment
				res = commands.CreateMicroservice(serviceName)
			case entities.ActionType_DeleteMicroservice:
				serviceName := commands.DirectoryNameToServiceName(action.Arguments["service"])
				// TODO: This call is blocking, thus parallel sections are not parallel at the moment
				res = commands.DeleteMicroservice(serviceName, AMQPHandler)
			default:
				log.Printf("[SERVICE %s] ERROR: undefined action type", config.ServiceName)
			}

			if res == "NOK" {
				log.Printf("[SERVICE %s] ERROR: Unable to execute action A%d.%d, aborting plan execution", config.ServiceName, sectionIndex, actionIndex)
				return "NOK"
			}

			actionIndex++
		}

		// TODO: Implement barriers to wait for section end

		sectionIndex++
	}

	return "OK"
}
