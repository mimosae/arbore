package commondsu

type strategy string

const (
	Blocking  strategy = "BlockingDSU"
	strategy2 strategy = "strategy2"
)

var BlockedcollaborationRequest = make(map[string]interface{})

// var OngoingDSU bool
type DSUQuiescence struct {
	Strategy   strategy
	OngoingDSU bool
}
