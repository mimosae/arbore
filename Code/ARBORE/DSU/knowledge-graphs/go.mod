module arbore/dsu/knowledge-graphs

go 1.19

require (
	github.com/go-echarts/go-echarts/v2 v2.2.4
	github.com/streadway/amqp v1.0.0
	gonum.org/v1/gonum v0.11.0
)
