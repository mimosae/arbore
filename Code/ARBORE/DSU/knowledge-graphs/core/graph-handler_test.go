package core

import (
	"fmt"
	"testing"
)

func TestGraphCreation(t *testing.T) {
	g1 := NewMultiGraph()

	n1 := g1.CreateMicroserviceNode("ms1")
	n2 := g1.CreateMicroserviceNode("ms2")
	n3 := g1.CreateMicroserviceNode("ms3")
	n4 := g1.CreateMicroserviceNode("ms2")

	g1.CreateMicroservicesEdge(n1, n2)
	g1.CreateMicroservicesEdge(n3, n4)

	fmt.Print("#################### create a test multigraph ####################")
	fmt.Println(g1)
}

func TestCreateMSGraphFromConfigFiles(t *testing.T) {
	serviceLinksFilePath := "../configuration-files/gde_link_type_test.txt"
	g := CreateMSMultiGraphFromConfigurationFile(serviceLinksFilePath)

	fmt.Print("#################### service links multigraph ####################")
	fmt.Print(g)
}

func TestCreateMsgTypeGraphFromConfigFiles(t *testing.T) {
	msgTypeFilePath := "../configuration-files/gde_msg_type_dep_test.txt"
	g := CreateMgsTypesMultiGraphFromConfigurationFile(msgTypeFilePath)

	fmt.Println()
	fmt.Print("#################### Message type links multigraph ####################")
	fmt.Print(g)
	fmt.Println()
}

func TestPathsTo(t *testing.T) {
	serviceLinksFilePath := "../configuration-files/gde_link_type.txt"
	g := CreateMSMultiGraphFromConfigurationFile(serviceLinksFilePath)

	from_service := "FileService"
	var services []string
	services = g.PathsTo(from_service, services)

	fmt.Println()
	fmt.Println("#################### Paths to from", from_service, "service ####################")
	fmt.Println(services)
	fmt.Println()
}

func TestFuturParticipation(t *testing.T) {
	msgTypeFilePath := "../configuration-files/gde_msg_type_dep.txt"
	g := CreateMgsTypesMultiGraphFromConfigurationFile(msgTypeFilePath)

	from_service := "AS"
	from_event := "authenticate"
	var services []string
	services = g.FuturParticipations(from_service, from_event, services)

	fmt.Println()
	fmt.Println("#################### Futur service participations to from <", from_service, ",", from_event, ">####################")
	fmt.Println(services)
	fmt.Println()
}
