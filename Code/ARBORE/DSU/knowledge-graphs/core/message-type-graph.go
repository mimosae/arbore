package core

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"reflect"
	"strconv"
	"strings"

	"gonum.org/v1/gonum/graph"
)

type MessageTypeNode struct {
	graph.Node
	MicroserviceType string
	MessageType      string
}

type MessageTypeEdge struct {
	graph.Line
	SeqNumber  int
	Offset     int
	Repetition int
}

func (n MessageTypeNode) String() string {
	return fmt.Sprintf("MessageTypeNode<MStype: %s, MsgType: %s>", n.MicroserviceType, n.MessageType)
}

func (e MessageTypeEdge) String() string {
	return fmt.Sprintf("MessageTypeEdge<seq: %d, offset: %d, recp: %d>", e.SeqNumber, e.Offset, e.Repetition)
}

func (g MultiGraph) CreateMsgTypeNode(msType string, msgType string) MessageTypeNode {
	node := MessageTypeNode{
		Node:             g.NewNode(),
		MicroserviceType: msType,
		MessageType:      msgType,
	}
	g.AddNode(node)
	return node
}

func (g MultiGraph) CreateMsgTypeEdge(from MessageTypeNode, to MessageTypeNode, seq int, offset int, nrep int) MessageTypeEdge {
	line := MessageTypeEdge{
		Line:       g.NewLine(from, to),
		SeqNumber:  seq,
		Offset:     offset,
		Repetition: nrep,
	}
	g.SetLine(line)
	return line
}

func (g MultiGraph) FindNodeByMsAndMsgTypes(msType string, msgType string) (bool, MessageTypeNode) {
	iterator := g.Nodes()
	for iterator.Next() {
		node := iterator.Node()
		if reflect.TypeOf(node) == reflect.TypeOf(MessageTypeNode{}) {
			n := node.(MessageTypeNode)
			if n.MicroserviceType == msType && n.MessageType == msgType {
				return true, n
			}
		}
	}

	return false, MessageTypeNode{}
}

func CreateMgsTypesMultiGraphFromConfigurationFile(filepath string) MultiGraph {
	configurationFile, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(configurationFile)

	g := NewMultiGraph()
	for scanner.Scan() {
		line := strings.Split(scanner.Text(), "::=")

		_, from := g.createNodeFromTextDescription(line[0])

		destinationLists := strings.Split(line[1], "|")
		for i := range destinationLists {
			destination := strings.Split(destinationLists[i], ">")
			for j := range destination {
				node_description, nrep := getRepetition(destination[j])
				_, to := g.createNodeFromTextDescription(node_description)
				seq := i + 1
				offset := j + 1
				if !g.FindEdgeBetweenNodes(from, to) {
					g.CreateMsgTypeEdge(from, to, seq, offset, nrep)
				}
			}
		}
	}

	return g

}

func (g MultiGraph) createNodeFromTextDescription(description string) (bool, MessageTypeNode) {
	node := strings.Trim(description, " ")
	node = strings.Trim(node, "(")
	node = strings.Trim(node, ")")

	node_description := strings.Split(node, ",")
	ms := node_description[0]
	event := node_description[1]

	found, to := g.FindNodeByMsAndMsgTypes(ms, event)
	if !found {
		to = g.CreateMsgTypeNode(ms, event)
	}
	return found, to
}

func getRepetition(description string) (string, int) {
	node_description := strings.Split(description, ")")
	repetition := 1
	if len(node_description[1]) > 0 {
		rep := strings.Trim(node_description[1], " ")
		if rep == "*" {
			repetition = math.MaxInt
		} else {
			repetition, _ = strconv.Atoi(rep)
		}
	}
	return node_description[0], repetition

}

func (g MultiGraph) FuturParticipations(msType string, event string, msNameSlice []string) []string {
	found, node := g.FindNodeByMsAndMsgTypes(msType, event)

	if !found {
		log.Fatal("Node <", msType, event, "> doesn't exist in this configuration")
	}

	msNameSlice = append(msNameSlice, node.MicroserviceType)

	directNodes := g.To(node.ID())
	for directNodes.Next() {
		nodeRAW := directNodes.Node()
		MSNode := nodeRAW.(MessageTypeNode)
		if !Contains(msNameSlice, MSNode.MicroserviceType) {
			msNameSlice = g.FuturParticipations(MSNode.MicroserviceType, MSNode.MessageType, msNameSlice)
		}
	}
	return msNameSlice
}
