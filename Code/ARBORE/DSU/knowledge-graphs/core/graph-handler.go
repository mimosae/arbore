package core

import (
	"fmt"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/multi"
)

type MultiGraph struct {
	*multi.DirectedGraph
}

func NewMultiGraph() MultiGraph {
	return MultiGraph{
		DirectedGraph: multi.NewDirectedGraph(),
	}
}

func (g MultiGraph) String() string {
	str := ""
	iterator := g.Nodes()

	for iterator.Next() {
		next := iterator.Node()
		str = fmt.Sprintf("%s \n node: %s", str, next)
		froms := g.From(next.ID())
		for froms.Next() {
			from := froms.Node()
			lines := g.Lines(next.ID(), from.ID())
			for lines.Next() {
				str = fmt.Sprintf("%s \n line from node %v to node %v : ", str, next, from)
				line := lines.Line()
				str = fmt.Sprintf("%s \n %s : ", str, line)
			}
		}
	}

	return str
}

func (g MultiGraph) FindEdgeBetweenNodes(from graph.Node, to graph.Node) bool {
	lines := g.Lines(from.ID(), to.ID())
	return lines.Next()
}

func Contains(sliceToSearch []string, x string) bool {
	for _, element := range sliceToSearch {
		if x == element {
			return true
		}
	}
	return false
}
