package core

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"

	"gonum.org/v1/gonum/graph"
)

type MicroserviceNode struct {
	graph.Node
	Name string
}

type MicroserviceEdge struct {
	graph.Line
}

func (n MicroserviceNode) String() string {
	return fmt.Sprintf("MicroserviceNode<Name: %s>", n.Name)
}

func (g MultiGraph) CreateMicroserviceNode(msName string) MicroserviceNode {
	found, node := g.FindNodeByName(msName)
	if !found {
		node = MicroserviceNode{
			Node: g.NewNode(),
			Name: msName,
		}
		g.AddNode(node)
	}
	return node
}

func (g MultiGraph) CreateMicroservicesEdge(from MicroserviceNode, to MicroserviceNode) MicroserviceEdge {
	line := MicroserviceEdge{
		Line: g.NewLine(from, to),
	}
	g.SetLine(line)
	return line
}

func (g MultiGraph) FindNodeByName(msName string) (bool, MicroserviceNode) {
	iterator := g.Nodes()
	for iterator.Next() {
		node := iterator.Node()
		if reflect.TypeOf(node) == reflect.TypeOf(MicroserviceNode{}) {
			n := node.(MicroserviceNode)
			if n.Name == msName {
				return true, n
			}
		}
	}

	return false, MicroserviceNode{}
}

func CreateMSMultiGraphFromConfigurationFile(filepath string) MultiGraph {
	configurationFile, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(configurationFile)

	g := NewMultiGraph()
	for scanner.Scan() {
		line := strings.Split(scanner.Text(), "::=")

		found, from := g.FindNodeByName(line[0])
		if !found {
			from = g.CreateMicroserviceNode(line[0])
		}

		found, to := g.FindNodeByName(line[1])
		if !found {
			to = g.CreateMicroserviceNode(line[1])
		}

		found = g.FindEdgeBetweenNodes(from, to)
		if !found {
			g.CreateMicroservicesEdge(from, to)
		}
	}

	return g

}

func (g MultiGraph) PathsTo(msName string, msNameSlice []string) []string {
	_, node := g.FindNodeByName(msName)
	directNodes := g.To(node.ID())
	for directNodes.Next() {
		nodeRAW := directNodes.Node()
		MSNode := nodeRAW.(MicroserviceNode)
		if !Contains(msNameSlice, MSNode.Name) {
			msNameSlice = append(msNameSlice, node.Name)
			msNameSlice = g.PathsTo(MSNode.Name, msNameSlice)
		}
	}
	return msNameSlice
}
