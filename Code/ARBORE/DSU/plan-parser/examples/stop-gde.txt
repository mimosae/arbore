Name "GDE Shutdown"
From d5438... to cddf83...

in parallel:
    [action=delete_microservice, args=[service="authentication-service"]]
    [action=delete_microservice, args=[service="file-service"]]
    [action=delete_microservice, args=[service="permission-service"]]
    [action=delete_microservice, args=[service="project-service"]]
    [action=delete_microservice, args=[service="user-service"]]
