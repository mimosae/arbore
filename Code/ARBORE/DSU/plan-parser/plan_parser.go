package planparser

import (
	"arbore/dsu/plan-parser/entities"
)

func LoadPlanFromFile(filename string) entities.Plan {
	return entities.ParsePlanFromFile(filename)
}
