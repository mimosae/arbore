package entities

import "fmt"

type ParallelSection struct {
	Actions []Action
}

func (ps *ParallelSection) AddAction(action Action) {
	ps.Actions = append(ps.Actions, action)
}

func (ps ParallelSection) String() string {
	str := "ParallelSection <Actions:\n"

	for _, action := range ps.Actions {
		str = fmt.Sprintf("%s    %s\n", str, action)
	}

	return fmt.Sprintf("%s  >", str)
}
