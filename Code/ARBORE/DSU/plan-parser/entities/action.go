package entities

import (
	"fmt"
	"log"
	"strings"
)

type Action struct {
	Type      ActionType
	Arguments map[string]string
}

func ParseAction(str string) Action {
	// Removing first and last character ("[" and "]")
	description := str[1 : len(str)-1]

	// Finding the two fields "action" and "args"
	fields := strings.SplitN(description, ",", 2)

	_, actionType, _ := strings.Cut(fields[0], "=")
	actionArguments := fields[1]

	return Action{
		Type:      ParseActionType(actionType),
		Arguments: ParseActionArguments(actionArguments),
	}
}

func ParseActionArguments(str string) map[string]string {
	args := make(map[string]string)

	// Removing the "args="
	_, str, found := strings.Cut(str, "args=")
	if !found {
		log.Fatalf("Error while parsing action arguments")
	}

	// Removing leading and trailing brackets
	str = str[1 : len(str)-1]

	// Parsing the fields
	fields := strings.Split(str, ",")

	// Adding each field to the args map
	for _, field := range fields {
		index, value, found := strings.Cut(strings.TrimSpace(field), "=")
		if !found {
			log.Fatalf("Error while parsing action arguments")
		}

		args[index] = strings.Trim(value, "\"")
	}

	return args
}

func (action Action) String() string {
	return fmt.Sprintf("Action <Type: '%s', Arguments: %v>", action.Type, action.Arguments)
}
