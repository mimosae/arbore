package entities

type ActionType int64

const (
	ActionType_Undefined ActionType = iota
	ActionType_DeleteMicroservice
	ActionType_CreateMicroservice
)

func ParseActionType(str string) ActionType {
	switch str {
	case "delete_microservice":
		return ActionType_DeleteMicroservice
	case "create_microservice":
		return ActionType_CreateMicroservice
	default:
		return ActionType_Undefined
	}
}

func (actionType ActionType) String() string {
	switch actionType {
	case ActionType_DeleteMicroservice:
		return "delete_microservice"
	case ActionType_CreateMicroservice:
		return "create_microservice"
	default:
		return "unknown"
	}
}
