package entities

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

type Plan struct {
	Name                    string
	StartingConfigurationID string
	EndingConfigurationID   string
	ParallelSections        []ParallelSection
}

func ParsePlanFromFile(filename string) Plan {
	planFile, err := os.Open(filename)
	if err != nil {
		log.Fatalf("Error while opening plan file: %s", err)
	}

	scanner := bufio.NewScanner(planFile)

	plan := Plan{
		ParallelSections: make([]ParallelSection, 0),
	}
	planName := "Undefined"
	startingConfigurationID := "Undefined"
	endingConfigurationID := "Undefined"
	currentParallelSection := ParallelSection{
		Actions: make([]Action, 0),
	}
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())

		// Ignore blank lines
		if line == "" {
			continue
		}

		if strings.Contains(line, "Name") {
			if planName != "Undefined" {
				log.Fatalf("Error while parsing plan file: multiple definition of Name")
			}

			desc := strings.SplitN(line, " ", 2)
			planName = strings.Trim(desc[1], "\"")
			continue
		}

		// Ignore header line
		if strings.Contains(line, "From") {
			if startingConfigurationID != "Undefined" || endingConfigurationID != "Undefined" {
				log.Fatalf("Error while parsing plan file: multiple definition of starting and ending configuration IDs")
			}

			_, line, _ = strings.Cut(line, "From")
			ids := strings.Split(strings.TrimSpace(line), " to ")

			startingConfigurationID = ids[0]
			endingConfigurationID = ids[1]

			continue
		}

		// We have the start of a new parallel section
		if line == "in parallel:" {
			// Add the previous parallel section in the plan if it is not empty
			if len(currentParallelSection.Actions) != 0 {
				plan.AddParallelSection(currentParallelSection)
			}

			// Start a new parallel section
			currentParallelSection = ParallelSection{
				Actions: make([]Action, 0),
			}
			continue
		}

		// We have the description of an action
		if strings.Contains(line, "[action=") {
			currentParallelSection.AddAction(ParseAction(line))
			continue
		}

		log.Fatalf("Error while parsing plan file: unexpected line")
	}

	// Save the last parallel section in the plan if it is not empty
	if len(currentParallelSection.Actions) != 0 {
		plan.AddParallelSection(currentParallelSection)
	}

	plan.Name = planName

	// Starting and ending configuration IDs are required
	if startingConfigurationID == "Undefined" || endingConfigurationID == "Undefined" {
		log.Fatal("Error while parsing plan file: no starting and ending configuration IDs found")
	}

	plan.StartingConfigurationID = startingConfigurationID
	plan.EndingConfigurationID = endingConfigurationID

	return plan
}

func (plan *Plan) AddParallelSection(ps ParallelSection) {
	plan.ParallelSections = append(plan.ParallelSections, ps)
}

func (plan Plan) String() string {
	str := ""
	str = fmt.Sprintf("%sPlan <Name: '%s', StartingConfigurationID: '%s', EndingConfigurationID: '%s', ParallelSections:\n", str, plan.Name, plan.StartingConfigurationID, plan.EndingConfigurationID)

	for _, ps := range plan.ParallelSections {
		str = fmt.Sprintf("%s  %s\n", str, ps)
	}

	return fmt.Sprintf("%s>", str)
}
