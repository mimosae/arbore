package planparser

import (
	"arbore/dsu/plan-parser/entities"
	"fmt"
	"testing"
)

func TestPlanParsingFromFile(t *testing.T) {
	plan := entities.ParsePlanFromFile("examples/plan-test.txt")
	fmt.Println(plan)
}
