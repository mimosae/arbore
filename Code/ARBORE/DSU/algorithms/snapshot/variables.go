package snapshot

type SnapshotVars struct {
	SnapshotCounter   int            // The snapshot counter
	MessageCounterMap map[string]int // The message counter map (key: collaborationID, value: message counter)
}
