package snapshot

import (
	"gde/common/gde-common/events"
	msghandler "gde/common/message-handler"

	"log"
)

// Structure for handling the snapshot algorithm, see AlgorithmHandler interface
type SnapshotHandler struct {
	Variables SnapshotVars
}

// Microservice-side handler, called before the microservice handles the message content itself
func (handler SnapshotHandler) LocalHandler(service string, routingKey msghandler.RoutingKey, message msghandler.Message) (bool, msghandler.RoutingKey, msghandler.Message) {
	collaborationID := message.Metadata.CollaborationID
	receivedSnapshotCounter := int(message.Metadata.Algorithms["Snapshot"]["SnapshotCounter"].(float64))

	switch {
	case routingKey.MessageType == "Application":
		handler.Variables.MessageCounterMap[collaborationID]--
		if handler.Variables.SnapshotCounter < receivedSnapshotCounter {
			var routingKey, msg = TakeSnapshot(service, collaborationID, handler.Variables.MessageCounterMap, receivedSnapshotCounter)
			handler.Variables.SnapshotCounter = receivedSnapshotCounter
			return true, routingKey, msg
		}

	case routingKey.MessageType == "Control":
		log.Printf("[SERVICE %s] [AMQP HANDLER] This is a control message (bis): %s", service, routingKey.EventType)
		if routingKey.EventType == events.DSU_Take_Snapshot {
			log.Printf("[SERVICE %s] [AMQP HANDLER] This is a control message (ter)", service)
			log.Printf("[SNAPSHOT ALGORITHM] Service \"" + service + "\" received \"TakeSnapshot\" event from AutonomicManager")
			var routingKey, msg = TakeSnapshot(service, collaborationID, handler.Variables.MessageCounterMap, receivedSnapshotCounter)
			return true, routingKey, msg
		}
	}

	return false, msghandler.RoutingKey{}, msghandler.Message{}
}

/* Gets the metadata related to the snapshot algorithm */
func (handler SnapshotHandler) GetAlgorithmMetadata() map[string]interface{} {
	var snapshotMetadata = make(map[string]interface{})
	snapshotMetadata["SnapshotCounter"] = handler.Variables.SnapshotCounter
	snapshotMetadata["MessageCounterMap"] = handler.Variables.MessageCounterMap

	return snapshotMetadata
}

/*Takes a snapshot of the service */
func TakeSnapshot(service string, collaborationID string, messageCounterMap map[string]int, receivedSnapshotCounter int) (msghandler.RoutingKey, msghandler.Message) {
	routingKey := msghandler.BuildRoutingKey(service, "AutonomicManager", "Control", events.DSU_Snapshot_Taken)

	algorithms := make(map[string]map[string]interface{})
	algorithms["Snapshot"] = make(map[string]interface{})
	algorithms["Snapshot"]["SnapshotCounter"] = receivedSnapshotCounter
	algorithms["Snapshot"]["MessageCounterMap"] = messageCounterMap
	metadata := msghandler.Metadata{
		CollaborationID: collaborationID,
		Algorithms:      algorithms,
	}

	content := make(map[string]interface{})

	message := msghandler.BuildMessage(metadata, content)

	return routingKey, message
}
