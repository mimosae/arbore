package algorithms

import (
	"arbore/dsu/algorithms/snapshot"
	msghandler "gde/common/message-handler"
)

// Each algorithm handler has to implement the AlgorithmeHandler interface
type AlgorithmHandler interface {
	LocalHandler(service string, routingKey msghandler.RoutingKey, message msghandler.Message) (bool, msghandler.RoutingKey, msghandler.Message)
	GetAlgorithmMetadata() map[string]interface{}
}

// Common variables used by multiple algorithms
type CommonVariables struct {
	StatusActive bool
	UpdateStart  bool
}

// Structure containing all existing algorithm handlers
type Handlers struct {
	SnapshotHandler *snapshot.SnapshotHandler
}
